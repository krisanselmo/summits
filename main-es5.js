(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-sidenav-container class=\"example-container\">\n  <mat-sidenav [(opened)]=\"opened\"\n               (opened)=\"events.push('open!')\"\n               (closed)=\"events.push('close!')\"\n               #sidenav\n               mode=\"side\">\n\n    <mat-toolbar color=\"primary\">\n      <!--<mat-toolbar-row>-->\n      <!--<span>First Row</span>-->\n      <!--</mat-toolbar-row>-->\n\n      <!--<mat-toolbar-row>-->\n      <span>Routing</span>\n      <span class=\"example-spacer\"></span>\n      <!--<mat-icon class=\"example-icon\" aria-hidden=\"false\" aria-label=\"Example heart icon\">favorite</mat-icon>-->\n      <button mat-icon-button>\n        <mat-icon>keyboard_arrow_left</mat-icon>\n      </button>\n    </mat-toolbar>\n\n    <!--<mat-toolbar color=\"primary\">-->\n      <!--<span class=\"fill-remaining-space\">-->\n        <!--<button mat-icon-button [matMenuTriggerFor]=\"menu\">-->\n          <!--<mat-icon>terrain</mat-icon>-->\n        <!--</button>-->\n    <mat-menu #menu=\"matMenu\" [overlapTrigger]=\"false\">\n      <button mat-menu-item>\n        <mat-icon>home</mat-icon>\n        <span>Home</span>\n      </button>\n      <button mat-menu-item>\n        <mat-icon>people_outline</mat-icon>\n        <span>Connecting</span>\n      </button>\n      <button mat-menu-item>\n        <mat-icon>videocam</mat-icon>\n        <span>Let's talk</span>\n      </button>\n      <button mat-menu-item>\n        <mat-icon>exit_to_app</mat-icon>\n        <span>Logout</span>\n      </button>\n    </mat-menu>\n      <!--</span>-->\n      <!--<span class=\"fill-remaining-space\">Mountain router</span>-->\n    <!--</mat-toolbar>-->\n\n    <!--{{ title }}-->\n\n    <!--<mat-card>-->\n      <!--<div cdkDrop>-->\n        <!--<div class=\"box\" cdkDrag>Drag me around!</div>-->\n      <!--</div>-->\n    <!--</mat-card>-->\n\n    <button #ref mat-button color=\"primary\">\n      Click me!\n    </button>\n\n    <!--<mat-form-field >-->\n      <!--<mat-label>Favorite food</mat-label>-->\n      <!--<mat-select>-->\n        <!--<mat-option *ngFor=\"let food of foods\"-->\n                    <!--[value]=\"food.value\">-->\n          <!--{{food.viewValue}}-->\n        <!--</mat-option>-->\n      <!--</mat-select>-->\n    <!--</mat-form-field>-->\n\n    <!--<p><button mat-button (click)=\"sidenav.toggle()\">sidenav.toggle()</button></p>-->\n\n    <!--<p>-->\n      <!--<span matBadge=\"4\" matBadgeOverlap=\"false\">Text with a badge</span>-->\n    <!--</p>-->\n\n    <mat-card class='example-card'>\n      <mat-card-header>\n        <!--<div mat-card-avatar class='example-header-image'></div>-->\n        <mat-card-title> {{ getInfo().distance | number:'1.0-2':'fr' }} km</mat-card-title>\n        <mat-card-subtitle>\n          <mat-icon>trending_up</mat-icon>\n          <span>&nbsp;{{ getInfo().totalAscent | number:'1.0-0':'fr' }} m d+</span>\n          &nbsp;|&nbsp;\n          <mat-icon>trending_down</mat-icon>\n          <span>&nbsp;{{ getInfo().totalDescent | number:'1.0-0':'fr' }} m d-</span>\n        </mat-card-subtitle>\n      </mat-card-header>\n    </mat-card>\n\n    <mat-toolbar color=\"primary\">\n\n      <!--<mat-toolbar-row>-->\n        <!--<span>First Row</span>-->\n      <!--</mat-toolbar-row>-->\n\n      <!--<mat-toolbar-row>-->\n      <span>Routing</span>\n      <span class=\"example-spacer\"></span>\n      <!--<mat-icon class=\"example-icon\" aria-hidden=\"false\" aria-label=\"Example heart icon\">favorite</mat-icon>-->\n      <button mat-icon-button [matMenuTriggerFor]=\"menu\">\n        <!--<mat-icon>menu</mat-icon>-->\n        <!--https://material.io/tools/icons/?style=baseline-->\n        <mat-icon [matBadge]=\"getNumberOfMarkers()\"\n                  matBadgeColor=\"primary\">timeline</mat-icon>\n      </button>\n\n        <!--</mat-toolbar-row>-->\n\n      <!--<span>Application Title</span>-->\n\n      <!--&lt;!&ndash; This fills the remaining space of the current row &ndash;&gt;-->\n      <!--<span class=\"example-fill-remaining-space\"></span>-->\n\n      <!--<span>Right Aligned Text</span>-->\n    </mat-toolbar>\n\n    <button (click)=\"resetRouting()\"\n            mat-icon-button>\n      <mat-icon matBadge=\"15\"\n                matBadgeColor=\"primary\">delete</mat-icon>\n    </button>\n\n    <button (click)=\"undo()\"\n            [disabled]=\"!canUndo()\"\n            mat-icon-button>\n      <mat-icon matBadge=\"\"\n                matBadgeColor=\"warn\">undo</mat-icon>\n    </button>\n\n    <button (click)=\"redo()\"\n            [disabled]=\"!canRedo()\"\n            mat-icon-button>\n      <mat-icon matBadge=\"\"\n                matBadgeColor=\"warn\">redo</mat-icon>\n    </button>\n\n    <button (click)=\"reverse()\"\n            [disabled]=\"getNumberOfMarkers() === 0\"\n            mat-icon-button>\n      <mat-icon>swap_horiz</mat-icon>\n    </button>\n\n    <button (click)=\"reverse()\"\n            [disabled]=\"getNumberOfMarkers() === 0\"\n            mat-icon-button>\n      <mat-icon>save_alt</mat-icon>\n    </button>\n\n    <button (click)=\"reverse()\"\n            [disabled]=\"getNumberOfMarkers() === 0\"\n            mat-icon-button>\n      <mat-icon>settings</mat-icon>\n    </button>\n\n    <div>text</div>\n\n    <app-barchart *ngIf=\"chartData\"\n                  [data]=\"chartData\">\n    </app-barchart>\n\n\n    <mat-toolbar color=\"primary\">\n      <span>POI</span>\n    </mat-toolbar>\n\n    <button (click)=\"printWater()\"\n            mat-icon-button>\n      <mat-icon>settings</mat-icon>\n    </button>\n\n\n  </mat-sidenav>\n\n  <mat-sidenav-content>\n    <app-map [state]=\"ok\"\n             [activePOI]=\"activePOIs\"></app-map>\n  </mat-sidenav-content>\n</mat-sidenav-container>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/charts/barchart/barchart.component.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/charts/barchart/barchart.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"d3-chart\" #chart></div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/map/map.component.html":
/*!******************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/map/map.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div #map></div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/map/popup/popup.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/map/popup/popup.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<div></div>-->\n\n\n<div>{{ test }}</div>\n"

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");




var routes = [
    {
        path: 'app',
        component: _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
        children: [],
    },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  background: #eee;\n}\n\nmat-sidenav {\n  width: 250px;\n  background: #eee;\n}\n\nmat-card {\n  margin: 10px;\n  padding: 16px 0;\n  text-align: center;\n}\n\nmat-card ::ng-deep .mat-card-header-text {\n  width: 100%;\n}\n\nmat-card mat-card-subtitle {\n  margin-bottom: 0;\n}\n\nmat-card mat-card-subtitle mat-icon {\n  vertical-align: middle;\n  display: -webkit-inline-box;\n  display: inline-flex;\n}\n\nbutton {\n  position: relative;\n  text-align: center;\n}\n\nmat-toolbar-row,\nmat-toolbar {\n  height: 48px;\n}\n\napp-leaflet {\n  width: 100%;\n  height: 100%;\n}\n\n.example-icon {\n  padding: 0 14px;\n}\n\n.example-spacer {\n  -webkit-box-flex: 1;\n          flex: 1 1 auto;\n}\n\n.fill-remaining-space {\n  /* This fills the remaining space, by using flexbox.\n     Every toolbar row uses a flexbox row layout. */\n  -webkit-box-flex: 1;\n          flex: 1 1 auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2tyaXMvRG9jdW1lbnRzL3JlcG9zL2tyaXMvc3VtbWl0cy9zcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQUE7RUFDQSxNQUFBO0VBQ0EsU0FBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0VBQ0EsZ0JBQUE7QUNDRjs7QURFQTtFQUNFLFlBQUE7RUFDQSxnQkFBQTtBQ0NGOztBREdBO0VBQ0UsWUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQ0FGOztBREdJO0VBQ0UsV0FBQTtBQ0ROOztBRFNFO0VBQ0UsZ0JBQUE7QUNQSjs7QURTSTtFQUNFLHNCQUFBO0VBQ0EsMkJBQUE7RUFBQSxvQkFBQTtBQ1BOOztBRFlBO0VBQ0Usa0JBQUE7RUFDQSxrQkFBQTtBQ1RGOztBRGFBOztFQUVFLFlBQUE7QUNWRjs7QURhQTtFQUNDLFdBQUE7RUFDQSxZQUFBO0FDVkQ7O0FEYUE7RUFDRSxlQUFBO0FDVkY7O0FEYUE7RUFDRSxtQkFBQTtVQUFBLGNBQUE7QUNWRjs7QURhQTtFQUNFO21EQUFBO0VBRUEsbUJBQUE7VUFBQSxjQUFBO0FDVkYiLCJmaWxlIjoic3JjL2FwcC9hcHAuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXhhbXBsZS1jb250YWluZXIge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgYm90dG9tOiAwO1xuICBsZWZ0OiAwO1xuICByaWdodDogMDtcbiAgYmFja2dyb3VuZDogI2VlZTtcbn1cblxubWF0LXNpZGVuYXYge1xuICB3aWR0aDogMjUwcHg7XG4gIGJhY2tncm91bmQ6ICNlZWU7XG4gIC8vYmFja2dyb3VuZDogIzI5MjkyOTtcbn1cblxubWF0LWNhcmQge1xuICBtYXJnaW46IDEwcHg7XG4gIHBhZGRpbmc6IDE2cHggMDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuXG4gIDo6bmctZGVlcCB7XG4gICAgLm1hdC1jYXJkLWhlYWRlci10ZXh0IHtcbiAgICAgIHdpZHRoOiAxMDAlO1xuICAgIH1cbiAgfVxuXG4gIG1hdC1jYXJkLXRpdGxlIHtcblxuICB9XG5cbiAgbWF0LWNhcmQtc3VidGl0bGUge1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG5cbiAgICBtYXQtaWNvbiB7XG4gICAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICAgICAgZGlzcGxheTogaW5saW5lLWZsZXg7XG4gICAgfVxuICB9XG59XG5cbmJ1dHRvbiB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLy9cbi8vLmZpbGwtcmVtYWluaW5nLXNwYWNlLFxubWF0LXRvb2xiYXItcm93LFxubWF0LXRvb2xiYXIge1xuICBoZWlnaHQ6IDQ4cHg7XG59XG5cbmFwcC1sZWFmbGV0IHtcbiB3aWR0aDogMTAwJTtcbiBoZWlnaHQ6IDEwMCU7XG59XG5cbi5leGFtcGxlLWljb24ge1xuICBwYWRkaW5nOiAwIDE0cHg7XG59XG5cbi5leGFtcGxlLXNwYWNlciB7XG4gIGZsZXg6IDEgMSBhdXRvO1xufVxuXG4uZmlsbC1yZW1haW5pbmctc3BhY2Uge1xuICAvKiBUaGlzIGZpbGxzIHRoZSByZW1haW5pbmcgc3BhY2UsIGJ5IHVzaW5nIGZsZXhib3guXG4gICAgIEV2ZXJ5IHRvb2xiYXIgcm93IHVzZXMgYSBmbGV4Ym94IHJvdyBsYXlvdXQuICovXG4gIGZsZXg6IDEgMSBhdXRvO1xufVxuIiwiLmV4YW1wbGUtY29udGFpbmVyIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGJvdHRvbTogMDtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIGJhY2tncm91bmQ6ICNlZWU7XG59XG5cbm1hdC1zaWRlbmF2IHtcbiAgd2lkdGg6IDI1MHB4O1xuICBiYWNrZ3JvdW5kOiAjZWVlO1xufVxuXG5tYXQtY2FyZCB7XG4gIG1hcmdpbjogMTBweDtcbiAgcGFkZGluZzogMTZweCAwO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5tYXQtY2FyZCA6Om5nLWRlZXAgLm1hdC1jYXJkLWhlYWRlci10ZXh0IHtcbiAgd2lkdGg6IDEwMCU7XG59XG5tYXQtY2FyZCBtYXQtY2FyZC1zdWJ0aXRsZSB7XG4gIG1hcmdpbi1ib3R0b206IDA7XG59XG5tYXQtY2FyZCBtYXQtY2FyZC1zdWJ0aXRsZSBtYXQtaWNvbiB7XG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG4gIGRpc3BsYXk6IGlubGluZS1mbGV4O1xufVxuXG5idXR0b24ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxubWF0LXRvb2xiYXItcm93LFxubWF0LXRvb2xiYXIge1xuICBoZWlnaHQ6IDQ4cHg7XG59XG5cbmFwcC1sZWFmbGV0IHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuLmV4YW1wbGUtaWNvbiB7XG4gIHBhZGRpbmc6IDAgMTRweDtcbn1cblxuLmV4YW1wbGUtc3BhY2VyIHtcbiAgZmxleDogMSAxIGF1dG87XG59XG5cbi5maWxsLXJlbWFpbmluZy1zcGFjZSB7XG4gIC8qIFRoaXMgZmlsbHMgdGhlIHJlbWFpbmluZyBzcGFjZSwgYnkgdXNpbmcgZmxleGJveC5cbiAgICAgRXZlcnkgdG9vbGJhciByb3cgdXNlcyBhIGZsZXhib3ggcm93IGxheW91dC4gKi9cbiAgZmxleDogMSAxIGF1dG87XG59Il19 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _map_routing_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./map/routing.service */ "./src/app/map/routing.service.ts");





var AppComponent = /** @class */ (function () {
    // @ViewChild('sidenav', { read: ElementRef, static: true })
    // public sidenav: MatSidenav;
    // @ViewChild('deleteRouteButton', { read: ElementRef, static: true })
    // public deleteRouteButton: ElementRef;
    function AppComponent(_routingService) {
        this._routingService = _routingService;
        this.events = [];
        this.opened = true;
        this.title = 'summits';
        this.activePOIs = [];
        this.ok = '["sac_scale"]';
        // public btn: any;
        this.foods = [
            { value: '["trail_visibility"]', viewValue: 'Visibility' },
            { value: '["sac_scale"]', viewValue: 'SAC scale' },
            { value: 'tacos-2', viewValue: 'Tacos' }
        ];
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        var source = Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["fromEvent"])(this.form.nativeElement, 'click');
        // const deleteRouteClick = fromEvent(this.deleteRouteButton.nativeElement, 'click');
        // deleteRouteClick.pipe(
        //   map(event => `Event time: ${event}`)
        // );
        //
        // deleteRouteClick.subscribe(val => {
        //   console.log('val', val);
        // });
        var example = source.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (event) { return "Event time: " + event; }));
        // output (example): 'Event time: 7276.390000000001'
        // const subscribe = example.subscribe(val => {
        //   this.ok = '["trail_visibility"]';
        // });
        setTimeout(function () {
            _this._generateRandomData();
            // change the data periodically
            setInterval(function () { return _this._generateRandomData(); }, 5000);
        }, 1000);
    };
    AppComponent.prototype.getNumberOfMarkers = function () {
        return this._routingService.getInfo().numberOfMarkers;
    };
    AppComponent.prototype.getInfo = function () {
        return this._routingService.getInfo();
    };
    AppComponent.prototype.resetRouting = function () {
        this._routingService.resetRouting();
    };
    AppComponent.prototype.undo = function () {
        this._routingService.undo();
    };
    AppComponent.prototype.redo = function () {
        this._routingService.redo();
    };
    AppComponent.prototype.reverse = function () {
        this._routingService.reverse();
    };
    AppComponent.prototype.canUndo = function () {
        return this._routingService.canUndoRedo('undo');
    };
    AppComponent.prototype.canRedo = function () {
        return this._routingService.canUndoRedo('redo');
    };
    AppComponent.prototype.printWater = function () {
        if (this.activePOIs.includes('water')) {
            this.activePOIs = this.activePOIs.filter(function (poi) { return poi !== 'water'; });
            // this.activePOIs.push('water');
        }
        else {
            this.activePOIs.push('water');
        }
        console.log('app', this.activePOIs);
    };
    AppComponent.prototype._generateRandomData = function () {
        this.chartData = [];
        for (var i = 0; i < (8 + Math.floor(Math.random() * 10)); i++) {
            this.chartData.push([
                "" + i,
                Math.floor(Math.random() * 100)
            ]);
        }
    };
    AppComponent.ctorParameters = function () { return [
        { type: _map_routing_service__WEBPACK_IMPORTED_MODULE_4__["RoutingService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('ref', { read: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], AppComponent.prototype, "form", void 0);
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_map_routing_service__WEBPACK_IMPORTED_MODULE_4__["RoutingService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: MyInterceptor, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyInterceptor", function() { return MyInterceptor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_common_locales_fr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/locales/fr */ "./node_modules/@angular/common/locales/fr.js");
/* harmony import */ var _angular_common_locales_fr__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_angular_common_locales_fr__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../material.module */ "./src/material.module.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _map_map_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./map/map.component */ "./src/app/map/map.component.ts");
/* harmony import */ var _map_popup_popup_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./map/popup/popup.component */ "./src/app/map/popup/popup.component.ts");
/* harmony import */ var _charts_barchart_barchart_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./charts/barchart/barchart.component */ "./src/app/charts/barchart/barchart.component.ts");
/* harmony import */ var _map_b_router_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./map/b-router.service */ "./src/app/map/b-router.service.ts");
















Object(_angular_common__WEBPACK_IMPORTED_MODULE_6__["registerLocaleData"])(_angular_common_locales_fr__WEBPACK_IMPORTED_MODULE_7___default.a);
var MyInterceptor = /** @class */ (function () {
    function MyInterceptor() {
    }
    MyInterceptor.prototype.intercept = function (req, next) {
        var trimmedUrl = req.url.trim();
        // https://blog.angularindepth.com/top-10-ways-to-use-interceptors-in-angular-db450f8a62d6
        if (!req.url.startsWith(_map_b_router_service__WEBPACK_IMPORTED_MODULE_15__["endpointBRouterAPI"])) {
            return next.handle(req);
        }
        return next.handle(req).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["tap"])(function (event) {
            if (event instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpResponse"] && event.status === 200) {
                // console.log('cool', event.body);
            }
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["catchError"])(function (e) {
            console.log('e', e.error.text);
            throw e;
        }));
    };
    MyInterceptor = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])()
    ], MyInterceptor);
    return MyInterceptor;
}());

var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_11__["AppComponent"],
                _map_map_component__WEBPACK_IMPORTED_MODULE_12__["MapComponent"],
                _charts_barchart_barchart_component__WEBPACK_IMPORTED_MODULE_14__["BarchartComponent"],
                _map_popup_popup_component__WEBPACK_IMPORTED_MODULE_13__["PopupComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_10__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"],
                _material_module__WEBPACK_IMPORTED_MODULE_9__["MaterialModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            ],
            providers: [
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HTTP_INTERCEPTORS"], useClass: MyInterceptor, multi: true },
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_11__["AppComponent"]],
            entryComponents: [
                _map_popup_popup_component__WEBPACK_IMPORTED_MODULE_13__["PopupComponent"],
            ],
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/charts/barchart/barchart.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/charts/barchart/barchart.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".d3-chart {\n  width: 100%;\n  height: 200px;\n}\n.d3-chart .axis line,\n.d3-chart .axis path {\n  stroke: #999;\n}\n.d3-chart .axis text {\n  fill: red;\n  font: 13px Roboto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2tyaXMvRG9jdW1lbnRzL3JlcG9zL2tyaXMvc3VtbWl0cy9zcmMvYXBwL2NoYXJ0cy9iYXJjaGFydC9iYXJjaGFydC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvY2hhcnRzL2JhcmNoYXJ0L2JhcmNoYXJ0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsV0FBQTtFQUNBLGFBQUE7QUNDRjtBREVJOztFQUVFLFlBQUE7QUNBTjtBREdJO0VBQ0UsU0FBQTtFQUNBLGlCQUFBO0FDRE4iLCJmaWxlIjoic3JjL2FwcC9jaGFydHMvYmFyY2hhcnQvYmFyY2hhcnQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZDMtY2hhcnQge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAyMDBweDtcblxuICAuYXhpcyB7XG4gICAgbGluZSxcbiAgICBwYXRoIHtcbiAgICAgIHN0cm9rZTogIzk5OTtcbiAgICB9XG5cbiAgICB0ZXh0IHtcbiAgICAgIGZpbGw6IHJlZDtcbiAgICAgIGZvbnQ6IDEzcHggUm9ib3RvO1xuICAgIH1cbiAgfVxufVxuIiwiLmQzLWNoYXJ0IHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMjAwcHg7XG59XG4uZDMtY2hhcnQgLmF4aXMgbGluZSxcbi5kMy1jaGFydCAuYXhpcyBwYXRoIHtcbiAgc3Ryb2tlOiAjOTk5O1xufVxuLmQzLWNoYXJ0IC5heGlzIHRleHQge1xuICBmaWxsOiByZWQ7XG4gIGZvbnQ6IDEzcHggUm9ib3RvO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/charts/barchart/barchart.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/charts/barchart/barchart.component.ts ***!
  \*******************************************************/
/*! exports provided: BarchartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BarchartComponent", function() { return BarchartComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var d3__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! d3 */ "./node_modules/d3/index.js");



var BarchartComponent = /** @class */ (function () {
    function BarchartComponent() {
        this.margin = {
            top: 20,
            bottom: 20,
            left: 40,
            right: 20
        };
    }
    BarchartComponent.prototype.ngOnInit = function () {
        this.createChart();
        if (this.data) {
            this.updateChart();
        }
    };
    BarchartComponent.prototype.ngOnChanges = function () {
        if (this.chart) {
            this.updateChart();
        }
    };
    BarchartComponent.prototype.createChart = function () {
        var element = this.chartContainer.nativeElement;
        this.width = element.offsetWidth - this.margin.left - this.margin.right;
        this.height = element.offsetHeight - this.margin.top - this.margin.bottom;
        var svg = d3__WEBPACK_IMPORTED_MODULE_2__["select"](element).append('svg')
            .attr('width', element.offsetWidth)
            .attr('height', element.offsetHeight);
        // chart plot area
        this.chart = svg.append('g')
            .attr('class', 'bars')
            .attr('transform', "translate(" + this.margin.left + ", " + this.margin.top + ")");
        // define X & Y domains
        var xDomain = this.data.map(function (d) { return d[0]; });
        var yDomain = [0, d3__WEBPACK_IMPORTED_MODULE_2__["max"](this.data, function (d) { return d[1]; })];
        // create scales
        this.xScale = d3__WEBPACK_IMPORTED_MODULE_2__["scaleBand"]().padding(0.1).domain(xDomain).rangeRound([0, this.width]);
        this.yScale = d3__WEBPACK_IMPORTED_MODULE_2__["scaleLinear"]().domain(yDomain).range([this.height, 0]);
        // bar colors
        this.colors = d3__WEBPACK_IMPORTED_MODULE_2__["scaleLinear"]().domain([0, this.data.length]).range(['red', 'blue']);
        // x & y axis
        this.xAxis = svg.append('g')
            .attr('class', 'axis axis-x')
            .attr('transform', "translate(" + this.margin.left + ", " + (this.margin.top + this.height) + ")")
            .call(d3__WEBPACK_IMPORTED_MODULE_2__["axisBottom"](this.xScale));
        this.yAxis = svg.append('g')
            .attr('class', 'axis axis-y')
            .attr('transform', "translate(" + this.margin.left + ", " + this.margin.top + ")")
            .call(d3__WEBPACK_IMPORTED_MODULE_2__["axisLeft"](this.yScale));
    };
    BarchartComponent.prototype.updateChart = function () {
        var _this = this;
        // update scales & axis
        this.xScale.domain(this.data.map(function (d) { return d[0]; }));
        this.yScale.domain([0, d3__WEBPACK_IMPORTED_MODULE_2__["max"](this.data, function (d) { return d[1]; })]);
        this.colors.domain([0, this.data.length]);
        this.xAxis.transition().call(d3__WEBPACK_IMPORTED_MODULE_2__["axisBottom"](this.xScale));
        this.yAxis.transition().call(d3__WEBPACK_IMPORTED_MODULE_2__["axisLeft"](this.yScale));
        var update = this.chart.selectAll('.bar')
            .data(this.data);
        // remove exiting bars
        update.exit().remove();
        // update existing bars
        this.chart.selectAll('.bar').transition()
            .attr('x', function (d) { return _this.xScale(d[0]); })
            .attr('y', function (d) { return _this.yScale(d[1]); })
            .attr('width', function (d) { return _this.xScale.bandwidth(); })
            .attr('height', function (d) { return _this.height - _this.yScale(d[1]); })
            .style('fill', function (d, i) { return _this.colors(i); });
        // add new bars
        update
            .enter()
            .append('rect')
            .attr('class', 'bar')
            .attr('x', function (d) { return _this.xScale(d[0]); })
            .attr('y', function (d) { return _this.yScale(0); })
            .attr('width', this.xScale.bandwidth())
            .attr('height', 0)
            .style('fill', function (d, i) { return _this.colors(i); })
            .transition()
            .delay(function (d, i) { return i * 10; })
            .attr('y', function (d) { return _this.yScale(d[1]); })
            .attr('height', function (d) { return _this.height - _this.yScale(d[1]); });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('chart', { read: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], BarchartComponent.prototype, "chartContainer", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], BarchartComponent.prototype, "data", void 0);
    BarchartComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-barchart',
            template: __webpack_require__(/*! raw-loader!./barchart.component.html */ "./node_modules/raw-loader/index.js!./src/app/charts/barchart/barchart.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./barchart.component.scss */ "./src/app/charts/barchart/barchart.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], BarchartComponent);
    return BarchartComponent;
}());



/***/ }),

/***/ "./src/app/gpx/gpx.service.ts":
/*!************************************!*\
  !*** ./src/app/gpx/gpx.service.ts ***!
  \************************************/
/*! exports provided: GpxService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GpxService", function() { return GpxService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _tmcw_togeojson__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @tmcw/togeojson */ "./node_modules/@tmcw/togeojson/dist/togeojson.umd.js");
/* harmony import */ var _tmcw_togeojson__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_tmcw_togeojson__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");





var GpxService = /** @class */ (function () {
    function GpxService(_httpClient) {
        this._httpClient = _httpClient;
    }
    GpxService.prototype.getGeoJSON = function (url) {
        return this._httpClient.get(url, {
            responseType: 'text'
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
            var parser = new DOMParser();
            var rawGpx = parser.parseFromString(data, 'text/xml');
            return _tmcw_togeojson__WEBPACK_IMPORTED_MODULE_3__["gpx"](rawGpx);
        }));
    };
    GpxService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    GpxService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root',
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], GpxService);
    return GpxService;
}());



/***/ }),

/***/ "./src/app/helper.ts":
/*!***************************!*\
  !*** ./src/app/helper.ts ***!
  \***************************/
/*! exports provided: round, componentToHex, rgbToHex, hexToRgb, pickInterpolatedColor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "round", function() { return round; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "componentToHex", function() { return componentToHex; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "rgbToHex", function() { return rgbToHex; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hexToRgb", function() { return hexToRgb; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pickInterpolatedColor", function() { return pickInterpolatedColor; });
function round(value, n) {
    if (n === void 0) { n = 2; }
    if (!value) {
        return value;
    }
    var k = Math.pow(10, n);
    return Math.round(value * k) / k;
}
function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length === 1 ? '0' + hex : hex;
}
function rgbToHex(rgb) {
    return '#' + componentToHex(rgb[0]) + componentToHex(rgb[1]) + componentToHex(rgb[2]);
}
function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16),
    } : null;
}
function pickInterpolatedColor(color1, color2, weight) {
    var w1 = weight;
    var w2 = 1 - w1;
    var rgb = [
        Math.round(color1[0] * w1 * 255 + color2[0] * w2 * 255),
        Math.round(color1[1] * w1 * 255 + color2[1] * w2 * 255),
        Math.round(color1[2] * w1 * 255 + color2[2] * w2 * 255)
    ];
    return rgb;
}


/***/ }),

/***/ "./src/app/map/b-router.service.ts":
/*!*****************************************!*\
  !*** ./src/app/map/b-router.service.ts ***!
  \*****************************************/
/*! exports provided: endpointBRouterAPI, BRouterService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "endpointBRouterAPI", function() { return endpointBRouterAPI; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BRouterService", function() { return BRouterService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");





var endpointBRouterAPI = 'http://h2096617.stratoserver.net:443/_getRouteFromBRouterAPI';
var parseCoordinate = function (coord) {
    return (+coord) / Math.pow(10, 6);
};
var BRouterService = /** @class */ (function () {
    function BRouterService(_httpClient) {
        this._httpClient = _httpClient;
    }
    BRouterService.prototype.fetchRoute = function (a, b) {
        // const url = 'https://brouter.damsy.net/api/brouter?lonlats=5.920945,45.452786|5.951104,45.468318&' +
        //   'profile=shortest&alternativeidx=0&format=geojson';
        return this._httpClient.get(endpointBRouterAPI, {
            params: {
                lonlats: a[1] + "," + a[0] + "|" + b[1] + "," + b[0],
                profile: 'shortest',
                alternativeidx: '0',
                format: 'geojson',
            },
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
            return data;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (e) {
            // this.handleError(e);
            // Swallow
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(null);
        }));
    };
    BRouterService.prototype.parseCoordsFromBRouterMessages = function (bRouterMessages) {
        var nodes = [];
        for (var i = 1; i < bRouterMessages.length; i++) {
            nodes.push([
                parseCoordinate(bRouterMessages[i][1]),
                parseCoordinate(bRouterMessages[i][0]),
            ]);
        }
        return nodes;
    };
    BRouterService.prototype.handleError = function (error) {
        var errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = "Error: " + error.error.message;
        }
        else {
            // server-side error
            errorMessage = "Error Code: " + error.status + "\nMessage: " + error.message;
        }
        // window.alert(errorMessage);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(errorMessage);
    };
    BRouterService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    BRouterService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root',
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], BRouterService);
    return BRouterService;
}());



/***/ }),

/***/ "./src/app/map/gpx.helper.ts":
/*!***********************************!*\
  !*** ./src/app/map/gpx.helper.ts ***!
  \***********************************/
/*! exports provided: convertToGpx */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "convertToGpx", function() { return convertToGpx; });
function convertToGpx(route) {
    return;
}


/***/ }),

/***/ "./src/app/map/interfaces.ts":
/*!***********************************!*\
  !*** ./src/app/map/interfaces.ts ***!
  \***********************************/
/*! exports provided: ClickType, OsmType */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClickType", function() { return ClickType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OsmType", function() { return OsmType; });
var ClickType;
(function (ClickType) {
    ClickType[ClickType["Left"] = 2] = "Left";
    ClickType[ClickType["Right"] = 1] = "Right";
})(ClickType || (ClickType = {}));
var OsmType;
(function (OsmType) {
    OsmType["Node"] = "node";
    OsmType["Way"] = "way";
    OsmType["Relation"] = "relation";
})(OsmType || (OsmType = {}));


/***/ }),

/***/ "./src/app/map/leaflet.helper.ts":
/*!***************************************!*\
  !*** ./src/app/map/leaflet.helper.ts ***!
  \***************************************/
/*! exports provided: haversineFormat, isMarkerOnMap, getLatLngTuple, customHaversine, getClosestCoordinate */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "haversineFormat", function() { return haversineFormat; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isMarkerOnMap", function() { return isMarkerOnMap; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getLatLngTuple", function() { return getLatLngTuple; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "customHaversine", function() { return customHaversine; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getClosestCoordinate", function() { return getClosestCoordinate; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var haversine__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! haversine */ "./node_modules/haversine/haversine.js");
/* harmony import */ var haversine__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(haversine__WEBPACK_IMPORTED_MODULE_1__);


var haversineFormat = { format: '[lat,lon]' };
function isMarkerOnMap(lMap, marker) {
    return lMap._layers[marker._leaflet_id];
}
function getLatLngTuple(marker) {
    var latLng = marker._latlng;
    return [latLng.lat, latLng.lng];
}
function customHaversine(latLng1, latLng2) {
    var d = haversine__WEBPACK_IMPORTED_MODULE_1__(latLng1, latLng2, haversineFormat);
    // if (!d) {
    //   throw Error(`haversine error, coord1: ${latLng1}, coord2: ${latLng2}`);
    // }
    return d || 0;
}
function getClosestCoordinate(polyline, latLng) {
    var e_1, _a;
    var latLngs = polyline._latlngs;
    var minDist = Infinity;
    var closetCoord = null;
    try {
        for (var latLngs_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__values"](latLngs), latLngs_1_1 = latLngs_1.next(); !latLngs_1_1.done; latLngs_1_1 = latLngs_1.next()) {
            var coord = latLngs_1_1.value;
            var polylineLatLng = [coord.lat, coord.lng];
            var distance = customHaversine(polylineLatLng, latLng);
            if (distance < minDist) {
                minDist = distance;
                closetCoord = polylineLatLng;
            }
        }
    }
    catch (e_1_1) { e_1 = { error: e_1_1 }; }
    finally {
        try {
            if (latLngs_1_1 && !latLngs_1_1.done && (_a = latLngs_1.return)) _a.call(latLngs_1);
        }
        finally { if (e_1) throw e_1.error; }
    }
    return closetCoord;
}


/***/ }),

/***/ "./src/app/map/leaflet.media.ts":
/*!**************************************!*\
  !*** ./src/app/map/leaflet.media.ts ***!
  \**************************************/
/*! exports provided: customIcon, routingPolylineOptions, waypointMarker */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "customIcon", function() { return customIcon; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routingPolylineOptions", function() { return routingPolylineOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "waypointMarker", function() { return waypointMarker; });
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! leaflet */ "./node_modules/leaflet/dist/leaflet-src.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(leaflet__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _interfaces__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./interfaces */ "./src/app/map/interfaces.ts");


var customIcon = function (clickType) { return leaflet__WEBPACK_IMPORTED_MODULE_0__["icon"]({
    iconUrl: clickType === _interfaces__WEBPACK_IMPORTED_MODULE_1__["ClickType"].Left ? '/assets/peak.svg' : '/assets/saddle.svg',
    // shadowUrl: 'icon-shadow.png',
    // iconSize:     [64, 64]
    // shadowSize:   [50, 64]
    iconAnchor: [32, 64],
    // shadowAnchor: [32, 64]
    popupAnchor: [-3, -76],
}); };
var routingPolylineOptions = {
    color: 'red',
    opacity: 0.7,
};
var waypointMarker = function (markerType) { return leaflet__WEBPACK_IMPORTED_MODULE_0__["icon"]({
    iconUrl: "/assets/markers/" + markerType + ".png",
    iconSize: [33, 50],
    iconAnchor: [16, 45],
    shadowSize: [50, 50],
    shadowAnchor: [16, 47],
    popupAnchor: [0, -45],
    // iconAnchor: [-15, 64],
    // popupAnchor: [-3, -76],
    // popupAnchor:  [0, -40],
    shadowUrl: '/assets/markers/pin-shadow.png',
}); };


/***/ }),

/***/ "./src/app/map/leaflet.popup.scss":
/*!****************************************!*\
  !*** ./src/app/map/leaflet.popup.scss ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "::ng-deep .popupStyle .leaflet-popup-content-wrapper {\n  background: #182b33;\n  color: #ffffff;\n}\n::ng-deep .popupStyle .leaflet-popup-tip {\n  background: #182b33;\n}\n::ng-deep .popupStyle a {\n  color: #ffffff;\n}\n::ng-deep .leaflet-control-layers {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2tyaXMvRG9jdW1lbnRzL3JlcG9zL2tyaXMvc3VtbWl0cy9zcmMvYXBwL21hcC9sZWFmbGV0LnBvcHVwLnNjc3MiLCJzcmMvYXBwL21hcC9sZWFmbGV0LnBvcHVwLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBSUk7RUFDRSxtQkFMSztFQU1MLGNBQUE7QUNITjtBRE1JO0VBQ0UsbUJBVks7QUNNWDtBRE9JO0VBQ0UsY0FBQTtBQ0xOO0FEU0U7RUFDRSxpREFBQTtBQ1BKIiwiZmlsZSI6InNyYy9hcHAvbWFwL2xlYWZsZXQucG9wdXAuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiRkZWVwQmx1ZTogIzE4MmIzMztcblxuOjpuZy1kZWVwIHtcbiAgLnBvcHVwU3R5bGUge1xuICAgIC5sZWFmbGV0LXBvcHVwLWNvbnRlbnQtd3JhcHBlciB7XG4gICAgICBiYWNrZ3JvdW5kOiAkZGVlcEJsdWU7XG4gICAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICB9XG5cbiAgICAubGVhZmxldC1wb3B1cC10aXAge1xuICAgICAgYmFja2dyb3VuZDogJGRlZXBCbHVlO1xuICAgIH1cblxuICAgIGEge1xuICAgICAgY29sb3I6ICNmZmZmZmY7XG4gICAgfVxuICB9XG5cbiAgLmxlYWZsZXQtY29udHJvbC1sYXllcnMge1xuICAgIGZvbnQtZmFtaWx5OiBSb2JvdG8sIFwiSGVsdmV0aWNhIE5ldWVcIiwgc2Fucy1zZXJpZjtcbiAgfVxufVxuIiwiOjpuZy1kZWVwIC5wb3B1cFN0eWxlIC5sZWFmbGV0LXBvcHVwLWNvbnRlbnQtd3JhcHBlciB7XG4gIGJhY2tncm91bmQ6ICMxODJiMzM7XG4gIGNvbG9yOiAjZmZmZmZmO1xufVxuOjpuZy1kZWVwIC5wb3B1cFN0eWxlIC5sZWFmbGV0LXBvcHVwLXRpcCB7XG4gIGJhY2tncm91bmQ6ICMxODJiMzM7XG59XG46Om5nLWRlZXAgLnBvcHVwU3R5bGUgYSB7XG4gIGNvbG9yOiAjZmZmZmZmO1xufVxuOjpuZy1kZWVwIC5sZWFmbGV0LWNvbnRyb2wtbGF5ZXJzIHtcbiAgZm9udC1mYW1pbHk6IFJvYm90bywgXCJIZWx2ZXRpY2EgTmV1ZVwiLCBzYW5zLXNlcmlmO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/map/map.component.scss":
/*!****************************************!*\
  !*** ./src/app/map/map.component.scss ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@charset \"UTF-8\";\ndiv {\n  width: 100%;\n  height: 100%;\n  cursor: auto;\n}\n#map  {\n  cursor: crosshair;\n  z-index: 1;\n}\n::ng-deep .leaflet-div-icon-violet {\n  background: rgba(130, 77, 241, 0.56);\n  border: 2px solid #370d82;\n  border-radius: 100%;\n}\n::ng-deep .leaflet-div-icon-violet:hover {\n  background: #370d82;\n}\n::ng-deep .leaflet-div-icon-red {\n  background: rgba(241, 73, 76, 0.56);\n  border: 2px solid rgba(255, 21, 19, 0.82);\n  border-radius: 100%;\n}\n::ng-deep .leaflet-div-icon-red:hover {\n  background: rgba(255, 21, 19, 0.82);\n}\n::ng-deep .leaflet-div-icon-yellow {\n  background: rgba(241, 219, 69, 0.56);\n  border: 2px solid rgba(255, 206, 21, 0.82);\n  border-radius: 100%;\n}\n::ng-deep .leaflet-div-icon-green {\n  background: rgba(143, 241, 93, 0.56);\n  border: 2px solid rgba(43, 201, 38, 0.82);\n  border-radius: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFwL21hcC5jb21wb25lbnQuc2NzcyIsIi9ob21lL2tyaXMvRG9jdW1lbnRzL3JlcG9zL2tyaXMvc3VtbWl0cy9zcmMvYXBwL21hcC9tYXAuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsZ0JBQWdCO0FDTWhCO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0FESkY7QUNPQTtFQUNFLGlCQUFBO0VBQ0EsVUFBQTtBREpGO0FDUUU7RUFDRSxvQ0FBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7QURMSjtBQ09JO0VBQ0UsbUJBckJVO0FEZ0JoQjtBQ1NFO0VBQ0UsbUNBQUE7RUFDQSx5Q0FBQTtFQUNBLG1CQUFBO0FEUEo7QUNTSTtFQUNFLG1DQTlCTztBRHVCYjtBQ1dFO0VBQ0Usb0NBQUE7RUFDQSwwQ0FBQTtFQUNBLG1CQUFBO0FEVEo7QUNZRTtFQUNFLG9DQUFBO0VBQ0EseUNBQUE7RUFDQSxtQkFBQTtBRFZKIiwiZmlsZSI6InNyYy9hcHAvbWFwL21hcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBjaGFyc2V0IFwiVVRGLThcIjtcbmRpdiB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGN1cnNvcjogYXV0bztcbn1cblxuI21hcMKgIHtcbiAgY3Vyc29yOiBjcm9zc2hhaXI7XG4gIHotaW5kZXg6IDE7XG59XG5cbjo6bmctZGVlcCAubGVhZmxldC1kaXYtaWNvbi12aW9sZXQge1xuICBiYWNrZ3JvdW5kOiByZ2JhKDEzMCwgNzcsIDI0MSwgMC41Nik7XG4gIGJvcmRlcjogMnB4IHNvbGlkICMzNzBkODI7XG4gIGJvcmRlci1yYWRpdXM6IDEwMCU7XG59XG46Om5nLWRlZXAgLmxlYWZsZXQtZGl2LWljb24tdmlvbGV0OmhvdmVyIHtcbiAgYmFja2dyb3VuZDogIzM3MGQ4Mjtcbn1cbjo6bmctZGVlcCAubGVhZmxldC1kaXYtaWNvbi1yZWQge1xuICBiYWNrZ3JvdW5kOiByZ2JhKDI0MSwgNzMsIDc2LCAwLjU2KTtcbiAgYm9yZGVyOiAycHggc29saWQgcmdiYSgyNTUsIDIxLCAxOSwgMC44Mik7XG4gIGJvcmRlci1yYWRpdXM6IDEwMCU7XG59XG46Om5nLWRlZXAgLmxlYWZsZXQtZGl2LWljb24tcmVkOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogcmdiYSgyNTUsIDIxLCAxOSwgMC44Mik7XG59XG46Om5nLWRlZXAgLmxlYWZsZXQtZGl2LWljb24teWVsbG93IHtcbiAgYmFja2dyb3VuZDogcmdiYSgyNDEsIDIxOSwgNjksIDAuNTYpO1xuICBib3JkZXI6IDJweCBzb2xpZCByZ2JhKDI1NSwgMjA2LCAyMSwgMC44Mik7XG4gIGJvcmRlci1yYWRpdXM6IDEwMCU7XG59XG46Om5nLWRlZXAgLmxlYWZsZXQtZGl2LWljb24tZ3JlZW4ge1xuICBiYWNrZ3JvdW5kOiByZ2JhKDE0MywgMjQxLCA5MywgMC41Nik7XG4gIGJvcmRlcjogMnB4IHNvbGlkIHJnYmEoNDMsIDIwMSwgMzgsIDAuODIpO1xuICBib3JkZXItcmFkaXVzOiAxMDAlO1xufSIsIlxuJGRlZXBCbHVlOiAjMTgyYjMzO1xuJHByaW1hcnlCbHVlOiAjMDA5M2ZmZDE7XG4kcHJpbWFyeVZpb2xldDogIzM3MGQ4MjtcbiRwcmltYXJ5UmVkOiByZ2JhKDI1NSwgMjEsIDE5LCAwLjgyKTtcblxuZGl2IHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgY3Vyc29yOiBhdXRvO1xufVxuXG4jbWFwwqB7XG4gIGN1cnNvcjogY3Jvc3NoYWlyO1xuICB6LWluZGV4OiAxO1xufVxuXG46Om5nLWRlZXAge1xuICAubGVhZmxldC1kaXYtaWNvbi12aW9sZXQge1xuICAgIGJhY2tncm91bmQ6IHJnYmEoMTMwLCA3NywgMjQxLCAwLjU2KTtcbiAgICBib3JkZXI6IDJweCBzb2xpZCAkcHJpbWFyeVZpb2xldDtcbiAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xuXG4gICAgJjpob3ZlciB7XG4gICAgICBiYWNrZ3JvdW5kOiAkcHJpbWFyeVZpb2xldDtcbiAgICB9XG4gIH1cblxuICAubGVhZmxldC1kaXYtaWNvbi1yZWQge1xuICAgIGJhY2tncm91bmQ6IHJnYmEoMjQxLCA3MywgNzYsIDAuNTYpO1xuICAgIGJvcmRlcjogMnB4IHNvbGlkICRwcmltYXJ5UmVkO1xuICAgIGJvcmRlci1yYWRpdXM6IDEwMCU7XG5cbiAgICAmOmhvdmVyIHtcbiAgICAgIGJhY2tncm91bmQ6ICRwcmltYXJ5UmVkO1xuICAgIH1cbiAgfVxuXG4gIC5sZWFmbGV0LWRpdi1pY29uLXllbGxvdyB7XG4gICAgYmFja2dyb3VuZDogcmdiYSgyNDEsIDIxOSwgNjksIDAuNTYpO1xuICAgIGJvcmRlcjogMnB4IHNvbGlkIHJnYmEoMjU1LCAyMDYsIDIxLCAwLjgyKTtcbiAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xuICB9XG5cbiAgLmxlYWZsZXQtZGl2LWljb24tZ3JlZW4ge1xuICAgIGJhY2tncm91bmQ6IHJnYmEoMTQzLCAyNDEsIDkzLCAwLjU2KTtcbiAgICBib3JkZXI6IDJweCBzb2xpZCByZ2JhKDQzLCAyMDEsIDM4LCAwLjgyKTtcbiAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xuICB9XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/map/map.component.ts":
/*!**************************************!*\
  !*** ./src/app/map/map.component.ts ***!
  \**************************************/
/*! exports provided: MapComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapComponent", function() { return MapComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! leaflet */ "./node_modules/leaflet/dist/leaflet-src.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(leaflet__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var pngjs_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! pngjs/browser */ "./node_modules/pngjs/browser.js");
/* harmony import */ var pngjs_browser__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(pngjs_browser__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _helper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../helper */ "./src/app/helper.ts");
/* harmony import */ var _gpx_gpx_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../gpx/gpx.service */ "./src/app/gpx/gpx.service.ts");
/* harmony import */ var _overpass_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./overpass.service */ "./src/app/map/overpass.service.ts");
/* harmony import */ var _popup_popup_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./popup/popup.component */ "./src/app/map/popup/popup.component.ts");
/* harmony import */ var _osm_helper__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./osm.helper */ "./src/app/map/osm.helper.ts");
/* harmony import */ var _tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./tiles.endpoints */ "./src/app/map/tiles.endpoints.ts");
/* harmony import */ var _media_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./media.service */ "./src/app/map/media.service.ts");
/* harmony import */ var _routing_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./routing.service */ "./src/app/map/routing.service.ts");
/* harmony import */ var _interfaces__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./interfaces */ "./src/app/map/interfaces.ts");














var MapComponent = /** @class */ (function () {
    function MapComponent(_changeDetectorRef, _injector, _activatedRoute, _router, _componentFactoryResolver, _overpassService, _mediaService, _gpxService, _routingService) {
        this._changeDetectorRef = _changeDetectorRef;
        this._injector = _injector;
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._componentFactoryResolver = _componentFactoryResolver;
        this._overpassService = _overpassService;
        this._mediaService = _mediaService;
        this._gpxService = _gpxService;
        this._routingService = _routingService;
        this._lat = 45.3426746;
        this._lon = 5.77443122;
        this._zoom = 14;
        this._overpass_data = [];
        this.overpassDisplayedNodes = [];
    }
    MapComponent.prototype.ngOnInit = function () {
        var _this = this;
        var baseMaps = {
            'OpenTopoMap': leaflet__WEBPACK_IMPORTED_MODULE_3__["tileLayer"](_tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["tileServers"].openTopoMap.url, _tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["tileServers"].openTopoMap.options),
            'OpenStreetMap': leaflet__WEBPACK_IMPORTED_MODULE_3__["tileLayer"](_tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["tileServers"].openStreetMap.url),
            'Strava': leaflet__WEBPACK_IMPORTED_MODULE_3__["tileLayer"](_tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["tileServers"].strava.url),
            'Mapbox: grey-scale': leaflet__WEBPACK_IMPORTED_MODULE_3__["tileLayer"](_tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["mapboxTiles"].grayscale.url, _tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["mapboxTiles"].grayscale.options),
            'Mapbox: streets': leaflet__WEBPACK_IMPORTED_MODULE_3__["tileLayer"](_tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["mapboxTiles"].streets.url, _tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["mapboxTiles"].streets.options),
            'Mapbox: satellite': leaflet__WEBPACK_IMPORTED_MODULE_3__["tileLayer"](_tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["mapboxTiles"].satellite.url, _tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["mapboxTiles"].satellite.options),
            'Mapbox: strava': leaflet__WEBPACK_IMPORTED_MODULE_3__["tileLayer"](_tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["mapboxTiles"].strava.url, _tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["mapboxTiles"].strava.options),
        };
        var overlayMaps = {
            'Waymarked trail': leaflet__WEBPACK_IMPORTED_MODULE_3__["tileLayer"](_tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["overlay"].waymarkedtrail.url),
            'Suunto AllTrails': leaflet__WEBPACK_IMPORTED_MODULE_3__["tileLayer"](Object(_tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["suuntoHeatmap"])('AllTrails'), _tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["overlay"].suunto.options),
            'Suunto AllDownhill': leaflet__WEBPACK_IMPORTED_MODULE_3__["tileLayer"](Object(_tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["suuntoHeatmap"])('AllDownhill'), _tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["overlay"].suunto.options),
            'Suunto Running': leaflet__WEBPACK_IMPORTED_MODULE_3__["tileLayer"](Object(_tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["suuntoHeatmap"])('Running'), _tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["overlay"].suunto.options),
            'Suunto TrailRunning': leaflet__WEBPACK_IMPORTED_MODULE_3__["tileLayer"](Object(_tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["suuntoHeatmap"])('TrailRunning'), _tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["overlay"].suunto.options),
            'Suunto Mountaineering': leaflet__WEBPACK_IMPORTED_MODULE_3__["tileLayer"](Object(_tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["suuntoHeatmap"])('Mountaineering'), _tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["overlay"].suunto.options),
        };
        this.map = leaflet__WEBPACK_IMPORTED_MODULE_3__["map"](this.mapElement.nativeElement, {
            center: [this._lat, this._lon],
            zoom: this._zoom,
            layers: [baseMaps.OpenTopoMap],
            zoomControl: true,
        });
        this._routingService.initMap(this.map);
        leaflet__WEBPACK_IMPORTED_MODULE_3__["control"].layers(baseMaps, overlayMaps).addTo(this.map);
        this._centerMapFromFragment();
        this.map.on('moveend', function (e) {
            _this._navigate();
            _this._onMovingMap();
        });
        // this.map.on('click', (e: any) => {
        //   console.log('Lat, Lon : ' + e.latlng.lat + ', ' + e.latlng.lng);
        //   // const baseUrl = suuntoHeatmap('AllTrails');
        //   const baseUrl = suuntoHeatmap('AllTrails');
        //   const zoom = 14;
        //
        //   const tileRef = {
        //     y: latTotile(e.latlng.lat, zoom),
        //     x: lngTotile(e.latlng.lng, zoom),
        //     zoom: zoom,
        //   };
        //
        //   const url = getTileUrl(baseUrl, tileRef);
        //
        //   const tileBounds = getTileBounds(tileRef);
        //
        //   this._mediaService.getImage(url)
        //   .subscribe((arrayBuffer) => {
        //     new PNG({ filterType: 4 }).parse(arrayBuffer, (error, data: { width: number, height: number, data: number[] }) => {
        //       const gamma = this._mediaService.getGamma(e.latlng.lat, e.latlng.lng, tileBounds, data);
        //       console.log('gamma', gamma);
        //     });
        //   });
        // });
        // this._getGamma(128445436);
        // this._getGamma(110332196);
        // this.map.on('baselayerchange', (e) => {
        //   console.log((e as any).layer);
        // });
        this.map.on('keypress', function () {
            // this._sacScale();
            // this._printPeaks();
            _this._wayPopularity();
            // this._overpassService.getPeaks(this.map.getBounds(), Natural.Saddle)
            // .subscribe((peaks) => {
            //   console.log('data', peaks);
            //   for (const peak of peaks) {
            //     if (peak.tags && peak.tags.name) {
            //       console.log('', peak.tags.name);
            //     }
            //   }
            // });
        });
        this.map.on('click', function (e) { return _this._routingService
            .navigate(_this.map, [e.latlng.lat, e.latlng.lng], _interfaces__WEBPACK_IMPORTED_MODULE_13__["ClickType"].Left, null); });
        this.map.on('contextmenu', function (e) { return _this._routingService
            .navigate(_this.map, [e.latlng.lat, e.latlng.lng], _interfaces__WEBPACK_IMPORTED_MODULE_13__["ClickType"].Right, null); });
        this.map.on('mousemove', function (e) { return _this._routingService
            .mouseOverPolyline(_this.map, [e.latlng.lat, e.latlng.lng]); });
        // this._gpxService.getGeoJSON('assets/1.gpx')
        this._gpxService.getGeoJSON('assets/diag.gpx')
            .subscribe(function (data) {
            var coordinates = data.features['0'].geometry.coordinates;
            var latlngs = coordinates.map(function (c) { return [c[1], c[0]]; });
            var elevation = coordinates.map(function (c) { return c[2]; });
            leaflet__WEBPACK_IMPORTED_MODULE_3__["polyline"](latlngs).addTo(_this.map);
        });
    };
    MapComponent.prototype.ngOnChanges = function () {
    };
    MapComponent.prototype._navigate = function () {
        var z = this.map.getZoom();
        var center = this.map.getCenter();
        var lat = Object(_helper__WEBPACK_IMPORTED_MODULE_5__["round"])(center.lat, 4);
        var lng = Object(_helper__WEBPACK_IMPORTED_MODULE_5__["round"])(center.lng, 4);
        // this.map.eachLayer((layer) => {
        //   if (layer instanceof L.TileLayer && (layer as any)._url) {
        //     console.log('layer', layer);
        //   }
        // });
        this._router.navigate([''], {
            fragment: "map=" + z + "/" + lat + "/" + lng,
            queryParams: {
            // 'layer': 'test',
            },
        });
    };
    MapComponent.prototype._onMovingMap = function () {
        if (this.activePOI.includes('water')) {
            console.log('this.activePOI', this.activePOI);
            this._printPOI();
        }
        else {
            this._clearPOI();
        }
    };
    // private _onMapClick(e) {
    //   console.log('Lat, Lon : ' + e.latlng.lat + ', ' + e.latlng.lng);
    //   const baseUrl = suuntoHeatmap('AllTrails');
    //   const zoom = 14;
    //
    //   const url = getTileUrl(baseUrl, {
    //     y: latTotile(e.latlng.lat, zoom),
    //     x: lngTotile(e.latlng.lng, zoom),
    //     zoom: zoom,
    //   });
    //
    //   console.log('url', this._overpassService);
    //   this._overpassService.getImage(url)
    //   .subscribe((arrayBuffer) => {
    //     new PNG({ filterType: 4 }).parse(arrayBuffer, (error, data) => {
    //       console.log('data', data.data.length);
    //       console.log('data 2', data.width * data.height * 4);
    //     });
    //   });
    // }
    MapComponent.prototype._getGamma = function (way, displayTiles) {
        var _this = this;
        var e_1, _a, e_2, _b;
        if (displayTiles === void 0) { displayTiles = false; }
        var tilesMap = new Map();
        if (way.latlngs) {
            var latlngs_2 = way.latlngs;
            try {
                for (var latlngs_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__values"](latlngs_2), latlngs_1_1 = latlngs_1.next(); !latlngs_1_1.done; latlngs_1_1 = latlngs_1.next()) {
                    var latlng = latlngs_1_1.value;
                    var t = Object(_osm_helper__WEBPACK_IMPORTED_MODULE_9__["coordsToTile"])(latlng[0], latlng[1], 14);
                    var s = t.zoom + "/" + t.x + "/" + t.y;
                    if (!tilesMap.has(s)) {
                        tilesMap.set(s, t);
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (latlngs_1_1 && !latlngs_1_1.done && (_a = latlngs_1.return)) _a.call(latlngs_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
            // const baseUrl = suuntoHeatmap('AllTrails');
            var baseUrl = Object(_tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["suuntoHeatmap"])('Running');
            var sources = [];
            try {
                for (var _c = tslib__WEBPACK_IMPORTED_MODULE_0__["__values"](Array.from(tilesMap.keys())), _d = _c.next(); !_d.done; _d = _c.next()) {
                    var key = _d.value;
                    var tile = tilesMap.get(key);
                    var url = Object(_osm_helper__WEBPACK_IMPORTED_MODULE_9__["getTileUrl"])(baseUrl, tile);
                    sources.push({
                        url: url,
                        tile: tile,
                    });
                    if (displayTiles) {
                        var tileBounds = Object(_osm_helper__WEBPACK_IMPORTED_MODULE_9__["getTileBounds"])(tile);
                        leaflet__WEBPACK_IMPORTED_MODULE_3__["imageOverlay"](url, tileBounds).addTo(this.map);
                    }
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (_d && !_d.done && (_b = _c.return)) _b.call(_c);
                }
                finally { if (e_2) throw e_2.error; }
            }
            this._mediaService.requestDataFromMultipleSources(sources)
                .subscribe(function (tiles) {
                var e_3, _a;
                var promises = [];
                var _loop_1 = function (tile) {
                    var promise = new Promise(function (res) {
                        new pngjs_browser__WEBPACK_IMPORTED_MODULE_4__["PNG"]({ filterType: 4 }).parse(tile.arrayBuffer, function (error, data) {
                            var e_4, _a;
                            var tileBounds = Object(_osm_helper__WEBPACK_IMPORTED_MODULE_9__["getTileBounds"])(tile.tile);
                            var latlngsInTile = Object(_osm_helper__WEBPACK_IMPORTED_MODULE_9__["filterLngLatInTile"])(latlngs_2, tileBounds);
                            var g = 0;
                            try {
                                for (var latlngsInTile_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__values"](latlngsInTile), latlngsInTile_1_1 = latlngsInTile_1.next(); !latlngsInTile_1_1.done; latlngsInTile_1_1 = latlngsInTile_1.next()) {
                                    var tuple = latlngsInTile_1_1.value;
                                    g += _this._mediaService.getGamma(tuple[0], tuple[1], tileBounds, data);
                                }
                            }
                            catch (e_4_1) { e_4 = { error: e_4_1 }; }
                            finally {
                                try {
                                    if (latlngsInTile_1_1 && !latlngsInTile_1_1.done && (_a = latlngsInTile_1.return)) _a.call(latlngsInTile_1);
                                }
                                finally { if (e_4) throw e_4.error; }
                            }
                            if (isNaN(g)) {
                                throw new Error("Tile: " + tile + " is nan, way " + way.id);
                            }
                            res(g);
                        });
                    });
                    promises.push(promise);
                };
                try {
                    for (var tiles_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__values"](tiles), tiles_1_1 = tiles_1.next(); !tiles_1_1.done; tiles_1_1 = tiles_1.next()) {
                        var tile = tiles_1_1.value;
                        _loop_1(tile);
                    }
                }
                catch (e_3_1) { e_3 = { error: e_3_1 }; }
                finally {
                    try {
                        if (tiles_1_1 && !tiles_1_1.done && (_a = tiles_1.return)) _a.call(tiles_1);
                    }
                    finally { if (e_3) throw e_3.error; }
                }
                Promise.all(promises)
                    .then(function (gammas) {
                    var gamma = gammas.reduce(function (a, b) { return a + b; }, 0) / latlngs_2.length / 256;
                    var polylineOptions = {
                        color: Object(_helper__WEBPACK_IMPORTED_MODULE_5__["rgbToHex"])(Object(_helper__WEBPACK_IMPORTED_MODULE_5__["pickInterpolatedColor"])([1, 0, 0], [0, 0, 1], gamma)),
                        opacity: 1,
                    };
                    var distance = Object(_helper__WEBPACK_IMPORTED_MODULE_5__["round"])(Object(_osm_helper__WEBPACK_IMPORTED_MODULE_9__["getDistance"])(way), 2);
                    var hyperlink = "<a href=\"" + Object(_osm_helper__WEBPACK_IMPORTED_MODULE_9__["osmEleUrl"])(_interfaces__WEBPACK_IMPORTED_MODULE_13__["OsmType"].Way, way.id) + "\" target=\"_blank\">OSM: " + way.id + "<a> ";
                    var popupContent = hyperlink + (" distance: " + distance + " km \n") + (gamma * 100 + " %") + JSON.stringify(way.tags, null, 2);
                    leaflet__WEBPACK_IMPORTED_MODULE_3__["polyline"](latlngs_2, polylineOptions).addTo(_this.map)
                        .bindPopup(popupContent);
                });
            });
        }
    };
    MapComponent.prototype._printPOI = function () {
        var _this = this;
        if (!this.map || this.map.getZoom() < 9) {
            return;
        }
        this._overpassService.getPOI(this.map.getBounds(), '["amenity"="drinking_water"]')
            .subscribe(function (nodes) {
            var e_5, _a, e_6, _b;
            var toDeleteNodes = _this.overpassDisplayedNodes.filter(function (n) { return !nodes.some(function (node) { return node.id === n.id; }); });
            try {
                for (var toDeleteNodes_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__values"](toDeleteNodes), toDeleteNodes_1_1 = toDeleteNodes_1.next(); !toDeleteNodes_1_1.done; toDeleteNodes_1_1 = toDeleteNodes_1.next()) {
                    var node = toDeleteNodes_1_1.value;
                    _this.map.removeLayer(node.marker);
                }
            }
            catch (e_5_1) { e_5 = { error: e_5_1 }; }
            finally {
                try {
                    if (toDeleteNodes_1_1 && !toDeleteNodes_1_1.done && (_a = toDeleteNodes_1.return)) _a.call(toDeleteNodes_1);
                }
                finally { if (e_5) throw e_5.error; }
            }
            // keep only returned ones
            _this.overpassDisplayedNodes = _this.overpassDisplayedNodes.filter(function (n) { return nodes.some(function (node) { return node.id === n.id; }); });
            var _loop_2 = function (node) {
                if (_this.overpassDisplayedNodes.find(function (n) { return n.id === node.id; })) {
                    return "continue";
                }
                // const polyline = L.Point(way.latlngs, option).addTo(this.map)
                // .bindPopup(popupContent, popupOptions);
                // const factory = this._componentFactoryResolver.resolveComponentFactory(PopupComponent);
                // const compRef: ComponentRef<any> = col.createComponent(factory);
                // this.compRef = compFactory.create(this.injector);
                // https://stackoverflow.com/questions/40922224/angular2-component-into-dynamically-created-element
                var compFactory = _this._componentFactoryResolver.resolveComponentFactory(_popup_popup_component__WEBPACK_IMPORTED_MODULE_8__["PopupComponent"]);
                _this.compRef = compFactory.create(_this._injector);
                var hyperlink = "<a href=\"" + Object(_osm_helper__WEBPACK_IMPORTED_MODULE_9__["osmEleUrl"])(_interfaces__WEBPACK_IMPORTED_MODULE_13__["OsmType"].Node, node.id) + "\" target=\"_blank\">OSM: " + node.id + "<a> ";
                var popupContent = hyperlink + JSON.stringify(node.tags, null, 2);
                // TODO not working
                _this.compRef.instance.test = popupContent;
                var div = document.createElement('div');
                div.appendChild(_this.compRef.location.nativeElement);
                // this._changeDetectorRef.detectChanges();
                // const { icon: customIcon }
                var marker = leaflet__WEBPACK_IMPORTED_MODULE_3__["marker"]([node.lat, node.lon]).addTo(_this.map)
                    .bindPopup(popupContent);
                _this.overpassDisplayedNodes.push(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, node, { marker: marker }));
            };
            try {
                for (var nodes_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__values"](nodes), nodes_1_1 = nodes_1.next(); !nodes_1_1.done; nodes_1_1 = nodes_1.next()) {
                    var node = nodes_1_1.value;
                    _loop_2(node);
                }
            }
            catch (e_6_1) { e_6 = { error: e_6_1 }; }
            finally {
                try {
                    if (nodes_1_1 && !nodes_1_1.done && (_b = nodes_1.return)) _b.call(nodes_1);
                }
                finally { if (e_6) throw e_6.error; }
            }
        });
    };
    MapComponent.prototype._printPeaks = function () {
        var _this = this;
        if (this.map.getZoom() < 9) {
            return;
        }
        this._overpassService.getPeaks(this.map.getBounds(), _overpass_service__WEBPACK_IMPORTED_MODULE_7__["Natural"].Peak)
            .subscribe(function (nodes) {
            var e_7, _a;
            try {
                for (var nodes_2 = tslib__WEBPACK_IMPORTED_MODULE_0__["__values"](nodes), nodes_2_1 = nodes_2.next(); !nodes_2_1.done; nodes_2_1 = nodes_2.next()) {
                    var node = nodes_2_1.value;
                    // const polyline = L.Point(way.latlngs, option).addTo(this.map)
                    // .bindPopup(popupContent, popupOptions);
                    // const factory = this._componentFactoryResolver.resolveComponentFactory(PopupComponent);
                    // const compRef: ComponentRef<any> = col.createComponent(factory);
                    // this.compRef = compFactory.create(this.injector);
                    // https://stackoverflow.com/questions/40922224/angular2-component-into-dynamically-created-element
                    var compFactory = _this._componentFactoryResolver.resolveComponentFactory(_popup_popup_component__WEBPACK_IMPORTED_MODULE_8__["PopupComponent"]);
                    _this.compRef = compFactory.create(_this._injector);
                    var hyperlink = "<a href=\"" + Object(_osm_helper__WEBPACK_IMPORTED_MODULE_9__["osmEleUrl"])(_interfaces__WEBPACK_IMPORTED_MODULE_13__["OsmType"].Node, node.id) + "\" target=\"_blank\">OSM: " + node.id + "<a> ";
                    var popupContent = hyperlink + JSON.stringify(node.tags, null, 2);
                    // TODO not working
                    _this.compRef.instance.test = popupContent;
                    var div = document.createElement('div');
                    div.appendChild(_this.compRef.location.nativeElement);
                    // this._changeDetectorRef.detectChanges();
                    // const { icon: customIcon }
                    leaflet__WEBPACK_IMPORTED_MODULE_3__["marker"]([node.lat, node.lon]).addTo(_this.map)
                        .bindPopup(popupContent);
                }
            }
            catch (e_7_1) { e_7 = { error: e_7_1 }; }
            finally {
                try {
                    if (nodes_2_1 && !nodes_2_1.done && (_a = nodes_2.return)) _a.call(nodes_2);
                }
                finally { if (e_7) throw e_7.error; }
            }
        });
    };
    MapComponent.prototype._centerMapFromFragment = function () {
        var _this = this;
        this._activatedRoute.fragment
            .subscribe(function (fragment) {
            var re = new RegExp(/(\d*)\/([\d-\.]*)\/([\d-\.]*)/);
            var found = re.exec(fragment);
            if (found && found.length) {
                _this._zoom = +found[1];
                _this._lat = +found[2];
                _this._lon = +found[3];
            }
            _this.map.setView([_this._lat, _this._lon], _this._zoom);
        });
    };
    MapComponent.prototype._wayPopularity = function () {
        var _this = this;
        if (this.map.getZoom() < 13) {
            console.log('only below zoom 13');
            return;
        }
        console.log('dbl click');
        this._overpassService.getWays('["highway"]', this.map.getBounds())
            .subscribe(function (ways) {
            var e_8, _a;
            try {
                for (var ways_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__values"](ways), ways_1_1 = ways_1.next(); !ways_1_1.done; ways_1_1 = ways_1.next()) {
                    var way = ways_1_1.value;
                    console.log('way.id', way.id);
                    _this._getGamma(way);
                }
            }
            catch (e_8_1) { e_8 = { error: e_8_1 }; }
            finally {
                try {
                    if (ways_1_1 && !ways_1_1.done && (_a = ways_1.return)) _a.call(ways_1);
                }
                finally { if (e_8) throw e_8.error; }
            }
        });
    };
    MapComponent.prototype._sacScale = function () {
        var _this = this;
        if (this.map.getZoom() < 13) {
            return;
        }
        // private _extractLatLng(data: IFit): LatLngTuple[] {
        //     return data.content.records.map(r => [r.position_lat, r.position_long]).filter(r => !!r[0]);
        //   }
        var popupOptions = {
            maxWidth: 500,
            className: 'popupStyle',
            closeButton: true,
        };
        var polylineOptions = {
            color: 'red',
        };
        this._overpassService.getWays(this.state, 
        // '["trail_visibility"]',
        this.map.getBounds())
            .subscribe(function (ways) {
            var e_9, _a;
            console.log('query', _this.state);
            console.log('', ways);
            var highwayTypes = new Map();
            console.log('this._overpass_data.length', _this._overpass_data.length);
            var _loop_3 = function (way) {
                if (_this._overpass_data.includes(way.id)) {
                    return "continue";
                }
                if (way.latlngs) {
                    _this._overpass_data.push(way.id);
                    var distance = Object(_helper__WEBPACK_IMPORTED_MODULE_5__["round"])(Object(_osm_helper__WEBPACK_IMPORTED_MODULE_9__["getDistance"])(way), 2);
                    var hyperlink = "<a href=\"" + Object(_osm_helper__WEBPACK_IMPORTED_MODULE_9__["osmEleUrl"])(_interfaces__WEBPACK_IMPORTED_MODULE_13__["OsmType"].Way, way.id) + "\" target=\"_blank\">OSM: " + way.id + "<a> ";
                    var popupContent = hyperlink + (" distance: " + distance + " km \n") + JSON.stringify(way.tags, null, 2);
                    var option = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, polylineOptions);
                    if (way.tags && way.tags.sac_scale) {
                        switch (way.tags.sac_scale) {
                            case 'hiking':
                                option.color = 'green';
                                break;
                            case 'mountain_hiking':
                                option.color = 'blue';
                                break;
                            case 'demanding_mountain_hiking':
                                option.color = 'blue';
                                option.dashArray = '10 5';
                                break;
                            case 'alpine_hiking':
                                option.color = 'red';
                                break;
                            case 'demanding_alpine_hiking':
                                option.color = 'black';
                                break;
                            case 'difficult_alpine_hiking':
                                option.color = 'black';
                                option.dashArray = '10 5';
                                break;
                        }
                    }
                    if (way.tags && way.tags.trail_visibility) {
                        switch (way.tags.trail_visibility) {
                            case 'excellent':
                                option.color = 'green';
                                break;
                            case 'good':
                                option.color = 'blue';
                                break;
                            case 'intermediate':
                                option.color = 'blue';
                                option.dashArray = '10 5';
                                break;
                            case 'bad':
                                option.color = 'red';
                                break;
                            case 'horrible':
                                option.color = 'black';
                                break;
                            case 'no':
                                option.color = 'black';
                                option.dashArray = '10 5';
                                break;
                        }
                    }
                    if (way.tags && way.tags.highway) {
                        var highway = way.tags.highway;
                        if (highwayTypes.has(highway)) {
                            highwayTypes.set(highway, highwayTypes.get(highway) + 1);
                        }
                        else {
                            highwayTypes.set(highway, 1);
                        }
                    }
                    var polyline_1 = leaflet__WEBPACK_IMPORTED_MODULE_3__["polyline"](way.latlngs, option).addTo(_this.map)
                        .bindPopup(popupContent, popupOptions);
                    polyline_1.on('mouseover', function (e) {
                        polyline_1.setStyle({
                            opacity: 0.5,
                        });
                    });
                    polyline_1.on('mouseout', function (e) {
                        polyline_1.setStyle({
                            opacity: 1,
                        });
                    });
                }
            };
            try {
                for (var ways_2 = tslib__WEBPACK_IMPORTED_MODULE_0__["__values"](ways), ways_2_1 = ways_2.next(); !ways_2_1.done; ways_2_1 = ways_2.next()) {
                    var way = ways_2_1.value;
                    _loop_3(way);
                }
            }
            catch (e_9_1) { e_9 = { error: e_9_1 }; }
            finally {
                try {
                    if (ways_2_1 && !ways_2_1.done && (_a = ways_2.return)) _a.call(ways_2);
                }
                finally { if (e_9) throw e_9.error; }
            }
            console.log('highwayTypes', highwayTypes);
        });
    };
    MapComponent.prototype._clearPOI = function (type) {
        var e_10, _a;
        try {
            for (var _b = tslib__WEBPACK_IMPORTED_MODULE_0__["__values"](this.overpassDisplayedNodes), _c = _b.next(); !_c.done; _c = _b.next()) {
                var node = _c.value;
                this.map.removeLayer(node.marker);
            }
        }
        catch (e_10_1) { e_10 = { error: e_10_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_10) throw e_10.error; }
        }
        this.overpassDisplayedNodes = [];
    };
    MapComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injector"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ComponentFactoryResolver"] },
        { type: _overpass_service__WEBPACK_IMPORTED_MODULE_7__["OverpassService"] },
        { type: _media_service__WEBPACK_IMPORTED_MODULE_11__["MediaService"] },
        { type: _gpx_gpx_service__WEBPACK_IMPORTED_MODULE_6__["GpxService"] },
        { type: _routing_service__WEBPACK_IMPORTED_MODULE_12__["RoutingService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], MapComponent.prototype, "state", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], MapComponent.prototype, "activePOI", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('map', { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], MapComponent.prototype, "mapElement", void 0);
    MapComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-map',
            template: __webpack_require__(/*! raw-loader!./map.component.html */ "./node_modules/raw-loader/index.js!./src/app/map/map.component.html"),
            styles: [__webpack_require__(/*! ./map.component.scss */ "./src/app/map/map.component.scss"), __webpack_require__(/*! ./leaflet.popup.scss */ "./src/app/map/leaflet.popup.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injector"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ComponentFactoryResolver"],
            _overpass_service__WEBPACK_IMPORTED_MODULE_7__["OverpassService"],
            _media_service__WEBPACK_IMPORTED_MODULE_11__["MediaService"],
            _gpx_gpx_service__WEBPACK_IMPORTED_MODULE_6__["GpxService"],
            _routing_service__WEBPACK_IMPORTED_MODULE_12__["RoutingService"]])
    ], MapComponent);
    return MapComponent;
}());



/***/ }),

/***/ "./src/app/map/media.service.ts":
/*!**************************************!*\
  !*** ./src/app/map/media.service.ts ***!
  \**************************************/
/*! exports provided: MediaService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MediaService", function() { return MediaService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");





var MediaService = /** @class */ (function () {
    function MediaService(_httpClient) {
        this._httpClient = _httpClient;
        this._tileRequests = [];
        this._tilesMap = new Map();
    }
    MediaService.prototype.requestDataFromMultipleSources = function (sources) {
        var _this = this;
        var e_1, _a;
        var responses = [];
        var _loop_1 = function (source) {
            if (this_1._tilesMap.has(source.url)) {
                responses.push(Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(this_1._tilesMap.get(source.url)));
            }
            else {
                responses.push(this_1.getImage(source.url)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (res) {
                    var tileBuffer = {
                        tile: source.tile,
                        arrayBuffer: res,
                    };
                    _this._tilesMap.set(source.url, tileBuffer);
                    return tileBuffer;
                }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) { return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(err); })));
            }
        };
        var this_1 = this;
        try {
            for (var sources_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__values"](sources), sources_1_1 = sources_1.next(); !sources_1_1.done; sources_1_1 = sources_1.next()) {
                var source = sources_1_1.value;
                _loop_1(source);
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (sources_1_1 && !sources_1_1.done && (_a = sources_1.return)) _a.call(sources_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["forkJoin"])(responses);
    };
    MediaService.prototype.getImage = function (imageUrl) {
        return this._httpClient.get(imageUrl, {
            observe: 'response',
            responseType: 'blob',
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["mergeMap"])(function (res) {
            var blob = new Blob([res.body], { type: res.headers.get('Content-Type') });
            var arrayBuffer = new Response(blob).arrayBuffer();
            return arrayBuffer;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) { return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(err); }));
    };
    // TODO TU
    MediaService.prototype.getGamma = function (lat, lng, tileBounds, data) {
        var north = tileBounds[0][0];
        var south = tileBounds[1][0];
        var west = tileBounds[0][1];
        var east = tileBounds[1][1];
        var x = Math.floor(Math.abs(west - lng) * data.width / Math.abs(west - east));
        var y = Math.floor(Math.abs(north - lat) * data.height / Math.abs(north - south));
        var i = y * data.width + x;
        // const pixel = {
        //   r: data.data[i * 4],
        //   v: data.data[i * 4 + 1],
        //   b: data.data[i * 4 + 2],
        //   g: data.data[i * 4 + 3],
        // };
        return data.data[i * 4 + 3];
    };
    MediaService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
    ]; };
    MediaService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
            providedIn: 'root',
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], MediaService);
    return MediaService;
}());



/***/ }),

/***/ "./src/app/map/osm.helper.ts":
/*!***********************************!*\
  !*** ./src/app/map/osm.helper.ts ***!
  \***********************************/
/*! exports provided: osmEleUrl, getDistance, getDistanceLatLngs, getTotalAscent, lngTotile, latTotile, coordsToTile, nextTile, TileToCoords, getTileUrl, getTileBounds, extractLatLngsFromGeoJson, extractElevationFromGeoJson, getLatLngBoundsFromGeoJson, getLatLngBounds, filterLngLatInTile */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "osmEleUrl", function() { return osmEleUrl; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDistance", function() { return getDistance; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDistanceLatLngs", function() { return getDistanceLatLngs; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTotalAscent", function() { return getTotalAscent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lngTotile", function() { return lngTotile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "latTotile", function() { return latTotile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "coordsToTile", function() { return coordsToTile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "nextTile", function() { return nextTile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TileToCoords", function() { return TileToCoords; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTileUrl", function() { return getTileUrl; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTileBounds", function() { return getTileBounds; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "extractLatLngsFromGeoJson", function() { return extractLatLngsFromGeoJson; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "extractElevationFromGeoJson", function() { return extractElevationFromGeoJson; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getLatLngBoundsFromGeoJson", function() { return getLatLngBoundsFromGeoJson; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getLatLngBounds", function() { return getLatLngBounds; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "filterLngLatInTile", function() { return filterLngLatInTile; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! leaflet */ "./node_modules/leaflet/dist/leaflet-src.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(leaflet__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _leaflet_helper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./leaflet.helper */ "./src/app/map/leaflet.helper.ts");



var osmEleUrl = function (type, id) { return "https://www.openstreetmap.org/" + type + "/" + id; };
function getDistance(way) {
    if (!way.latlngs) {
        return null;
    }
    return way.latlngs.reduce(function (acc, latlng, index) {
        if (index === 0) {
            return 0;
        }
        return acc + Object(_leaflet_helper__WEBPACK_IMPORTED_MODULE_2__["customHaversine"])(way.latlngs[index - 1], latlng);
    }, 0);
}
function getDistanceLatLngs(latLngs) {
    if (!latLngs) {
        return null;
    }
    return latLngs.reduce(function (acc, latlng, index) {
        if (index === 0) {
            return 0;
        }
        return acc + Object(_leaflet_helper__WEBPACK_IMPORTED_MODULE_2__["customHaversine"])(latLngs[index - 1], latlng);
    }, 0);
}
function getTotalAscent(elevations) {
    if (!elevations) {
        return null;
    }
    return elevations.reduce(function (acc, ele, index) {
        if (index === 0) {
            return [0, 0];
        }
        var diff = ele - elevations[index - 1];
        acc[0] += diff > 0 ? diff : 0;
        acc[1] += diff < 0 ? diff : 0;
        return acc;
    }, [0, 0]);
}
function lngTotile(lng, zoom) {
    return Math.floor((lng + 180) / 360 * Math.pow(2, zoom));
}
function latTotile(lat, zoom) {
    return Math.floor((1 - Math.log(Math.tan(lat * Math.PI / 180) + 1 / Math.cos(lat * Math.PI / 180)) / Math.PI) / 2 * Math.pow(2, zoom));
}
function coordsToTile(lat, lng, zoom) {
    return {
        zoom: zoom,
        y: latTotile(lat, zoom),
        x: lngTotile(lng, zoom),
    };
}
function tileToLng(x, z) {
    return (x / Math.pow(2, z) * 360 - 180);
}
function tileToLat(y, z) {
    var n = Math.PI - 2 * Math.PI * y / Math.pow(2, z);
    return (180 / Math.PI * Math.atan(0.5 * (Math.exp(n) - Math.exp(-n))));
}
function nextTile(tile) {
    return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, tile, { x: tile.x + 1, y: tile.y + 1 });
}
function TileToCoords(tile) {
    return [
        tileToLat(tile.y, tile.zoom),
        tileToLng(tile.x, tile.zoom),
    ];
}
function getTileUrl(tileServer, tile) {
    return tileServer
        .replace('{z}', tile.zoom.toString())
        .replace('{x}', tile.x.toString())
        .replace('{y}', tile.y.toString());
}
function getTileBounds(tile) {
    var coordsUpperLeft = TileToCoords(tile);
    var coordsBottomRight = TileToCoords(nextTile(tile));
    return [
        [coordsUpperLeft[0], coordsUpperLeft[1]],
        [coordsBottomRight[0], coordsBottomRight[1]],
    ];
}
function extractLatLngsFromGeoJson(data) {
    return data.features['0'].geometry.coordinates.map(function (c) { return [c[1], c[0]]; });
}
function extractElevationFromGeoJson(data) {
    return data.features['0'].geometry.coordinates.map(function (c) { return c[2]; });
}
function getLatLngBoundsFromGeoJson(data, extend) {
    if (extend === void 0) { extend = 0; }
    return getLatLngBounds(extractLatLngsFromGeoJson(data), extend);
}
function getLatLngBounds(tuples, extend) {
    if (extend === void 0) { extend = 0; }
    var lats = tuples.map(function (t) { return t[0]; });
    var lngs = tuples.map(function (t) { return t[1]; });
    return new leaflet__WEBPACK_IMPORTED_MODULE_1__["LatLngBounds"]([Math.min.apply(Math, tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"](lats)) - extend, Math.min.apply(Math, tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"](lngs)) - extend], [Math.max.apply(Math, tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"](lats)) + extend, Math.max.apply(Math, tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"](lngs)) + extend]);
}
function filterLngLatInTile(tuples, bounds) {
    var north = bounds[0][0];
    var south = bounds[1][0];
    var west = bounds[0][1];
    var east = bounds[1][1];
    return tuples.filter(function (t) { return south < t[0] && t[0] < north && west < t[1] && t[1] < east; });
}


/***/ }),

/***/ "./src/app/map/overpass.service.ts":
/*!*****************************************!*\
  !*** ./src/app/map/overpass.service.ts ***!
  \*****************************************/
/*! exports provided: Natural, pointOfInterestOnRoute, OverpassService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Natural", function() { return Natural; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pointOfInterestOnRoute", function() { return pointOfInterestOnRoute; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OverpassService", function() { return OverpassService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _helper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../helper */ "./src/app/helper.ts");
/* harmony import */ var _interfaces__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./interfaces */ "./src/app/map/interfaces.ts");






var toBbox = function (bounds, n) {
    if (n) {
        return Object(_helper__WEBPACK_IMPORTED_MODULE_4__["round"])(bounds.getSouth(), n) + "," + Object(_helper__WEBPACK_IMPORTED_MODULE_4__["round"])(bounds.getWest(), n) + "," + Object(_helper__WEBPACK_IMPORTED_MODULE_4__["round"])(bounds.getNorth(), n) + "," + Object(_helper__WEBPACK_IMPORTED_MODULE_4__["round"])(bounds.getEast(), n);
    }
    return bounds.getSouth() + "," + bounds.getWest() + "," + bounds.getNorth() + "," + bounds.getEast();
};
var precision = 3;
var Natural;
(function (Natural) {
    Natural["Peak"] = "peak";
    Natural["Saddle"] = "saddle";
})(Natural || (Natural = {}));
var pointOfInterestOnRoute = [
    '["natural"="saddle"]',
    '["natural"="peak"]',
    '["natural"="volcano"]',
    '["tourism"="viewpoint"]["map_type"!="toposcope"]',
    '["map_type"="toposcope"]',
    '["waterway"="waterfall"]',
    '["amenity"="drinking_water"]',
];
//   'saddle':'node["natural"="saddle"]',
//     'peak':'node["natural"="peak"]',
// # 'peak':'node["natural"="volcano"]',
//     'waterfall':'node["waterway"="waterfall"]',
// # 'waterfall':'node["natural"="waterfall"]',
//     'guidepost':'node["information"="guidepost"]',
//     'cave_entrance':'node["natural"="cave_entrance"]',
//     'viewpoint':'node["tourism"="viewpoint"]["map_type"!="toposcope"]',
//     'toposcope':'node["map_type"="toposcope"]',
//     'drinking_water':'node["amenity"="drinking_water"]',
//     'fountain':'node["amenity"="fountain"]',
//     'alpine_hut':'node["tourism"="alpine_hut"]',
//     'wilderness_hut':'node["tourism"="wilderness_hut"]',
//     'shelter':'node["amenity"="shelter"]',
//     'tree':'node["natural"="tree"]["name"]',
//     'aircraft_wreck':'node["historic"="aircraft_wreck"]["aircraft_wreck"]',
//     'barrier':'node["barrier"]["barrier"!="bollard"]',
//     'chapel':'node["building"="chapel"]',
//     'ford':'node["ford"="yes"]',
//     'ruins':'node["historic"="ruins"]',
//     'castle':'node["historic"="castle"]',
//     'toilets':'node["amenity"="toilets"]',
//     'attraction':'node["tourism"="attraction"]',
//     'spring':'node["natural"="spring"]',
//     'cairn':'node["man_made"="cairn"]',
//     'locality':'node["place"="locality"]',
//     'camp_site':'node["tourism"="camp_site"]',
//     'hostel':'node["tourism"="hostel"]',
//     'hotel':'node["tourism"="hotel"]'
var OverpassService = /** @class */ (function () {
    function OverpassService(_httpClient) {
        this._httpClient = _httpClient;
    }
    OverpassService.prototype.getWays = function (query, bounds) {
        var _this = this;
        return this._fetchOverpassApi("[out:json][timeout:25];(way" + query + "(" + toBbox(bounds, precision) + "););out;>;out skel qt;")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (data) {
            return _this._handleWays(data);
        }));
    };
    OverpassService.prototype.getNodes = function (query, bounds) {
        var _this = this;
        return this._fetchOverpassApi("[out:json][timeout:25];(" + query.map(function (q) { return "node" + q + "(" + toBbox(bounds, precision) + ");"; }).join('') + ");out;>;out skel qt;")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (data) {
            return _this._handleNodes(data);
        }));
    };
    OverpassService.prototype.getWayFromCoord = function (latlng, around) {
        var _this = this;
        if (around === void 0) { around = 1; }
        return this._fetchOverpassApi("[out:json][timeout:25];(way(around:" + around + ", " + latlng.join(',') + "););out;>;out skel qt;")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (data) {
            return _this._handleWays(data);
        }));
    };
    OverpassService.prototype.getPOI = function (bounds, overPassKey) {
        var _this = this;
        return this._fetchOverpassApi("[out:json][timeout:25];(node" + overPassKey + "(" + toBbox(bounds, precision) + "););out;>;out skel qt;")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (data) {
            return _this._handleNodes(data);
        }));
    };
    OverpassService.prototype.getPeaks = function (bounds, type) {
        var _this = this;
        return this._fetchOverpassApi("[out:json][timeout:25];(node[\"natural\"=\"" + type + "\"](" + toBbox(bounds, precision) + "););out;>;out skel qt;")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (data) {
            return _this._handleNodes(data);
        }));
    };
    OverpassService.prototype.getElementById = function (id, osmType) {
        var _this = this;
        return this._fetchOverpassApi("[out:json][timeout:25];(" + osmType + "(" + id + "););out;>;out skel qt;")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (data) {
            switch (osmType) {
                case _interfaces__WEBPACK_IMPORTED_MODULE_5__["OsmType"].Node: {
                    return _this._handleNodes(data)[0];
                }
                case _interfaces__WEBPACK_IMPORTED_MODULE_5__["OsmType"].Way: {
                    return _this._handleWays(data)[0];
                }
            }
        }));
    };
    OverpassService.prototype._fetchOverpassApi = function (overpassQuery) {
        // .replace(/[\s]*/gm, '')
        console.log('overpassQuery', overpassQuery);
        var queryString = encodeURIComponent(overpassQuery);
        // console.log('queryString', overpassQuery);
        // const url = `http://overpass-api.de/api/interpreter?data=${queryString}`;
        var url = "https://overpass.openstreetmap.fr/api/interpreter?data=" + queryString;
        // const url = `https://lz4.overpass-api.de/api/interpreter?data=${queryString}`;
        return this._httpClient.get(url);
    };
    OverpassService.prototype._handleNodes = function (data) {
        return data.elements.map(function (e) { return (tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, e, { latlng: [e.lat, e.lon] })); });
    };
    OverpassService.prototype._handleWays = function (data) {
        var nodes = data.elements.filter(function (e) { return e.type === 'node'; });
        var ways = data.elements.filter(function (e) { return e.type === 'way'; });
        return ways.map(function (way) {
            var output = way;
            output.latlngs = output.nodes.map(function (e) {
                var node = nodes.find(function (n) { return n.id === e; });
                return [node.lat, node.lon];
            });
            return output;
        });
    };
    OverpassService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
    ]; };
    OverpassService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
            providedIn: 'root',
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], OverpassService);
    return OverpassService;
}());



/***/ }),

/***/ "./src/app/map/popup/popup.component.scss":
/*!************************************************!*\
  !*** ./src/app/map/popup/popup.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "div {\n  background: #182b33;\n  color: #ffffff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2tyaXMvRG9jdW1lbnRzL3JlcG9zL2tyaXMvc3VtbWl0cy9zcmMvYXBwL21hcC9wb3B1cC9wb3B1cC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbWFwL3BvcHVwL3BvcHVwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0UsbUJBSFM7RUFJVCxjQUFBO0FDREYiLCJmaWxlIjoic3JjL2FwcC9tYXAvcG9wdXAvcG9wdXAuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIkZGVlcEJsdWU6ICMxODJiMzM7XG5cbmRpdiB7XG4gIGJhY2tncm91bmQ6ICRkZWVwQmx1ZTtcbiAgY29sb3I6ICNmZmZmZmY7XG59XG4iLCJkaXYge1xuICBiYWNrZ3JvdW5kOiAjMTgyYjMzO1xuICBjb2xvcjogI2ZmZmZmZjtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/map/popup/popup.component.ts":
/*!**********************************************!*\
  !*** ./src/app/map/popup/popup.component.ts ***!
  \**********************************************/
/*! exports provided: PopupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PopupComponent", function() { return PopupComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PopupComponent = /** @class */ (function () {
    function PopupComponent() {
        this.test = 'ok';
    }
    PopupComponent.prototype.ngOnInit = function () {
        // this.test = 'loool';
        console.log('this.test', this.test);
    };
    PopupComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-popup',
            template: __webpack_require__(/*! raw-loader!./popup.component.html */ "./node_modules/raw-loader/index.js!./src/app/map/popup/popup.component.html"),
            styles: [__webpack_require__(/*! ./popup.component.scss */ "./src/app/map/popup/popup.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PopupComponent);
    return PopupComponent;
}());



/***/ }),

/***/ "./src/app/map/routing.service.ts":
/*!****************************************!*\
  !*** ./src/app/map/routing.service.ts ***!
  \****************************************/
/*! exports provided: RoutingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoutingService", function() { return RoutingService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! leaflet */ "./node_modules/leaflet/dist/leaflet-src.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(leaflet__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _gpx_gpx_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../gpx/gpx.service */ "./src/app/gpx/gpx.service.ts");
/* harmony import */ var _helper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../helper */ "./src/app/helper.ts");
/* harmony import */ var _interfaces__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./interfaces */ "./src/app/map/interfaces.ts");
/* harmony import */ var _overpass_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./overpass.service */ "./src/app/map/overpass.service.ts");
/* harmony import */ var _osm_helper__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./osm.helper */ "./src/app/map/osm.helper.ts");
/* harmony import */ var _leaflet_media__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./leaflet.media */ "./src/app/map/leaflet.media.ts");
/* harmony import */ var _leaflet_helper__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./leaflet.helper */ "./src/app/map/leaflet.helper.ts");
/* harmony import */ var _b_router_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./b-router.service */ "./src/app/map/b-router.service.ts");
/* harmony import */ var _gpx_helper__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./gpx.helper */ "./src/app/map/gpx.helper.ts");













var circleIcon = function (isFirst, clickType) { return leaflet__WEBPACK_IMPORTED_MODULE_3__["divIcon"]({
    className: isFirst
        ? 'leaflet-div-icon-green'
        : clickType === _interfaces__WEBPACK_IMPORTED_MODULE_6__["ClickType"].Left
            ? 'leaflet-div-icon-red'
            : 'leaflet-div-icon-violet',
}); };
var RoutingService = /** @class */ (function () {
    function RoutingService(_gpxService, _overpassService, _bRouterService, _httpClient) {
        this._gpxService = _gpxService;
        this._overpassService = _overpassService;
        this._bRouterService = _bRouterService;
        this._httpClient = _httpClient;
        this._routingMarkers = [];
        this._redoRoutingMarkers = [];
        this._isReversed = false;
        this._waypointMarkers = [];
        // Optimisation to find associated polyline closest to mouse
        this._mouseOverPolylineId = null;
        this._mouseAdditionalMarker = null;
        this._distanceToVanish = 0.3;
    }
    RoutingService.prototype.initMap = function (lMap) {
        if (!this._map) {
            this._map = lMap;
        }
    };
    RoutingService.prototype.getInfo = function () {
        var distance = this._routingMarkers.reduce(function (acc, marker) {
            return acc + Object(_osm_helper__WEBPACK_IMPORTED_MODULE_8__["getDistanceLatLngs"])(marker.route);
        }, 0);
        var elevation = this._routingMarkers.reduce(function (acc, marker) {
            if (marker && marker.elevation) {
                var ascent = Object(_osm_helper__WEBPACK_IMPORTED_MODULE_8__["getTotalAscent"])(marker.elevation);
                acc[0] += ascent[0];
                acc[1] += ascent[1];
            }
            return acc;
        }, [0, 0]);
        return {
            distance: distance,
            numberOfMarkers: this._routingMarkers.length,
            totalAscent: elevation[0],
            totalDescent: -elevation[1],
            data: {
                elevation: [],
            },
        };
    };
    RoutingService.prototype.canUndoRedo = function (type) {
        if (type === 'undo') {
            return this._routingMarkers.length > 0;
        }
        return this._redoRoutingMarkers.length > 0;
    };
    RoutingService.prototype.undo = function () {
        var lastMarker = this._routingMarkers.pop();
        if (lastMarker) {
            if (lastMarker.polyline) {
                this._map.removeLayer(lastMarker.polyline);
            }
            this._map.removeLayer(lastMarker.marker);
            this._redoRoutingMarkers.push(lastMarker);
        }
    };
    RoutingService.prototype.redo = function () {
        var lastMarker = this._redoRoutingMarkers.pop();
        if (lastMarker) {
            if (lastMarker.polyline) {
                this._map.addLayer(lastMarker.polyline);
            }
            this._map.addLayer(lastMarker.marker);
            this._routingMarkers.push(lastMarker);
        }
    };
    RoutingService.prototype.reverse = function () {
        // Move polyline to the marker ahead (so the marker index 0 has no polyline)
        for (var i = 0; i < this._routingMarkers.length; i++) {
            var marker = this._routingMarkers[i];
            if (i < this._routingMarkers.length - 1) {
                var nextMarker = this._routingMarkers[i + 1];
                marker.polyline = nextMarker.polyline;
                marker.route = nextMarker.route.reverse();
            }
            else {
                marker.polyline = null;
                marker.route = [];
                marker.marker.setIcon(circleIcon(true, marker.type));
            }
            if (i === 0) {
                marker.marker.setIcon(circleIcon(false, marker.type));
            }
        }
        this._routingMarkers.reverse();
        this._isReversed = !this._isReversed;
    };
    RoutingService.prototype.resetRouting = function () {
        var e_1, _a, e_2, _b;
        try {
            for (var _c = tslib__WEBPACK_IMPORTED_MODULE_0__["__values"](this._routingMarkers), _d = _c.next(); !_d.done; _d = _c.next()) {
                var marker = _d.value;
                if (marker.polyline) {
                    this._map.removeLayer(marker.polyline);
                }
                this._map.removeLayer(marker.marker);
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_d && !_d.done && (_a = _c.return)) _a.call(_c);
            }
            finally { if (e_1) throw e_1.error; }
        }
        try {
            for (var _e = tslib__WEBPACK_IMPORTED_MODULE_0__["__values"](this._waypointMarkers), _f = _e.next(); !_f.done; _f = _e.next()) {
                var marker = _f.value;
                this._map.removeLayer(marker.marker);
            }
        }
        catch (e_2_1) { e_2 = { error: e_2_1 }; }
        finally {
            try {
                if (_f && !_f.done && (_b = _e.return)) _b.call(_e);
            }
            finally { if (e_2) throw e_2.error; }
        }
        this._routingMarkers = [];
    };
    RoutingService.prototype.saveAsGpx = function () {
        var completeRoute = this._routingMarkers.reduce(function (route, marker) {
            return route.concat(marker.route);
        }, []);
        Object(_gpx_helper__WEBPACK_IMPORTED_MODULE_12__["convertToGpx"])(completeRoute);
    };
    RoutingService.prototype.navigate = function (lMap, latLng, clickType, previous) {
        var _this = this;
        // Avoid redo
        this._redoRoutingMarkers = [];
        var isFirst = this._routingMarkers.length === 0;
        var newDestinationMarker = leaflet__WEBPACK_IMPORTED_MODULE_3__["marker"](latLng, {
            draggable: true,
            icon: circleIcon(isFirst, clickType),
        }).addTo(lMap);
        // Remove destination
        newDestinationMarker.on('click', function (ev) {
            _this._renavigate(lMap, newDestinationMarker, ev, 'click');
        });
        newDestinationMarker.on('moveend', function (ev) {
            _this._renavigate(lMap, newDestinationMarker, ev, 'moveend');
            _this._removeHoverMarker(lMap);
        });
        newDestinationMarker.on('mouseover', function () {
            _this._removeHoverMarker(lMap);
        });
        var routingMarker = {
            markerLeafletId: newDestinationMarker._leaflet_id,
            latLng: latLng,
            type: clickType,
            marker: newDestinationMarker,
        };
        if (this._routingMarkers.length === 0) {
            this._routingMarkers.push(routingMarker);
            return;
        }
        // TODO Useless for now ... WHy ???
        var previousLatLng = previous ? previous : this._routingMarkers[this._routingMarkers.length - 1].latLng;
        if (clickType === _interfaces__WEBPACK_IMPORTED_MODULE_6__["ClickType"].Right) {
            routingMarker.polyline = leaflet__WEBPACK_IMPORTED_MODULE_3__["polyline"]([previousLatLng, latLng], _leaflet_media__WEBPACK_IMPORTED_MODULE_9__["routingPolylineOptions"]).addTo(lMap);
            routingMarker.route = [previousLatLng, latLng];
            // TODO find a way to get elevation data
            routingMarker.elevation = [null, null];
        }
        else {
            this._bRouterService.fetchRoute(previousLatLng, latLng)
                .subscribe(function (geoJsonData) {
                if (!geoJsonData) {
                    lMap.removeLayer(newDestinationMarker);
                    _this._routingMarkers.pop();
                    return;
                }
                // Check if marker has not been deleted
                if (!_this._routingMarkers.find(function (m) { return m.markerLeafletId === routingMarker.markerLeafletId; })) {
                    console.log('ghost');
                    return;
                }
                routingMarker.route = tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"]([
                    previousLatLng
                ], Object(_osm_helper__WEBPACK_IMPORTED_MODULE_8__["extractLatLngsFromGeoJson"])(geoJsonData));
                routingMarker.polyline = leaflet__WEBPACK_IMPORTED_MODULE_3__["polyline"](routingMarker.route, _leaflet_media__WEBPACK_IMPORTED_MODULE_9__["routingPolylineOptions"])
                    .addTo(lMap)
                    .bindPopup("<pre>" + JSON.stringify(routingMarker.route, null, 2) + "</pre>");
                routingMarker.elevation = Object(_osm_helper__WEBPACK_IMPORTED_MODULE_8__["extractElevationFromGeoJson"])(geoJsonData);
                // routingMarker.polyline.on('click', (e) => {
                //   this._mouseOverPolylineId = (e.target as any)._leaflet_id;
                // });
                routingMarker.polyline.on('mouseover', function (e) {
                    _this._mouseOverPolylineId = e.target._leaflet_id;
                });
                routingMarker.polyline.on('mouseout', function () {
                    _this._mouseOverPolylineId = null;
                });
                // POIs
                _this._addPointOfInterestsOnRoute(lMap, routingMarker.route, newDestinationMarker);
                // Try to guess way ids then recall overpass API to get other data
                var messages = geoJsonData.features['0'].properties.messages;
                var messagesLatLngs = _this._bRouterService.parseCoordsFromBRouterMessages(messages);
                if (messagesLatLngs && messagesLatLngs.length) {
                    _this._overpassService.getWayFromCoord(messagesLatLngs[0])
                        .subscribe(function (ways) {
                        var e_3, _a;
                        try {
                            for (var ways_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__values"](ways), ways_1_1 = ways_1.next(); !ways_1_1.done; ways_1_1 = ways_1.next()) {
                                var way = ways_1_1.value;
                                _this._handleSacScale(lMap, way);
                            }
                        }
                        catch (e_3_1) { e_3 = { error: e_3_1 }; }
                        finally {
                            try {
                                if (ways_1_1 && !ways_1_1.done && (_a = ways_1.return)) _a.call(ways_1);
                            }
                            finally { if (e_3) throw e_3.error; }
                        }
                    });
                }
            });
        }
        this._routingMarkers.push(routingMarker);
    };
    RoutingService.prototype.mouseOverPolyline = function (lMap, mouseLatLng) {
        var _this = this;
        if (this._mouseAdditionalMarker) {
            var distanceToPolyline = Object(_leaflet_helper__WEBPACK_IMPORTED_MODULE_10__["customHaversine"])(Object(_leaflet_helper__WEBPACK_IMPORTED_MODULE_10__["getLatLngTuple"])(this._mouseAdditionalMarker.marker), mouseLatLng);
            if (distanceToPolyline > this._distanceToVanish && !this._mouseAdditionalMarker.isMoving) {
                lMap.removeLayer(this._mouseAdditionalMarker.marker);
                this._mouseAdditionalMarker = null;
                return;
            }
            var markerOnMouse = this._routingMarkers
                .filter(function (m) { return m.polyline; })
                .find(function (m) { return m.polyline._leaflet_id === _this._mouseAdditionalMarker.polylineId; });
            if (markerOnMouse.polyline) {
                var closetCoordinate = Object(_leaflet_helper__WEBPACK_IMPORTED_MODULE_10__["getClosestCoordinate"])(markerOnMouse.polyline, mouseLatLng);
                if (closetCoordinate) {
                    this._mouseAdditionalMarker.marker.setLatLng(closetCoordinate);
                }
            }
        }
        if (this._mouseOverPolylineId) {
            if (!this._mouseAdditionalMarker) {
                var marker_1 = leaflet__WEBPACK_IMPORTED_MODULE_3__["marker"](mouseLatLng, {
                    draggable: true,
                    icon: leaflet__WEBPACK_IMPORTED_MODULE_3__["divIcon"]({
                        className: 'leaflet-div-icon-light-blue',
                    })
                })
                    .addTo(lMap);
                this._mouseAdditionalMarker = {
                    marker: marker_1,
                    polylineId: this._mouseOverPolylineId,
                    isMoving: false,
                };
                marker_1.on('movestart', function () {
                    _this._mouseAdditionalMarker.isMoving = true;
                });
                marker_1.on('moveend', function (e) {
                    lMap.removeLayer(_this._mouseAdditionalMarker.marker);
                    _this._mouseAdditionalMarker = null;
                    _this._renavigate(lMap, marker_1, e, 'moveend');
                });
            }
        }
    };
    RoutingService.prototype._renavigate = function (lMap, marker, event, type) {
        var _this = this;
        var markerToMoveOrRemove = this._routingMarkers.find(function (p) { return p.markerLeafletId === event.target._leaflet_id; });
        var markerIndex = this._routingMarkers.indexOf(markerToMoveOrRemove);
        var _a = this._getBoundsMarkers(markerIndex), nextMarker = _a.nextMarker, previousMarker = _a.previousMarker;
        // Delete marker
        if (type === 'click') {
            lMap.removeLayer(marker);
            this._removeAssociatedWaypointOnMap(lMap, marker);
            if (markerToMoveOrRemove.polyline) {
                lMap.removeLayer(markerToMoveOrRemove.polyline);
            }
            if (!previousMarker && nextMarker) {
                lMap.removeLayer(nextMarker.polyline);
                nextMarker.route = [];
                nextMarker.polyline = null;
                nextMarker.marker.setIcon(circleIcon(true, nextMarker.type));
            }
            else if (previousMarker && nextMarker) {
                lMap.removeLayer(nextMarker.polyline);
                this._bRouterService.fetchRoute(previousMarker.latLng, nextMarker.latLng)
                    .subscribe(function (geoJsonData) {
                    if (!geoJsonData) {
                        nextMarker.polyline = leaflet__WEBPACK_IMPORTED_MODULE_3__["polyline"]([previousMarker.latLng, nextMarker.latLng], _leaflet_media__WEBPACK_IMPORTED_MODULE_9__["routingPolylineOptions"]).addTo(lMap);
                        return;
                    }
                    var route = _this._getRoute(previousMarker, geoJsonData);
                    nextMarker.polyline = leaflet__WEBPACK_IMPORTED_MODULE_3__["polyline"](route, _leaflet_media__WEBPACK_IMPORTED_MODULE_9__["routingPolylineOptions"]).addTo(lMap);
                });
            }
            this._routingMarkers.splice(markerIndex, 1);
        }
        else if (type === 'moveend') {
            var newLatLng_1 = [event.target._latlng.lat, event.target._latlng.lng];
            markerToMoveOrRemove.latLng = newLatLng_1;
            if (markerToMoveOrRemove.polyline) {
                lMap.removeLayer(markerToMoveOrRemove.polyline);
                this._removeAssociatedWaypointOnMap(lMap, markerToMoveOrRemove.markerLeafletId);
                if (markerToMoveOrRemove.type === _interfaces__WEBPACK_IMPORTED_MODULE_6__["ClickType"].Left) {
                    this._bRouterService.fetchRoute(previousMarker.latLng, newLatLng_1)
                        .subscribe(function (data) {
                        if (!data) {
                            markerToMoveOrRemove.polyline = leaflet__WEBPACK_IMPORTED_MODULE_3__["polyline"]([previousMarker.latLng, newLatLng_1], _leaflet_media__WEBPACK_IMPORTED_MODULE_9__["routingPolylineOptions"]).addTo(lMap);
                            return;
                        }
                        var route = _this._getRoute(previousMarker, data);
                        markerToMoveOrRemove.polyline = leaflet__WEBPACK_IMPORTED_MODULE_3__["polyline"](route, _leaflet_media__WEBPACK_IMPORTED_MODULE_9__["routingPolylineOptions"]).addTo(lMap);
                        _this._addPointOfInterestsOnRoute(lMap, route, markerToMoveOrRemove.marker);
                    });
                }
                else {
                    markerToMoveOrRemove.polyline = leaflet__WEBPACK_IMPORTED_MODULE_3__["polyline"]([previousMarker.latLng, newLatLng_1], _leaflet_media__WEBPACK_IMPORTED_MODULE_9__["routingPolylineOptions"]).addTo(lMap);
                }
            }
            if (nextMarker) {
                lMap.removeLayer(nextMarker.polyline);
                this._removeAssociatedWaypointOnMap(lMap, nextMarker.markerLeafletId);
                if (nextMarker.type === _interfaces__WEBPACK_IMPORTED_MODULE_6__["ClickType"].Left) {
                    this._bRouterService.fetchRoute(newLatLng_1, nextMarker.latLng)
                        .subscribe(function (data) {
                        if (!data) {
                            nextMarker.polyline = leaflet__WEBPACK_IMPORTED_MODULE_3__["polyline"]([newLatLng_1, nextMarker.latLng], _leaflet_media__WEBPACK_IMPORTED_MODULE_9__["routingPolylineOptions"]).addTo(lMap);
                            return;
                        }
                        nextMarker.polyline = leaflet__WEBPACK_IMPORTED_MODULE_3__["polyline"](Object(_osm_helper__WEBPACK_IMPORTED_MODULE_8__["extractLatLngsFromGeoJson"])(data), _leaflet_media__WEBPACK_IMPORTED_MODULE_9__["routingPolylineOptions"]).addTo(lMap);
                    });
                }
                else {
                    nextMarker.polyline = leaflet__WEBPACK_IMPORTED_MODULE_3__["polyline"]([newLatLng_1, nextMarker.latLng], _leaflet_media__WEBPACK_IMPORTED_MODULE_9__["routingPolylineOptions"]).addTo(lMap);
                }
            }
        }
    };
    RoutingService.prototype._addPointOfInterestsOnRoute = function (lMap, route, newDestinationMarker) {
        var _this = this;
        var boundsBox = Object(_osm_helper__WEBPACK_IMPORTED_MODULE_8__["getLatLngBounds"])(route, 0.001);
        this._overpassService.getNodes(_overpass_service__WEBPACK_IMPORTED_MODULE_7__["pointOfInterestOnRoute"], boundsBox)
            .subscribe(function (osmNodes) {
            var e_4, _a;
            try {
                for (var osmNodes_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__values"](osmNodes), osmNodes_1_1 = osmNodes_1.next(); !osmNodes_1_1.done; osmNodes_1_1 = osmNodes_1.next()) {
                    var osmNode = osmNodes_1_1.value;
                    _this._addWaypointMarker(lMap, route, newDestinationMarker, osmNode);
                }
            }
            catch (e_4_1) { e_4 = { error: e_4_1 }; }
            finally {
                try {
                    if (osmNodes_1_1 && !osmNodes_1_1.done && (_a = osmNodes_1.return)) _a.call(osmNodes_1);
                }
                finally { if (e_4) throw e_4.error; }
            }
        });
    };
    RoutingService.prototype._getBoundsMarkers = function (markerIndex) {
        var isFirstMarker = markerIndex === 0;
        var isLastMarker = markerIndex === this._routingMarkers.length - 1;
        var nextMarker;
        var previousMarker;
        if (!isLastMarker) {
            nextMarker = this._routingMarkers[markerIndex + 1];
        }
        if (!isFirstMarker) {
            previousMarker = this._routingMarkers[markerIndex - 1];
        }
        return { nextMarker: nextMarker, previousMarker: previousMarker };
    };
    RoutingService.prototype._addWaypointMarker = function (lMap, route, nextDestinationMarker, node) {
        var e_5, _a;
        if (!node || !node.tags) {
            return;
        }
        // Avoid duplicate
        if (this._waypointMarkers.some(function (m) { return m.osmNodeId === node.id; })) {
            return;
        }
        // Avoid ghosts
        if (!Object(_leaflet_helper__WEBPACK_IMPORTED_MODULE_10__["isMarkerOnMap"])(lMap, nextDestinationMarker)) {
            return;
        }
        // Find closest point to osm node
        var minDist = Infinity;
        try {
            for (var route_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__values"](route), route_1_1 = route_1.next(); !route_1_1.done; route_1_1 = route_1.next()) {
                var point = route_1_1.value;
                var dist = Object(_leaflet_helper__WEBPACK_IMPORTED_MODULE_10__["customHaversine"])(point, node.latlng);
                minDist = Math.min(dist, minDist);
            }
        }
        catch (e_5_1) { e_5 = { error: e_5_1 }; }
        finally {
            try {
                if (route_1_1 && !route_1_1.done && (_a = route_1.return)) _a.call(route_1);
            }
            finally { if (e_5) throw e_5.error; }
        }
        console.log("min dist " + Object(_helper__WEBPACK_IMPORTED_MODULE_5__["round"])(minDist * 1000, 2) + " m");
        if (minDist * 1000 > 60) {
            return;
        }
        var markerType = 'tree';
        if ('amenity' in node.tags) {
            switch (node.tags.amenity) {
                case 'drinking_water': {
                    markerType = 'water';
                }
            }
        }
        else if ('natural' in node.tags) {
            markerType = node.tags.natural;
        }
        else if ('tourism' in node.tags) {
            markerType = node.tags.tourism;
        }
        var osmMarker = leaflet__WEBPACK_IMPORTED_MODULE_3__["marker"](node.latlng, {
            icon: Object(_leaflet_media__WEBPACK_IMPORTED_MODULE_9__["waypointMarker"])(markerType),
        })
            .addTo(lMap)
            .bindPopup("<pre>" + JSON.stringify(node.tags, null, 2) + "</pre>");
        // .bindPopup(`min dist ${round(minDist * 1000, 2)} m`);
        this._waypointMarkers.push({
            marker: osmMarker,
            osmNodeId: node.id,
            id: nextDestinationMarker._leaflet_id,
        });
    };
    RoutingService.prototype._handleSacScale = function (lMap, way) {
        if (way.tags && way.tags.sac_scale) {
            var coord = way.latlngs[0];
            switch (way.tags.sac_scale) {
                case 'hiking':
                    // option.color = 'green';
                    break;
                case 'mountain_hiking':
                    // option.color = 'blue';
                    break;
                case 'demanding_mountain_hiking':
                    // option.color = 'blue';
                    // option.dashArray = '10 5';
                    var myIcon = leaflet__WEBPACK_IMPORTED_MODULE_3__["divIcon"]({
                        className: 'leaflet-div-icon-yellow',
                    });
                    var marker = leaflet__WEBPACK_IMPORTED_MODULE_3__["marker"](coord, {
                        draggable: true,
                        icon: myIcon,
                    }).addTo(lMap);
                    break;
                case 'alpine_hiking':
                    // option.color = 'red';
                    break;
                case 'demanding_alpine_hiking':
                    // option.color = 'black';
                    break;
                case 'difficult_alpine_hiking':
                    // option.color = 'black';
                    // option.dashArray = '10 5';
                    break;
            }
        }
    };
    RoutingService.prototype._getRoute = function (previousMarker, geoJsonData) {
        if (previousMarker.type === _interfaces__WEBPACK_IMPORTED_MODULE_6__["ClickType"].Right) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"]([
                previousMarker.latLng
            ], Object(_osm_helper__WEBPACK_IMPORTED_MODULE_8__["extractLatLngsFromGeoJson"])(geoJsonData));
        }
        else {
            return Object(_osm_helper__WEBPACK_IMPORTED_MODULE_8__["extractLatLngsFromGeoJson"])(geoJsonData);
        }
    };
    RoutingService.prototype._removeAssociatedWaypointOnMap = function (lMap, oldDestinationMarker) {
        var e_6, _a;
        var leafletId = typeof oldDestinationMarker === 'number'
            ? oldDestinationMarker
            : oldDestinationMarker._leaflet_id;
        try {
            for (var _b = tslib__WEBPACK_IMPORTED_MODULE_0__["__values"](this._waypointMarkers.filter(function (w) { return w.id === leafletId; })), _c = _b.next(); !_c.done; _c = _b.next()) {
                var waypoint = _c.value;
                lMap.removeLayer(waypoint.marker);
            }
        }
        catch (e_6_1) { e_6 = { error: e_6_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_6) throw e_6.error; }
        }
        this._waypointMarkers = this._waypointMarkers.filter(function (w) { return w.id !== leafletId; });
    };
    /**
     * Avoid messy maps
     */
    RoutingService.prototype._removeHoverMarker = function (lMap) {
        if (this._mouseAdditionalMarker && this._mouseAdditionalMarker.marker) {
            lMap.removeLayer(this._mouseAdditionalMarker.marker);
        }
        this._mouseAdditionalMarker = null;
    };
    RoutingService.ctorParameters = function () { return [
        { type: _gpx_gpx_service__WEBPACK_IMPORTED_MODULE_4__["GpxService"] },
        { type: _overpass_service__WEBPACK_IMPORTED_MODULE_7__["OverpassService"] },
        { type: _b_router_service__WEBPACK_IMPORTED_MODULE_11__["BRouterService"] },
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    RoutingService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root',
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_gpx_gpx_service__WEBPACK_IMPORTED_MODULE_4__["GpxService"],
            _overpass_service__WEBPACK_IMPORTED_MODULE_7__["OverpassService"],
            _b_router_service__WEBPACK_IMPORTED_MODULE_11__["BRouterService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], RoutingService);
    return RoutingService;
}());



/***/ }),

/***/ "./src/app/map/tiles.endpoints.ts":
/*!****************************************!*\
  !*** ./src/app/map/tiles.endpoints.ts ***!
  \****************************************/
/*! exports provided: tileServers, mapboxTiles, suuntoHeatmapDomain, suuntoHeatmap, overlay */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "tileServers", function() { return tileServers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mapboxTiles", function() { return mapboxTiles; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "suuntoHeatmapDomain", function() { return suuntoHeatmapDomain; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "suuntoHeatmap", function() { return suuntoHeatmap; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "overlay", function() { return overlay; });
// tslint:disable:max-line-length
var tileServers = {
    openStreetMap: {
        url: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png'
    },
    openTopoMap: {
        url: 'http://{s}.tile.opentopomap.org/{z}/{x}/{y}.png',
        options: {
            maxZoom: 17,
            detectRetina: true,
            attribution: '',
        },
    },
    strava: {
        url: 'https://b.tiles.mapbox.com/v4/strava.blprdx6r/{z}/{x}/{y}.png?access_token=pk.eyJ1Ijoic3RyYXZhIiwiYSI6IlpoeXU2U0UifQ.c7yhlZevNRFCqHYm6G6Cyg'
    },
};
var mapboxUrl = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}@2x.png?access_token=pk.eyJ1Ijoia3Jpc3RyZW1vbmRvdXMiLCJhIjoiT2R0Y084YyJ9.fePC_n9LzdWkjWug4mdTCw';
var mapboxAttr = '';
var mapboxTiles = {
    grayscale: {
        url: mapboxUrl,
        options: {
            id: 'mapbox.light',
            attribution: mapboxAttr
        },
    },
    strava: {
        url: mapboxUrl,
        options: {
            id: 'strava.1vr0ms4i',
            attribution: mapboxAttr
        },
    },
    streets: {
        url: mapboxUrl,
        options: {
            id: 'mapbox.streets',
            attribution: mapboxAttr
        },
    },
    satellite: {
        url: mapboxUrl,
        options: {
            id: 'mapbox.satellite',
            attribution: mapboxAttr
        },
    },
};
// http://heatmaprestapi-production.eu-west-1.elasticbeanstalk.com/heatmap/v1/AllTrails/{z}/{x}/{y}.png?appkey=KJ5ZC2d0mx1wE6FtkXrHGigZzdrL3OOl9IZKWftul9KhH25L2wKkAUyBDgzyKC6I
var suuntoHeatmapDomain = 'http://heatmaprestapi-production.eu-west-1.elasticbeanstalk.com';
var appKey = 'KJ5ZC2d0mx1wE6FtkXrHGigZzdrL3OOl9IZKWftul9KhH25L2wKkAUyBDgzyKC6I';
var suuntoHeatmap = function (type) { return suuntoHeatmapDomain + "/heatmap/v1/" + type + "/{z}/{x}/{y}.png?appkey=" + appKey; };
var overlay = {
    waymarkedtrail: {
        url: 'https://tile.waymarkedtrails.org/hiking/{z}/{x}/{y}.png',
    },
    suunto: {
        options: {
            maxNativeZoom: 14,
        },
    }
};


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ "./src/material.module.ts":
/*!********************************!*\
  !*** ./src/material.module.ts ***!
  \********************************/
/*! exports provided: MaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialModule", function() { return MaterialModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");



var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
                // MatFormFieldModule,
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
                // MatInputModule,
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatBadgeModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
            ],
            exports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatBadgeModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
            ],
        })
    ], MaterialModule);
    return MaterialModule;
}());



/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/kris/Documents/repos/kris/summits/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es5.js.map