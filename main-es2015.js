(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-sidenav-container class=\"example-container\">\n  <mat-sidenav [(opened)]=\"opened\"\n               (opened)=\"events.push('open!')\"\n               (closed)=\"events.push('close!')\"\n               #sidenav\n               mode=\"side\">\n\n    <mat-toolbar color=\"primary\">\n      <!--<mat-toolbar-row>-->\n      <!--<span>First Row</span>-->\n      <!--</mat-toolbar-row>-->\n\n      <!--<mat-toolbar-row>-->\n      <span>Routing</span>\n      <span class=\"example-spacer\"></span>\n      <!--<mat-icon class=\"example-icon\" aria-hidden=\"false\" aria-label=\"Example heart icon\">favorite</mat-icon>-->\n      <button mat-icon-button>\n        <mat-icon>keyboard_arrow_left</mat-icon>\n      </button>\n    </mat-toolbar>\n\n    <!--<mat-toolbar color=\"primary\">-->\n      <!--<span class=\"fill-remaining-space\">-->\n        <!--<button mat-icon-button [matMenuTriggerFor]=\"menu\">-->\n          <!--<mat-icon>terrain</mat-icon>-->\n        <!--</button>-->\n    <mat-menu #menu=\"matMenu\" [overlapTrigger]=\"false\">\n      <button mat-menu-item>\n        <mat-icon>home</mat-icon>\n        <span>Home</span>\n      </button>\n      <button mat-menu-item>\n        <mat-icon>people_outline</mat-icon>\n        <span>Connecting</span>\n      </button>\n      <button mat-menu-item>\n        <mat-icon>videocam</mat-icon>\n        <span>Let's talk</span>\n      </button>\n      <button mat-menu-item>\n        <mat-icon>exit_to_app</mat-icon>\n        <span>Logout</span>\n      </button>\n    </mat-menu>\n      <!--</span>-->\n      <!--<span class=\"fill-remaining-space\">Mountain router</span>-->\n    <!--</mat-toolbar>-->\n\n    <!--{{ title }}-->\n\n    <!--<mat-card>-->\n      <!--<div cdkDrop>-->\n        <!--<div class=\"box\" cdkDrag>Drag me around!</div>-->\n      <!--</div>-->\n    <!--</mat-card>-->\n\n    <button #ref mat-button color=\"primary\">\n      Click me!\n    </button>\n\n    <!--<mat-form-field >-->\n      <!--<mat-label>Favorite food</mat-label>-->\n      <!--<mat-select>-->\n        <!--<mat-option *ngFor=\"let food of foods\"-->\n                    <!--[value]=\"food.value\">-->\n          <!--{{food.viewValue}}-->\n        <!--</mat-option>-->\n      <!--</mat-select>-->\n    <!--</mat-form-field>-->\n\n    <!--<p><button mat-button (click)=\"sidenav.toggle()\">sidenav.toggle()</button></p>-->\n\n    <!--<p>-->\n      <!--<span matBadge=\"4\" matBadgeOverlap=\"false\">Text with a badge</span>-->\n    <!--</p>-->\n\n    <mat-card class='example-card'>\n      <mat-card-header>\n        <!--<div mat-card-avatar class='example-header-image'></div>-->\n        <mat-card-title> {{ getInfo().distance | number:'1.0-2':'fr' }} km</mat-card-title>\n        <mat-card-subtitle>\n          <mat-icon>trending_up</mat-icon>\n          <span>&nbsp;{{ getInfo().totalAscent | number:'1.0-0':'fr' }} m d+</span>\n          &nbsp;|&nbsp;\n          <mat-icon>trending_down</mat-icon>\n          <span>&nbsp;{{ getInfo().totalDescent | number:'1.0-0':'fr' }} m d-</span>\n        </mat-card-subtitle>\n      </mat-card-header>\n    </mat-card>\n\n    <mat-toolbar color=\"primary\">\n\n      <!--<mat-toolbar-row>-->\n        <!--<span>First Row</span>-->\n      <!--</mat-toolbar-row>-->\n\n      <!--<mat-toolbar-row>-->\n      <span>Routing</span>\n      <span class=\"example-spacer\"></span>\n      <!--<mat-icon class=\"example-icon\" aria-hidden=\"false\" aria-label=\"Example heart icon\">favorite</mat-icon>-->\n      <button mat-icon-button [matMenuTriggerFor]=\"menu\">\n        <!--<mat-icon>menu</mat-icon>-->\n        <!--https://material.io/tools/icons/?style=baseline-->\n        <mat-icon [matBadge]=\"getNumberOfMarkers()\"\n                  matBadgeColor=\"primary\">timeline</mat-icon>\n      </button>\n\n        <!--</mat-toolbar-row>-->\n\n      <!--<span>Application Title</span>-->\n\n      <!--&lt;!&ndash; This fills the remaining space of the current row &ndash;&gt;-->\n      <!--<span class=\"example-fill-remaining-space\"></span>-->\n\n      <!--<span>Right Aligned Text</span>-->\n    </mat-toolbar>\n\n    <button (click)=\"resetRouting()\"\n            mat-icon-button>\n      <mat-icon matBadge=\"15\"\n                matBadgeColor=\"primary\">delete</mat-icon>\n    </button>\n\n    <button (click)=\"undo()\"\n            [disabled]=\"!canUndo()\"\n            mat-icon-button>\n      <mat-icon matBadge=\"\"\n                matBadgeColor=\"warn\">undo</mat-icon>\n    </button>\n\n    <button (click)=\"redo()\"\n            [disabled]=\"!canRedo()\"\n            mat-icon-button>\n      <mat-icon matBadge=\"\"\n                matBadgeColor=\"warn\">redo</mat-icon>\n    </button>\n\n    <button (click)=\"reverse()\"\n            [disabled]=\"getNumberOfMarkers() === 0\"\n            mat-icon-button>\n      <mat-icon>swap_horiz</mat-icon>\n    </button>\n\n    <button (click)=\"reverse()\"\n            [disabled]=\"getNumberOfMarkers() === 0\"\n            mat-icon-button>\n      <mat-icon>save_alt</mat-icon>\n    </button>\n\n    <button (click)=\"reverse()\"\n            [disabled]=\"getNumberOfMarkers() === 0\"\n            mat-icon-button>\n      <mat-icon>settings</mat-icon>\n    </button>\n\n    <div>text</div>\n\n    <app-barchart *ngIf=\"chartData\"\n                  [data]=\"chartData\">\n    </app-barchart>\n\n\n    <mat-toolbar color=\"primary\">\n      <span>POI</span>\n    </mat-toolbar>\n\n    <button (click)=\"printWater()\"\n            mat-icon-button>\n      <mat-icon>settings</mat-icon>\n    </button>\n\n\n  </mat-sidenav>\n\n  <mat-sidenav-content>\n    <app-map [state]=\"ok\"\n             [activePOI]=\"activePOIs\"></app-map>\n  </mat-sidenav-content>\n</mat-sidenav-container>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/charts/barchart/barchart.component.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/charts/barchart/barchart.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"d3-chart\" #chart></div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/map/map.component.html":
/*!******************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/map/map.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div #map></div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/map/popup/popup.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/map/popup/popup.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<div></div>-->\n\n\n<div>{{ test }}</div>\n"

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");




const routes = [
    {
        path: 'app',
        component: _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
        children: [],
    },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  background: #eee;\n}\n\nmat-sidenav {\n  width: 250px;\n  background: #eee;\n}\n\nmat-card {\n  margin: 10px;\n  padding: 16px 0;\n  text-align: center;\n}\n\nmat-card ::ng-deep .mat-card-header-text {\n  width: 100%;\n}\n\nmat-card mat-card-subtitle {\n  margin-bottom: 0;\n}\n\nmat-card mat-card-subtitle mat-icon {\n  vertical-align: middle;\n  display: -webkit-inline-box;\n  display: inline-flex;\n}\n\nbutton {\n  position: relative;\n  text-align: center;\n}\n\nmat-toolbar-row,\nmat-toolbar {\n  height: 48px;\n}\n\napp-leaflet {\n  width: 100%;\n  height: 100%;\n}\n\n.example-icon {\n  padding: 0 14px;\n}\n\n.example-spacer {\n  -webkit-box-flex: 1;\n          flex: 1 1 auto;\n}\n\n.fill-remaining-space {\n  /* This fills the remaining space, by using flexbox.\n     Every toolbar row uses a flexbox row layout. */\n  -webkit-box-flex: 1;\n          flex: 1 1 auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2tyaXMvRG9jdW1lbnRzL3JlcG9zL2tyaXMvc3VtbWl0cy9zcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQUE7RUFDQSxNQUFBO0VBQ0EsU0FBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0VBQ0EsZ0JBQUE7QUNDRjs7QURFQTtFQUNFLFlBQUE7RUFDQSxnQkFBQTtBQ0NGOztBREdBO0VBQ0UsWUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQ0FGOztBREdJO0VBQ0UsV0FBQTtBQ0ROOztBRFNFO0VBQ0UsZ0JBQUE7QUNQSjs7QURTSTtFQUNFLHNCQUFBO0VBQ0EsMkJBQUE7RUFBQSxvQkFBQTtBQ1BOOztBRFlBO0VBQ0Usa0JBQUE7RUFDQSxrQkFBQTtBQ1RGOztBRGFBOztFQUVFLFlBQUE7QUNWRjs7QURhQTtFQUNDLFdBQUE7RUFDQSxZQUFBO0FDVkQ7O0FEYUE7RUFDRSxlQUFBO0FDVkY7O0FEYUE7RUFDRSxtQkFBQTtVQUFBLGNBQUE7QUNWRjs7QURhQTtFQUNFO21EQUFBO0VBRUEsbUJBQUE7VUFBQSxjQUFBO0FDVkYiLCJmaWxlIjoic3JjL2FwcC9hcHAuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXhhbXBsZS1jb250YWluZXIge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgYm90dG9tOiAwO1xuICBsZWZ0OiAwO1xuICByaWdodDogMDtcbiAgYmFja2dyb3VuZDogI2VlZTtcbn1cblxubWF0LXNpZGVuYXYge1xuICB3aWR0aDogMjUwcHg7XG4gIGJhY2tncm91bmQ6ICNlZWU7XG4gIC8vYmFja2dyb3VuZDogIzI5MjkyOTtcbn1cblxubWF0LWNhcmQge1xuICBtYXJnaW46IDEwcHg7XG4gIHBhZGRpbmc6IDE2cHggMDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuXG4gIDo6bmctZGVlcCB7XG4gICAgLm1hdC1jYXJkLWhlYWRlci10ZXh0IHtcbiAgICAgIHdpZHRoOiAxMDAlO1xuICAgIH1cbiAgfVxuXG4gIG1hdC1jYXJkLXRpdGxlIHtcblxuICB9XG5cbiAgbWF0LWNhcmQtc3VidGl0bGUge1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG5cbiAgICBtYXQtaWNvbiB7XG4gICAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICAgICAgZGlzcGxheTogaW5saW5lLWZsZXg7XG4gICAgfVxuICB9XG59XG5cbmJ1dHRvbiB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLy9cbi8vLmZpbGwtcmVtYWluaW5nLXNwYWNlLFxubWF0LXRvb2xiYXItcm93LFxubWF0LXRvb2xiYXIge1xuICBoZWlnaHQ6IDQ4cHg7XG59XG5cbmFwcC1sZWFmbGV0IHtcbiB3aWR0aDogMTAwJTtcbiBoZWlnaHQ6IDEwMCU7XG59XG5cbi5leGFtcGxlLWljb24ge1xuICBwYWRkaW5nOiAwIDE0cHg7XG59XG5cbi5leGFtcGxlLXNwYWNlciB7XG4gIGZsZXg6IDEgMSBhdXRvO1xufVxuXG4uZmlsbC1yZW1haW5pbmctc3BhY2Uge1xuICAvKiBUaGlzIGZpbGxzIHRoZSByZW1haW5pbmcgc3BhY2UsIGJ5IHVzaW5nIGZsZXhib3guXG4gICAgIEV2ZXJ5IHRvb2xiYXIgcm93IHVzZXMgYSBmbGV4Ym94IHJvdyBsYXlvdXQuICovXG4gIGZsZXg6IDEgMSBhdXRvO1xufVxuIiwiLmV4YW1wbGUtY29udGFpbmVyIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGJvdHRvbTogMDtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIGJhY2tncm91bmQ6ICNlZWU7XG59XG5cbm1hdC1zaWRlbmF2IHtcbiAgd2lkdGg6IDI1MHB4O1xuICBiYWNrZ3JvdW5kOiAjZWVlO1xufVxuXG5tYXQtY2FyZCB7XG4gIG1hcmdpbjogMTBweDtcbiAgcGFkZGluZzogMTZweCAwO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5tYXQtY2FyZCA6Om5nLWRlZXAgLm1hdC1jYXJkLWhlYWRlci10ZXh0IHtcbiAgd2lkdGg6IDEwMCU7XG59XG5tYXQtY2FyZCBtYXQtY2FyZC1zdWJ0aXRsZSB7XG4gIG1hcmdpbi1ib3R0b206IDA7XG59XG5tYXQtY2FyZCBtYXQtY2FyZC1zdWJ0aXRsZSBtYXQtaWNvbiB7XG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG4gIGRpc3BsYXk6IGlubGluZS1mbGV4O1xufVxuXG5idXR0b24ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxubWF0LXRvb2xiYXItcm93LFxubWF0LXRvb2xiYXIge1xuICBoZWlnaHQ6IDQ4cHg7XG59XG5cbmFwcC1sZWFmbGV0IHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuLmV4YW1wbGUtaWNvbiB7XG4gIHBhZGRpbmc6IDAgMTRweDtcbn1cblxuLmV4YW1wbGUtc3BhY2VyIHtcbiAgZmxleDogMSAxIGF1dG87XG59XG5cbi5maWxsLXJlbWFpbmluZy1zcGFjZSB7XG4gIC8qIFRoaXMgZmlsbHMgdGhlIHJlbWFpbmluZyBzcGFjZSwgYnkgdXNpbmcgZmxleGJveC5cbiAgICAgRXZlcnkgdG9vbGJhciByb3cgdXNlcyBhIGZsZXhib3ggcm93IGxheW91dC4gKi9cbiAgZmxleDogMSAxIGF1dG87XG59Il19 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _map_routing_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./map/routing.service */ "./src/app/map/routing.service.ts");





let AppComponent = class AppComponent {
    // @ViewChild('sidenav', { read: ElementRef, static: true })
    // public sidenav: MatSidenav;
    // @ViewChild('deleteRouteButton', { read: ElementRef, static: true })
    // public deleteRouteButton: ElementRef;
    constructor(_routingService) {
        this._routingService = _routingService;
        this.events = [];
        this.opened = true;
        this.title = 'summits';
        this.activePOIs = [];
        this.ok = '["sac_scale"]';
        // public btn: any;
        this.foods = [
            { value: '["trail_visibility"]', viewValue: 'Visibility' },
            { value: '["sac_scale"]', viewValue: 'SAC scale' },
            { value: 'tacos-2', viewValue: 'Tacos' }
        ];
    }
    ngOnInit() {
        const source = Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["fromEvent"])(this.form.nativeElement, 'click');
        // const deleteRouteClick = fromEvent(this.deleteRouteButton.nativeElement, 'click');
        // deleteRouteClick.pipe(
        //   map(event => `Event time: ${event}`)
        // );
        //
        // deleteRouteClick.subscribe(val => {
        //   console.log('val', val);
        // });
        const example = source.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(event => `Event time: ${event}`));
        // output (example): 'Event time: 7276.390000000001'
        // const subscribe = example.subscribe(val => {
        //   this.ok = '["trail_visibility"]';
        // });
        setTimeout(() => {
            this._generateRandomData();
            // change the data periodically
            setInterval(() => this._generateRandomData(), 5000);
        }, 1000);
    }
    getNumberOfMarkers() {
        return this._routingService.getInfo().numberOfMarkers;
    }
    getInfo() {
        return this._routingService.getInfo();
    }
    resetRouting() {
        this._routingService.resetRouting();
    }
    undo() {
        this._routingService.undo();
    }
    redo() {
        this._routingService.redo();
    }
    reverse() {
        this._routingService.reverse();
    }
    canUndo() {
        return this._routingService.canUndoRedo('undo');
    }
    canRedo() {
        return this._routingService.canUndoRedo('redo');
    }
    printWater() {
        if (this.activePOIs.includes('water')) {
            this.activePOIs = this.activePOIs.filter(poi => poi !== 'water');
            // this.activePOIs.push('water');
        }
        else {
            this.activePOIs.push('water');
        }
        console.log('app', this.activePOIs);
    }
    _generateRandomData() {
        this.chartData = [];
        for (let i = 0; i < (8 + Math.floor(Math.random() * 10)); i++) {
            this.chartData.push([
                `${i}`,
                Math.floor(Math.random() * 100)
            ]);
        }
    }
};
AppComponent.ctorParameters = () => [
    { type: _map_routing_service__WEBPACK_IMPORTED_MODULE_4__["RoutingService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('ref', { read: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], static: true }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
], AppComponent.prototype, "form", void 0);
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
        styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_map_routing_service__WEBPACK_IMPORTED_MODULE_4__["RoutingService"]])
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: MyInterceptor, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyInterceptor", function() { return MyInterceptor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_common_locales_fr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/locales/fr */ "./node_modules/@angular/common/locales/fr.js");
/* harmony import */ var _angular_common_locales_fr__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_angular_common_locales_fr__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../material.module */ "./src/material.module.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _map_map_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./map/map.component */ "./src/app/map/map.component.ts");
/* harmony import */ var _map_popup_popup_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./map/popup/popup.component */ "./src/app/map/popup/popup.component.ts");
/* harmony import */ var _charts_barchart_barchart_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./charts/barchart/barchart.component */ "./src/app/charts/barchart/barchart.component.ts");
/* harmony import */ var _map_b_router_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./map/b-router.service */ "./src/app/map/b-router.service.ts");
















Object(_angular_common__WEBPACK_IMPORTED_MODULE_6__["registerLocaleData"])(_angular_common_locales_fr__WEBPACK_IMPORTED_MODULE_7___default.a);
let MyInterceptor = class MyInterceptor {
    intercept(req, next) {
        const trimmedUrl = req.url.trim();
        // https://blog.angularindepth.com/top-10-ways-to-use-interceptors-in-angular-db450f8a62d6
        if (!req.url.startsWith(_map_b_router_service__WEBPACK_IMPORTED_MODULE_15__["endpointBRouterAPI"])) {
            return next.handle(req);
        }
        return next.handle(req).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["tap"])((event) => {
            if (event instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpResponse"] && event.status === 200) {
                // console.log('cool', event.body);
            }
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["catchError"])((e) => {
            console.log('e', e.error.text);
            throw e;
        }));
    }
};
MyInterceptor = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])()
], MyInterceptor);

let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_11__["AppComponent"],
            _map_map_component__WEBPACK_IMPORTED_MODULE_12__["MapComponent"],
            _charts_barchart_barchart_component__WEBPACK_IMPORTED_MODULE_14__["BarchartComponent"],
            _map_popup_popup_component__WEBPACK_IMPORTED_MODULE_13__["PopupComponent"],
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_10__["AppRoutingModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"],
            _material_module__WEBPACK_IMPORTED_MODULE_9__["MaterialModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
        ],
        providers: [
            { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HTTP_INTERCEPTORS"], useClass: MyInterceptor, multi: true },
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_11__["AppComponent"]],
        entryComponents: [
            _map_popup_popup_component__WEBPACK_IMPORTED_MODULE_13__["PopupComponent"],
        ],
    })
], AppModule);



/***/ }),

/***/ "./src/app/charts/barchart/barchart.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/charts/barchart/barchart.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".d3-chart {\n  width: 100%;\n  height: 200px;\n}\n.d3-chart .axis line,\n.d3-chart .axis path {\n  stroke: #999;\n}\n.d3-chart .axis text {\n  fill: red;\n  font: 13px Roboto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2tyaXMvRG9jdW1lbnRzL3JlcG9zL2tyaXMvc3VtbWl0cy9zcmMvYXBwL2NoYXJ0cy9iYXJjaGFydC9iYXJjaGFydC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvY2hhcnRzL2JhcmNoYXJ0L2JhcmNoYXJ0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsV0FBQTtFQUNBLGFBQUE7QUNDRjtBREVJOztFQUVFLFlBQUE7QUNBTjtBREdJO0VBQ0UsU0FBQTtFQUNBLGlCQUFBO0FDRE4iLCJmaWxlIjoic3JjL2FwcC9jaGFydHMvYmFyY2hhcnQvYmFyY2hhcnQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZDMtY2hhcnQge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAyMDBweDtcblxuICAuYXhpcyB7XG4gICAgbGluZSxcbiAgICBwYXRoIHtcbiAgICAgIHN0cm9rZTogIzk5OTtcbiAgICB9XG5cbiAgICB0ZXh0IHtcbiAgICAgIGZpbGw6IHJlZDtcbiAgICAgIGZvbnQ6IDEzcHggUm9ib3RvO1xuICAgIH1cbiAgfVxufVxuIiwiLmQzLWNoYXJ0IHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMjAwcHg7XG59XG4uZDMtY2hhcnQgLmF4aXMgbGluZSxcbi5kMy1jaGFydCAuYXhpcyBwYXRoIHtcbiAgc3Ryb2tlOiAjOTk5O1xufVxuLmQzLWNoYXJ0IC5heGlzIHRleHQge1xuICBmaWxsOiByZWQ7XG4gIGZvbnQ6IDEzcHggUm9ib3RvO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/charts/barchart/barchart.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/charts/barchart/barchart.component.ts ***!
  \*******************************************************/
/*! exports provided: BarchartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BarchartComponent", function() { return BarchartComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var d3__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! d3 */ "./node_modules/d3/index.js");



let BarchartComponent = class BarchartComponent {
    constructor() {
        this.margin = {
            top: 20,
            bottom: 20,
            left: 40,
            right: 20
        };
    }
    ngOnInit() {
        this.createChart();
        if (this.data) {
            this.updateChart();
        }
    }
    ngOnChanges() {
        if (this.chart) {
            this.updateChart();
        }
    }
    createChart() {
        const element = this.chartContainer.nativeElement;
        this.width = element.offsetWidth - this.margin.left - this.margin.right;
        this.height = element.offsetHeight - this.margin.top - this.margin.bottom;
        const svg = d3__WEBPACK_IMPORTED_MODULE_2__["select"](element).append('svg')
            .attr('width', element.offsetWidth)
            .attr('height', element.offsetHeight);
        // chart plot area
        this.chart = svg.append('g')
            .attr('class', 'bars')
            .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`);
        // define X & Y domains
        const xDomain = this.data.map(d => d[0]);
        const yDomain = [0, d3__WEBPACK_IMPORTED_MODULE_2__["max"](this.data, d => d[1])];
        // create scales
        this.xScale = d3__WEBPACK_IMPORTED_MODULE_2__["scaleBand"]().padding(0.1).domain(xDomain).rangeRound([0, this.width]);
        this.yScale = d3__WEBPACK_IMPORTED_MODULE_2__["scaleLinear"]().domain(yDomain).range([this.height, 0]);
        // bar colors
        this.colors = d3__WEBPACK_IMPORTED_MODULE_2__["scaleLinear"]().domain([0, this.data.length]).range(['red', 'blue']);
        // x & y axis
        this.xAxis = svg.append('g')
            .attr('class', 'axis axis-x')
            .attr('transform', `translate(${this.margin.left}, ${this.margin.top + this.height})`)
            .call(d3__WEBPACK_IMPORTED_MODULE_2__["axisBottom"](this.xScale));
        this.yAxis = svg.append('g')
            .attr('class', 'axis axis-y')
            .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`)
            .call(d3__WEBPACK_IMPORTED_MODULE_2__["axisLeft"](this.yScale));
    }
    updateChart() {
        // update scales & axis
        this.xScale.domain(this.data.map(d => d[0]));
        this.yScale.domain([0, d3__WEBPACK_IMPORTED_MODULE_2__["max"](this.data, d => d[1])]);
        this.colors.domain([0, this.data.length]);
        this.xAxis.transition().call(d3__WEBPACK_IMPORTED_MODULE_2__["axisBottom"](this.xScale));
        this.yAxis.transition().call(d3__WEBPACK_IMPORTED_MODULE_2__["axisLeft"](this.yScale));
        const update = this.chart.selectAll('.bar')
            .data(this.data);
        // remove exiting bars
        update.exit().remove();
        // update existing bars
        this.chart.selectAll('.bar').transition()
            .attr('x', d => this.xScale(d[0]))
            .attr('y', d => this.yScale(d[1]))
            .attr('width', d => this.xScale.bandwidth())
            .attr('height', d => this.height - this.yScale(d[1]))
            .style('fill', (d, i) => this.colors(i));
        // add new bars
        update
            .enter()
            .append('rect')
            .attr('class', 'bar')
            .attr('x', d => this.xScale(d[0]))
            .attr('y', d => this.yScale(0))
            .attr('width', this.xScale.bandwidth())
            .attr('height', 0)
            .style('fill', (d, i) => this.colors(i))
            .transition()
            .delay((d, i) => i * 10)
            .attr('y', d => this.yScale(d[1]))
            .attr('height', d => this.height - this.yScale(d[1]));
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('chart', { read: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], static: true }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
], BarchartComponent.prototype, "chartContainer", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
], BarchartComponent.prototype, "data", void 0);
BarchartComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-barchart',
        template: __webpack_require__(/*! raw-loader!./barchart.component.html */ "./node_modules/raw-loader/index.js!./src/app/charts/barchart/barchart.component.html"),
        encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
        styles: [__webpack_require__(/*! ./barchart.component.scss */ "./src/app/charts/barchart/barchart.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], BarchartComponent);



/***/ }),

/***/ "./src/app/gpx/gpx.service.ts":
/*!************************************!*\
  !*** ./src/app/gpx/gpx.service.ts ***!
  \************************************/
/*! exports provided: GpxService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GpxService", function() { return GpxService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _tmcw_togeojson__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @tmcw/togeojson */ "./node_modules/@tmcw/togeojson/dist/togeojson.umd.js");
/* harmony import */ var _tmcw_togeojson__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_tmcw_togeojson__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");





let GpxService = class GpxService {
    constructor(_httpClient) {
        this._httpClient = _httpClient;
    }
    getGeoJSON(url) {
        return this._httpClient.get(url, {
            responseType: 'text'
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])((data) => {
            const parser = new DOMParser();
            const rawGpx = parser.parseFromString(data, 'text/xml');
            return _tmcw_togeojson__WEBPACK_IMPORTED_MODULE_3__["gpx"](rawGpx);
        }));
    }
};
GpxService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
GpxService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], GpxService);



/***/ }),

/***/ "./src/app/helper.ts":
/*!***************************!*\
  !*** ./src/app/helper.ts ***!
  \***************************/
/*! exports provided: round, componentToHex, rgbToHex, hexToRgb, pickInterpolatedColor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "round", function() { return round; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "componentToHex", function() { return componentToHex; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "rgbToHex", function() { return rgbToHex; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hexToRgb", function() { return hexToRgb; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pickInterpolatedColor", function() { return pickInterpolatedColor; });
function round(value, n = 2) {
    if (!value) {
        return value;
    }
    const k = Math.pow(10, n);
    return Math.round(value * k) / k;
}
function componentToHex(c) {
    const hex = c.toString(16);
    return hex.length === 1 ? '0' + hex : hex;
}
function rgbToHex(rgb) {
    return '#' + componentToHex(rgb[0]) + componentToHex(rgb[1]) + componentToHex(rgb[2]);
}
function hexToRgb(hex) {
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16),
    } : null;
}
function pickInterpolatedColor(color1, color2, weight) {
    const w1 = weight;
    const w2 = 1 - w1;
    const rgb = [
        Math.round(color1[0] * w1 * 255 + color2[0] * w2 * 255),
        Math.round(color1[1] * w1 * 255 + color2[1] * w2 * 255),
        Math.round(color1[2] * w1 * 255 + color2[2] * w2 * 255)
    ];
    return rgb;
}


/***/ }),

/***/ "./src/app/map/b-router.service.ts":
/*!*****************************************!*\
  !*** ./src/app/map/b-router.service.ts ***!
  \*****************************************/
/*! exports provided: endpointBRouterAPI, BRouterService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "endpointBRouterAPI", function() { return endpointBRouterAPI; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BRouterService", function() { return BRouterService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");





const endpointBRouterAPI = 'http://h2096617.stratoserver.net:443/_getRouteFromBRouterAPI';
const parseCoordinate = (coord) => {
    return (+coord) / Math.pow(10, 6);
};
let BRouterService = class BRouterService {
    constructor(_httpClient) {
        this._httpClient = _httpClient;
    }
    fetchRoute(a, b) {
        // const url = 'https://brouter.damsy.net/api/brouter?lonlats=5.920945,45.452786|5.951104,45.468318&' +
        //   'profile=shortest&alternativeidx=0&format=geojson';
        return this._httpClient.get(endpointBRouterAPI, {
            params: {
                lonlats: `${a[1]},${a[0]}|${b[1]},${b[0]}`,
                profile: 'shortest',
                alternativeidx: '0',
                format: 'geojson',
            },
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])((data) => {
            return data;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((e) => {
            // this.handleError(e);
            // Swallow
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(null);
        }));
    }
    parseCoordsFromBRouterMessages(bRouterMessages) {
        const nodes = [];
        for (let i = 1; i < bRouterMessages.length; i++) {
            nodes.push([
                parseCoordinate(bRouterMessages[i][1]),
                parseCoordinate(bRouterMessages[i][0]),
            ]);
        }
        return nodes;
    }
    handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Error: ${error.error.message}`;
        }
        else {
            // server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        // window.alert(errorMessage);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(errorMessage);
    }
};
BRouterService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
BRouterService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], BRouterService);



/***/ }),

/***/ "./src/app/map/gpx.helper.ts":
/*!***********************************!*\
  !*** ./src/app/map/gpx.helper.ts ***!
  \***********************************/
/*! exports provided: convertToGpx */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "convertToGpx", function() { return convertToGpx; });
function convertToGpx(route) {
    return;
}


/***/ }),

/***/ "./src/app/map/interfaces.ts":
/*!***********************************!*\
  !*** ./src/app/map/interfaces.ts ***!
  \***********************************/
/*! exports provided: ClickType, OsmType */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClickType", function() { return ClickType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OsmType", function() { return OsmType; });
var ClickType;
(function (ClickType) {
    ClickType[ClickType["Left"] = 2] = "Left";
    ClickType[ClickType["Right"] = 1] = "Right";
})(ClickType || (ClickType = {}));
var OsmType;
(function (OsmType) {
    OsmType["Node"] = "node";
    OsmType["Way"] = "way";
    OsmType["Relation"] = "relation";
})(OsmType || (OsmType = {}));


/***/ }),

/***/ "./src/app/map/leaflet.helper.ts":
/*!***************************************!*\
  !*** ./src/app/map/leaflet.helper.ts ***!
  \***************************************/
/*! exports provided: haversineFormat, isMarkerOnMap, getLatLngTuple, customHaversine, getClosestCoordinate */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "haversineFormat", function() { return haversineFormat; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isMarkerOnMap", function() { return isMarkerOnMap; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getLatLngTuple", function() { return getLatLngTuple; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "customHaversine", function() { return customHaversine; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getClosestCoordinate", function() { return getClosestCoordinate; });
/* harmony import */ var haversine__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! haversine */ "./node_modules/haversine/haversine.js");
/* harmony import */ var haversine__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(haversine__WEBPACK_IMPORTED_MODULE_0__);

const haversineFormat = { format: '[lat,lon]' };
function isMarkerOnMap(lMap, marker) {
    return lMap._layers[marker._leaflet_id];
}
function getLatLngTuple(marker) {
    const latLng = marker._latlng;
    return [latLng.lat, latLng.lng];
}
function customHaversine(latLng1, latLng2) {
    const d = haversine__WEBPACK_IMPORTED_MODULE_0__(latLng1, latLng2, haversineFormat);
    // if (!d) {
    //   throw Error(`haversine error, coord1: ${latLng1}, coord2: ${latLng2}`);
    // }
    return d || 0;
}
function getClosestCoordinate(polyline, latLng) {
    const latLngs = polyline._latlngs;
    let minDist = Infinity;
    let closetCoord = null;
    for (const coord of latLngs) {
        const polylineLatLng = [coord.lat, coord.lng];
        const distance = customHaversine(polylineLatLng, latLng);
        if (distance < minDist) {
            minDist = distance;
            closetCoord = polylineLatLng;
        }
    }
    return closetCoord;
}


/***/ }),

/***/ "./src/app/map/leaflet.media.ts":
/*!**************************************!*\
  !*** ./src/app/map/leaflet.media.ts ***!
  \**************************************/
/*! exports provided: customIcon, routingPolylineOptions, waypointMarker */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "customIcon", function() { return customIcon; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routingPolylineOptions", function() { return routingPolylineOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "waypointMarker", function() { return waypointMarker; });
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! leaflet */ "./node_modules/leaflet/dist/leaflet-src.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(leaflet__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _interfaces__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./interfaces */ "./src/app/map/interfaces.ts");


const customIcon = (clickType) => leaflet__WEBPACK_IMPORTED_MODULE_0__["icon"]({
    iconUrl: clickType === _interfaces__WEBPACK_IMPORTED_MODULE_1__["ClickType"].Left ? '/assets/peak.svg' : '/assets/saddle.svg',
    // shadowUrl: 'icon-shadow.png',
    // iconSize:     [64, 64]
    // shadowSize:   [50, 64]
    iconAnchor: [32, 64],
    // shadowAnchor: [32, 64]
    popupAnchor: [-3, -76],
});
const routingPolylineOptions = {
    color: 'red',
    opacity: 0.7,
};
const waypointMarker = (markerType) => leaflet__WEBPACK_IMPORTED_MODULE_0__["icon"]({
    iconUrl: `/assets/markers/${markerType}.png`,
    iconSize: [33, 50],
    iconAnchor: [16, 45],
    shadowSize: [50, 50],
    shadowAnchor: [16, 47],
    popupAnchor: [0, -45],
    // iconAnchor: [-15, 64],
    // popupAnchor: [-3, -76],
    // popupAnchor:  [0, -40],
    shadowUrl: '/assets/markers/pin-shadow.png',
});


/***/ }),

/***/ "./src/app/map/leaflet.popup.scss":
/*!****************************************!*\
  !*** ./src/app/map/leaflet.popup.scss ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "::ng-deep .popupStyle .leaflet-popup-content-wrapper {\n  background: #182b33;\n  color: #ffffff;\n}\n::ng-deep .popupStyle .leaflet-popup-tip {\n  background: #182b33;\n}\n::ng-deep .popupStyle a {\n  color: #ffffff;\n}\n::ng-deep .leaflet-control-layers {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2tyaXMvRG9jdW1lbnRzL3JlcG9zL2tyaXMvc3VtbWl0cy9zcmMvYXBwL21hcC9sZWFmbGV0LnBvcHVwLnNjc3MiLCJzcmMvYXBwL21hcC9sZWFmbGV0LnBvcHVwLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBSUk7RUFDRSxtQkFMSztFQU1MLGNBQUE7QUNITjtBRE1JO0VBQ0UsbUJBVks7QUNNWDtBRE9JO0VBQ0UsY0FBQTtBQ0xOO0FEU0U7RUFDRSxpREFBQTtBQ1BKIiwiZmlsZSI6InNyYy9hcHAvbWFwL2xlYWZsZXQucG9wdXAuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiRkZWVwQmx1ZTogIzE4MmIzMztcblxuOjpuZy1kZWVwIHtcbiAgLnBvcHVwU3R5bGUge1xuICAgIC5sZWFmbGV0LXBvcHVwLWNvbnRlbnQtd3JhcHBlciB7XG4gICAgICBiYWNrZ3JvdW5kOiAkZGVlcEJsdWU7XG4gICAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICB9XG5cbiAgICAubGVhZmxldC1wb3B1cC10aXAge1xuICAgICAgYmFja2dyb3VuZDogJGRlZXBCbHVlO1xuICAgIH1cblxuICAgIGEge1xuICAgICAgY29sb3I6ICNmZmZmZmY7XG4gICAgfVxuICB9XG5cbiAgLmxlYWZsZXQtY29udHJvbC1sYXllcnMge1xuICAgIGZvbnQtZmFtaWx5OiBSb2JvdG8sIFwiSGVsdmV0aWNhIE5ldWVcIiwgc2Fucy1zZXJpZjtcbiAgfVxufVxuIiwiOjpuZy1kZWVwIC5wb3B1cFN0eWxlIC5sZWFmbGV0LXBvcHVwLWNvbnRlbnQtd3JhcHBlciB7XG4gIGJhY2tncm91bmQ6ICMxODJiMzM7XG4gIGNvbG9yOiAjZmZmZmZmO1xufVxuOjpuZy1kZWVwIC5wb3B1cFN0eWxlIC5sZWFmbGV0LXBvcHVwLXRpcCB7XG4gIGJhY2tncm91bmQ6ICMxODJiMzM7XG59XG46Om5nLWRlZXAgLnBvcHVwU3R5bGUgYSB7XG4gIGNvbG9yOiAjZmZmZmZmO1xufVxuOjpuZy1kZWVwIC5sZWFmbGV0LWNvbnRyb2wtbGF5ZXJzIHtcbiAgZm9udC1mYW1pbHk6IFJvYm90bywgXCJIZWx2ZXRpY2EgTmV1ZVwiLCBzYW5zLXNlcmlmO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/map/map.component.scss":
/*!****************************************!*\
  !*** ./src/app/map/map.component.scss ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@charset \"UTF-8\";\ndiv {\n  width: 100%;\n  height: 100%;\n  cursor: auto;\n}\n#map  {\n  cursor: crosshair;\n  z-index: 1;\n}\n::ng-deep .leaflet-div-icon-violet {\n  background: rgba(130, 77, 241, 0.56);\n  border: 2px solid #370d82;\n  border-radius: 100%;\n}\n::ng-deep .leaflet-div-icon-violet:hover {\n  background: #370d82;\n}\n::ng-deep .leaflet-div-icon-red {\n  background: rgba(241, 73, 76, 0.56);\n  border: 2px solid rgba(255, 21, 19, 0.82);\n  border-radius: 100%;\n}\n::ng-deep .leaflet-div-icon-red:hover {\n  background: rgba(255, 21, 19, 0.82);\n}\n::ng-deep .leaflet-div-icon-yellow {\n  background: rgba(241, 219, 69, 0.56);\n  border: 2px solid rgba(255, 206, 21, 0.82);\n  border-radius: 100%;\n}\n::ng-deep .leaflet-div-icon-green {\n  background: rgba(143, 241, 93, 0.56);\n  border: 2px solid rgba(43, 201, 38, 0.82);\n  border-radius: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFwL21hcC5jb21wb25lbnQuc2NzcyIsIi9ob21lL2tyaXMvRG9jdW1lbnRzL3JlcG9zL2tyaXMvc3VtbWl0cy9zcmMvYXBwL21hcC9tYXAuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsZ0JBQWdCO0FDTWhCO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0FESkY7QUNPQTtFQUNFLGlCQUFBO0VBQ0EsVUFBQTtBREpGO0FDUUU7RUFDRSxvQ0FBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7QURMSjtBQ09JO0VBQ0UsbUJBckJVO0FEZ0JoQjtBQ1NFO0VBQ0UsbUNBQUE7RUFDQSx5Q0FBQTtFQUNBLG1CQUFBO0FEUEo7QUNTSTtFQUNFLG1DQTlCTztBRHVCYjtBQ1dFO0VBQ0Usb0NBQUE7RUFDQSwwQ0FBQTtFQUNBLG1CQUFBO0FEVEo7QUNZRTtFQUNFLG9DQUFBO0VBQ0EseUNBQUE7RUFDQSxtQkFBQTtBRFZKIiwiZmlsZSI6InNyYy9hcHAvbWFwL21hcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBjaGFyc2V0IFwiVVRGLThcIjtcbmRpdiB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGN1cnNvcjogYXV0bztcbn1cblxuI21hcMKgIHtcbiAgY3Vyc29yOiBjcm9zc2hhaXI7XG4gIHotaW5kZXg6IDE7XG59XG5cbjo6bmctZGVlcCAubGVhZmxldC1kaXYtaWNvbi12aW9sZXQge1xuICBiYWNrZ3JvdW5kOiByZ2JhKDEzMCwgNzcsIDI0MSwgMC41Nik7XG4gIGJvcmRlcjogMnB4IHNvbGlkICMzNzBkODI7XG4gIGJvcmRlci1yYWRpdXM6IDEwMCU7XG59XG46Om5nLWRlZXAgLmxlYWZsZXQtZGl2LWljb24tdmlvbGV0OmhvdmVyIHtcbiAgYmFja2dyb3VuZDogIzM3MGQ4Mjtcbn1cbjo6bmctZGVlcCAubGVhZmxldC1kaXYtaWNvbi1yZWQge1xuICBiYWNrZ3JvdW5kOiByZ2JhKDI0MSwgNzMsIDc2LCAwLjU2KTtcbiAgYm9yZGVyOiAycHggc29saWQgcmdiYSgyNTUsIDIxLCAxOSwgMC44Mik7XG4gIGJvcmRlci1yYWRpdXM6IDEwMCU7XG59XG46Om5nLWRlZXAgLmxlYWZsZXQtZGl2LWljb24tcmVkOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogcmdiYSgyNTUsIDIxLCAxOSwgMC44Mik7XG59XG46Om5nLWRlZXAgLmxlYWZsZXQtZGl2LWljb24teWVsbG93IHtcbiAgYmFja2dyb3VuZDogcmdiYSgyNDEsIDIxOSwgNjksIDAuNTYpO1xuICBib3JkZXI6IDJweCBzb2xpZCByZ2JhKDI1NSwgMjA2LCAyMSwgMC44Mik7XG4gIGJvcmRlci1yYWRpdXM6IDEwMCU7XG59XG46Om5nLWRlZXAgLmxlYWZsZXQtZGl2LWljb24tZ3JlZW4ge1xuICBiYWNrZ3JvdW5kOiByZ2JhKDE0MywgMjQxLCA5MywgMC41Nik7XG4gIGJvcmRlcjogMnB4IHNvbGlkIHJnYmEoNDMsIDIwMSwgMzgsIDAuODIpO1xuICBib3JkZXItcmFkaXVzOiAxMDAlO1xufSIsIlxuJGRlZXBCbHVlOiAjMTgyYjMzO1xuJHByaW1hcnlCbHVlOiAjMDA5M2ZmZDE7XG4kcHJpbWFyeVZpb2xldDogIzM3MGQ4MjtcbiRwcmltYXJ5UmVkOiByZ2JhKDI1NSwgMjEsIDE5LCAwLjgyKTtcblxuZGl2IHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgY3Vyc29yOiBhdXRvO1xufVxuXG4jbWFwwqB7XG4gIGN1cnNvcjogY3Jvc3NoYWlyO1xuICB6LWluZGV4OiAxO1xufVxuXG46Om5nLWRlZXAge1xuICAubGVhZmxldC1kaXYtaWNvbi12aW9sZXQge1xuICAgIGJhY2tncm91bmQ6IHJnYmEoMTMwLCA3NywgMjQxLCAwLjU2KTtcbiAgICBib3JkZXI6IDJweCBzb2xpZCAkcHJpbWFyeVZpb2xldDtcbiAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xuXG4gICAgJjpob3ZlciB7XG4gICAgICBiYWNrZ3JvdW5kOiAkcHJpbWFyeVZpb2xldDtcbiAgICB9XG4gIH1cblxuICAubGVhZmxldC1kaXYtaWNvbi1yZWQge1xuICAgIGJhY2tncm91bmQ6IHJnYmEoMjQxLCA3MywgNzYsIDAuNTYpO1xuICAgIGJvcmRlcjogMnB4IHNvbGlkICRwcmltYXJ5UmVkO1xuICAgIGJvcmRlci1yYWRpdXM6IDEwMCU7XG5cbiAgICAmOmhvdmVyIHtcbiAgICAgIGJhY2tncm91bmQ6ICRwcmltYXJ5UmVkO1xuICAgIH1cbiAgfVxuXG4gIC5sZWFmbGV0LWRpdi1pY29uLXllbGxvdyB7XG4gICAgYmFja2dyb3VuZDogcmdiYSgyNDEsIDIxOSwgNjksIDAuNTYpO1xuICAgIGJvcmRlcjogMnB4IHNvbGlkIHJnYmEoMjU1LCAyMDYsIDIxLCAwLjgyKTtcbiAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xuICB9XG5cbiAgLmxlYWZsZXQtZGl2LWljb24tZ3JlZW4ge1xuICAgIGJhY2tncm91bmQ6IHJnYmEoMTQzLCAyNDEsIDkzLCAwLjU2KTtcbiAgICBib3JkZXI6IDJweCBzb2xpZCByZ2JhKDQzLCAyMDEsIDM4LCAwLjgyKTtcbiAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xuICB9XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/map/map.component.ts":
/*!**************************************!*\
  !*** ./src/app/map/map.component.ts ***!
  \**************************************/
/*! exports provided: MapComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapComponent", function() { return MapComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! leaflet */ "./node_modules/leaflet/dist/leaflet-src.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(leaflet__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var pngjs_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! pngjs/browser */ "./node_modules/pngjs/browser.js");
/* harmony import */ var pngjs_browser__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(pngjs_browser__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _helper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../helper */ "./src/app/helper.ts");
/* harmony import */ var _gpx_gpx_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../gpx/gpx.service */ "./src/app/gpx/gpx.service.ts");
/* harmony import */ var _overpass_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./overpass.service */ "./src/app/map/overpass.service.ts");
/* harmony import */ var _popup_popup_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./popup/popup.component */ "./src/app/map/popup/popup.component.ts");
/* harmony import */ var _osm_helper__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./osm.helper */ "./src/app/map/osm.helper.ts");
/* harmony import */ var _tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./tiles.endpoints */ "./src/app/map/tiles.endpoints.ts");
/* harmony import */ var _media_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./media.service */ "./src/app/map/media.service.ts");
/* harmony import */ var _routing_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./routing.service */ "./src/app/map/routing.service.ts");
/* harmony import */ var _interfaces__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./interfaces */ "./src/app/map/interfaces.ts");














let MapComponent = class MapComponent {
    constructor(_changeDetectorRef, _injector, _activatedRoute, _router, _componentFactoryResolver, _overpassService, _mediaService, _gpxService, _routingService) {
        this._changeDetectorRef = _changeDetectorRef;
        this._injector = _injector;
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._componentFactoryResolver = _componentFactoryResolver;
        this._overpassService = _overpassService;
        this._mediaService = _mediaService;
        this._gpxService = _gpxService;
        this._routingService = _routingService;
        this._lat = 45.3426746;
        this._lon = 5.77443122;
        this._zoom = 14;
        this._overpass_data = [];
        this.overpassDisplayedNodes = [];
    }
    ngOnInit() {
        const baseMaps = {
            'OpenTopoMap': leaflet__WEBPACK_IMPORTED_MODULE_3__["tileLayer"](_tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["tileServers"].openTopoMap.url, _tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["tileServers"].openTopoMap.options),
            'OpenStreetMap': leaflet__WEBPACK_IMPORTED_MODULE_3__["tileLayer"](_tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["tileServers"].openStreetMap.url),
            'Strava': leaflet__WEBPACK_IMPORTED_MODULE_3__["tileLayer"](_tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["tileServers"].strava.url),
            'Mapbox: grey-scale': leaflet__WEBPACK_IMPORTED_MODULE_3__["tileLayer"](_tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["mapboxTiles"].grayscale.url, _tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["mapboxTiles"].grayscale.options),
            'Mapbox: streets': leaflet__WEBPACK_IMPORTED_MODULE_3__["tileLayer"](_tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["mapboxTiles"].streets.url, _tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["mapboxTiles"].streets.options),
            'Mapbox: satellite': leaflet__WEBPACK_IMPORTED_MODULE_3__["tileLayer"](_tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["mapboxTiles"].satellite.url, _tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["mapboxTiles"].satellite.options),
            'Mapbox: strava': leaflet__WEBPACK_IMPORTED_MODULE_3__["tileLayer"](_tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["mapboxTiles"].strava.url, _tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["mapboxTiles"].strava.options),
        };
        const overlayMaps = {
            'Waymarked trail': leaflet__WEBPACK_IMPORTED_MODULE_3__["tileLayer"](_tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["overlay"].waymarkedtrail.url),
            'Suunto AllTrails': leaflet__WEBPACK_IMPORTED_MODULE_3__["tileLayer"](Object(_tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["suuntoHeatmap"])('AllTrails'), _tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["overlay"].suunto.options),
            'Suunto AllDownhill': leaflet__WEBPACK_IMPORTED_MODULE_3__["tileLayer"](Object(_tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["suuntoHeatmap"])('AllDownhill'), _tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["overlay"].suunto.options),
            'Suunto Running': leaflet__WEBPACK_IMPORTED_MODULE_3__["tileLayer"](Object(_tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["suuntoHeatmap"])('Running'), _tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["overlay"].suunto.options),
            'Suunto TrailRunning': leaflet__WEBPACK_IMPORTED_MODULE_3__["tileLayer"](Object(_tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["suuntoHeatmap"])('TrailRunning'), _tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["overlay"].suunto.options),
            'Suunto Mountaineering': leaflet__WEBPACK_IMPORTED_MODULE_3__["tileLayer"](Object(_tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["suuntoHeatmap"])('Mountaineering'), _tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["overlay"].suunto.options),
        };
        this.map = leaflet__WEBPACK_IMPORTED_MODULE_3__["map"](this.mapElement.nativeElement, {
            center: [this._lat, this._lon],
            zoom: this._zoom,
            layers: [baseMaps.OpenTopoMap],
            zoomControl: true,
        });
        this._routingService.initMap(this.map);
        leaflet__WEBPACK_IMPORTED_MODULE_3__["control"].layers(baseMaps, overlayMaps).addTo(this.map);
        this._centerMapFromFragment();
        this.map.on('moveend', (e) => {
            this._navigate();
            this._onMovingMap();
        });
        // this.map.on('click', (e: any) => {
        //   console.log('Lat, Lon : ' + e.latlng.lat + ', ' + e.latlng.lng);
        //   // const baseUrl = suuntoHeatmap('AllTrails');
        //   const baseUrl = suuntoHeatmap('AllTrails');
        //   const zoom = 14;
        //
        //   const tileRef = {
        //     y: latTotile(e.latlng.lat, zoom),
        //     x: lngTotile(e.latlng.lng, zoom),
        //     zoom: zoom,
        //   };
        //
        //   const url = getTileUrl(baseUrl, tileRef);
        //
        //   const tileBounds = getTileBounds(tileRef);
        //
        //   this._mediaService.getImage(url)
        //   .subscribe((arrayBuffer) => {
        //     new PNG({ filterType: 4 }).parse(arrayBuffer, (error, data: { width: number, height: number, data: number[] }) => {
        //       const gamma = this._mediaService.getGamma(e.latlng.lat, e.latlng.lng, tileBounds, data);
        //       console.log('gamma', gamma);
        //     });
        //   });
        // });
        // this._getGamma(128445436);
        // this._getGamma(110332196);
        // this.map.on('baselayerchange', (e) => {
        //   console.log((e as any).layer);
        // });
        this.map.on('keypress', () => {
            // this._sacScale();
            // this._printPeaks();
            this._wayPopularity();
            // this._overpassService.getPeaks(this.map.getBounds(), Natural.Saddle)
            // .subscribe((peaks) => {
            //   console.log('data', peaks);
            //   for (const peak of peaks) {
            //     if (peak.tags && peak.tags.name) {
            //       console.log('', peak.tags.name);
            //     }
            //   }
            // });
        });
        this.map.on('click', (e) => this._routingService
            .navigate(this.map, [e.latlng.lat, e.latlng.lng], _interfaces__WEBPACK_IMPORTED_MODULE_13__["ClickType"].Left, null));
        this.map.on('contextmenu', (e) => this._routingService
            .navigate(this.map, [e.latlng.lat, e.latlng.lng], _interfaces__WEBPACK_IMPORTED_MODULE_13__["ClickType"].Right, null));
        this.map.on('mousemove', (e) => this._routingService
            .mouseOverPolyline(this.map, [e.latlng.lat, e.latlng.lng]));
        // this._gpxService.getGeoJSON('assets/1.gpx')
        this._gpxService.getGeoJSON('assets/diag.gpx')
            .subscribe((data) => {
            const coordinates = data.features['0'].geometry.coordinates;
            const latlngs = coordinates.map(c => [c[1], c[0]]);
            const elevation = coordinates.map(c => c[2]);
            leaflet__WEBPACK_IMPORTED_MODULE_3__["polyline"](latlngs).addTo(this.map);
        });
    }
    ngOnChanges() {
    }
    _navigate() {
        const z = this.map.getZoom();
        const center = this.map.getCenter();
        const lat = Object(_helper__WEBPACK_IMPORTED_MODULE_5__["round"])(center.lat, 4);
        const lng = Object(_helper__WEBPACK_IMPORTED_MODULE_5__["round"])(center.lng, 4);
        // this.map.eachLayer((layer) => {
        //   if (layer instanceof L.TileLayer && (layer as any)._url) {
        //     console.log('layer', layer);
        //   }
        // });
        this._router.navigate([''], {
            fragment: `map=${z}/${lat}/${lng}`,
            queryParams: {
            // 'layer': 'test',
            },
        });
    }
    _onMovingMap() {
        if (this.activePOI.includes('water')) {
            console.log('this.activePOI', this.activePOI);
            this._printPOI();
        }
        else {
            this._clearPOI();
        }
    }
    // private _onMapClick(e) {
    //   console.log('Lat, Lon : ' + e.latlng.lat + ', ' + e.latlng.lng);
    //   const baseUrl = suuntoHeatmap('AllTrails');
    //   const zoom = 14;
    //
    //   const url = getTileUrl(baseUrl, {
    //     y: latTotile(e.latlng.lat, zoom),
    //     x: lngTotile(e.latlng.lng, zoom),
    //     zoom: zoom,
    //   });
    //
    //   console.log('url', this._overpassService);
    //   this._overpassService.getImage(url)
    //   .subscribe((arrayBuffer) => {
    //     new PNG({ filterType: 4 }).parse(arrayBuffer, (error, data) => {
    //       console.log('data', data.data.length);
    //       console.log('data 2', data.width * data.height * 4);
    //     });
    //   });
    // }
    _getGamma(way, displayTiles = false) {
        const tilesMap = new Map();
        if (way.latlngs) {
            const latlngs = way.latlngs;
            for (const latlng of latlngs) {
                const t = Object(_osm_helper__WEBPACK_IMPORTED_MODULE_9__["coordsToTile"])(latlng[0], latlng[1], 14);
                const s = `${t.zoom}/${t.x}/${t.y}`;
                if (!tilesMap.has(s)) {
                    tilesMap.set(s, t);
                }
            }
            // const baseUrl = suuntoHeatmap('AllTrails');
            const baseUrl = Object(_tiles_endpoints__WEBPACK_IMPORTED_MODULE_10__["suuntoHeatmap"])('Running');
            const sources = [];
            for (const key of Array.from(tilesMap.keys())) {
                const tile = tilesMap.get(key);
                const url = Object(_osm_helper__WEBPACK_IMPORTED_MODULE_9__["getTileUrl"])(baseUrl, tile);
                sources.push({
                    url,
                    tile,
                });
                if (displayTiles) {
                    const tileBounds = Object(_osm_helper__WEBPACK_IMPORTED_MODULE_9__["getTileBounds"])(tile);
                    leaflet__WEBPACK_IMPORTED_MODULE_3__["imageOverlay"](url, tileBounds).addTo(this.map);
                }
            }
            this._mediaService.requestDataFromMultipleSources(sources)
                .subscribe((tiles) => {
                const promises = [];
                for (const tile of tiles) {
                    const promise = new Promise((res) => {
                        new pngjs_browser__WEBPACK_IMPORTED_MODULE_4__["PNG"]({ filterType: 4 }).parse(tile.arrayBuffer, (error, data) => {
                            const tileBounds = Object(_osm_helper__WEBPACK_IMPORTED_MODULE_9__["getTileBounds"])(tile.tile);
                            const latlngsInTile = Object(_osm_helper__WEBPACK_IMPORTED_MODULE_9__["filterLngLatInTile"])(latlngs, tileBounds);
                            let g = 0;
                            for (const tuple of latlngsInTile) {
                                g += this._mediaService.getGamma(tuple[0], tuple[1], tileBounds, data);
                            }
                            if (isNaN(g)) {
                                throw new Error(`Tile: ${tile} is nan, way ${way.id}`);
                            }
                            res(g);
                        });
                    });
                    promises.push(promise);
                }
                Promise.all(promises)
                    .then((gammas) => {
                    const gamma = gammas.reduce((a, b) => a + b, 0) / latlngs.length / 256;
                    const polylineOptions = {
                        color: Object(_helper__WEBPACK_IMPORTED_MODULE_5__["rgbToHex"])(Object(_helper__WEBPACK_IMPORTED_MODULE_5__["pickInterpolatedColor"])([1, 0, 0], [0, 0, 1], gamma)),
                        opacity: 1,
                    };
                    const distance = Object(_helper__WEBPACK_IMPORTED_MODULE_5__["round"])(Object(_osm_helper__WEBPACK_IMPORTED_MODULE_9__["getDistance"])(way), 2);
                    const hyperlink = `<a href="${Object(_osm_helper__WEBPACK_IMPORTED_MODULE_9__["osmEleUrl"])(_interfaces__WEBPACK_IMPORTED_MODULE_13__["OsmType"].Way, way.id)}" target="_blank">OSM: ${way.id}<a> `;
                    const popupContent = hyperlink + ` distance: ${distance} km \n` + `${gamma * 100} %` + JSON.stringify(way.tags, null, 2);
                    leaflet__WEBPACK_IMPORTED_MODULE_3__["polyline"](latlngs, polylineOptions).addTo(this.map)
                        .bindPopup(popupContent);
                });
            });
        }
    }
    _printPOI() {
        if (!this.map || this.map.getZoom() < 9) {
            return;
        }
        this._overpassService.getPOI(this.map.getBounds(), '["amenity"="drinking_water"]')
            .subscribe((nodes) => {
            const toDeleteNodes = this.overpassDisplayedNodes.filter(n => !nodes.some(node => node.id === n.id));
            for (const node of toDeleteNodes) {
                this.map.removeLayer(node.marker);
            }
            // keep only returned ones
            this.overpassDisplayedNodes = this.overpassDisplayedNodes.filter(n => nodes.some(node => node.id === n.id));
            for (const node of nodes) {
                if (this.overpassDisplayedNodes.find(n => n.id === node.id)) {
                    continue;
                }
                // const polyline = L.Point(way.latlngs, option).addTo(this.map)
                // .bindPopup(popupContent, popupOptions);
                // const factory = this._componentFactoryResolver.resolveComponentFactory(PopupComponent);
                // const compRef: ComponentRef<any> = col.createComponent(factory);
                // this.compRef = compFactory.create(this.injector);
                // https://stackoverflow.com/questions/40922224/angular2-component-into-dynamically-created-element
                const compFactory = this._componentFactoryResolver.resolveComponentFactory(_popup_popup_component__WEBPACK_IMPORTED_MODULE_8__["PopupComponent"]);
                this.compRef = compFactory.create(this._injector);
                const hyperlink = `<a href="${Object(_osm_helper__WEBPACK_IMPORTED_MODULE_9__["osmEleUrl"])(_interfaces__WEBPACK_IMPORTED_MODULE_13__["OsmType"].Node, node.id)}" target="_blank">OSM: ${node.id}<a> `;
                const popupContent = hyperlink + JSON.stringify(node.tags, null, 2);
                // TODO not working
                this.compRef.instance.test = popupContent;
                const div = document.createElement('div');
                div.appendChild(this.compRef.location.nativeElement);
                // this._changeDetectorRef.detectChanges();
                // const { icon: customIcon }
                const marker = leaflet__WEBPACK_IMPORTED_MODULE_3__["marker"]([node.lat, node.lon]).addTo(this.map)
                    .bindPopup(popupContent);
                this.overpassDisplayedNodes.push(Object.assign({}, node, { marker }));
            }
        });
    }
    _printPeaks() {
        if (this.map.getZoom() < 9) {
            return;
        }
        this._overpassService.getPeaks(this.map.getBounds(), _overpass_service__WEBPACK_IMPORTED_MODULE_7__["Natural"].Peak)
            .subscribe((nodes) => {
            for (const node of nodes) {
                // const polyline = L.Point(way.latlngs, option).addTo(this.map)
                // .bindPopup(popupContent, popupOptions);
                // const factory = this._componentFactoryResolver.resolveComponentFactory(PopupComponent);
                // const compRef: ComponentRef<any> = col.createComponent(factory);
                // this.compRef = compFactory.create(this.injector);
                // https://stackoverflow.com/questions/40922224/angular2-component-into-dynamically-created-element
                const compFactory = this._componentFactoryResolver.resolveComponentFactory(_popup_popup_component__WEBPACK_IMPORTED_MODULE_8__["PopupComponent"]);
                this.compRef = compFactory.create(this._injector);
                const hyperlink = `<a href="${Object(_osm_helper__WEBPACK_IMPORTED_MODULE_9__["osmEleUrl"])(_interfaces__WEBPACK_IMPORTED_MODULE_13__["OsmType"].Node, node.id)}" target="_blank">OSM: ${node.id}<a> `;
                const popupContent = hyperlink + JSON.stringify(node.tags, null, 2);
                // TODO not working
                this.compRef.instance.test = popupContent;
                const div = document.createElement('div');
                div.appendChild(this.compRef.location.nativeElement);
                // this._changeDetectorRef.detectChanges();
                // const { icon: customIcon }
                leaflet__WEBPACK_IMPORTED_MODULE_3__["marker"]([node.lat, node.lon]).addTo(this.map)
                    .bindPopup(popupContent);
            }
        });
    }
    _centerMapFromFragment() {
        this._activatedRoute.fragment
            .subscribe((fragment) => {
            const re = new RegExp(/(\d*)\/([\d-\.]*)\/([\d-\.]*)/);
            const found = re.exec(fragment);
            if (found && found.length) {
                this._zoom = +found[1];
                this._lat = +found[2];
                this._lon = +found[3];
            }
            this.map.setView([this._lat, this._lon], this._zoom);
        });
    }
    _wayPopularity() {
        if (this.map.getZoom() < 13) {
            console.log('only below zoom 13');
            return;
        }
        console.log('dbl click');
        this._overpassService.getWays('["highway"]', this.map.getBounds())
            .subscribe((ways) => {
            for (const way of ways) {
                console.log('way.id', way.id);
                this._getGamma(way);
            }
        });
    }
    _sacScale() {
        if (this.map.getZoom() < 13) {
            return;
        }
        // private _extractLatLng(data: IFit): LatLngTuple[] {
        //     return data.content.records.map(r => [r.position_lat, r.position_long]).filter(r => !!r[0]);
        //   }
        const popupOptions = {
            maxWidth: 500,
            className: 'popupStyle',
            closeButton: true,
        };
        const polylineOptions = {
            color: 'red',
        };
        this._overpassService.getWays(this.state, 
        // '["trail_visibility"]',
        this.map.getBounds())
            .subscribe((ways) => {
            console.log('query', this.state);
            console.log('', ways);
            const highwayTypes = new Map();
            console.log('this._overpass_data.length', this._overpass_data.length);
            for (const way of ways) {
                if (this._overpass_data.includes(way.id)) {
                    continue;
                }
                if (way.latlngs) {
                    this._overpass_data.push(way.id);
                    const distance = Object(_helper__WEBPACK_IMPORTED_MODULE_5__["round"])(Object(_osm_helper__WEBPACK_IMPORTED_MODULE_9__["getDistance"])(way), 2);
                    const hyperlink = `<a href="${Object(_osm_helper__WEBPACK_IMPORTED_MODULE_9__["osmEleUrl"])(_interfaces__WEBPACK_IMPORTED_MODULE_13__["OsmType"].Way, way.id)}" target="_blank">OSM: ${way.id}<a> `;
                    const popupContent = hyperlink + ` distance: ${distance} km \n` + JSON.stringify(way.tags, null, 2);
                    const option = Object.assign({}, polylineOptions);
                    if (way.tags && way.tags.sac_scale) {
                        switch (way.tags.sac_scale) {
                            case 'hiking':
                                option.color = 'green';
                                break;
                            case 'mountain_hiking':
                                option.color = 'blue';
                                break;
                            case 'demanding_mountain_hiking':
                                option.color = 'blue';
                                option.dashArray = '10 5';
                                break;
                            case 'alpine_hiking':
                                option.color = 'red';
                                break;
                            case 'demanding_alpine_hiking':
                                option.color = 'black';
                                break;
                            case 'difficult_alpine_hiking':
                                option.color = 'black';
                                option.dashArray = '10 5';
                                break;
                        }
                    }
                    if (way.tags && way.tags.trail_visibility) {
                        switch (way.tags.trail_visibility) {
                            case 'excellent':
                                option.color = 'green';
                                break;
                            case 'good':
                                option.color = 'blue';
                                break;
                            case 'intermediate':
                                option.color = 'blue';
                                option.dashArray = '10 5';
                                break;
                            case 'bad':
                                option.color = 'red';
                                break;
                            case 'horrible':
                                option.color = 'black';
                                break;
                            case 'no':
                                option.color = 'black';
                                option.dashArray = '10 5';
                                break;
                        }
                    }
                    if (way.tags && way.tags.highway) {
                        const highway = way.tags.highway;
                        if (highwayTypes.has(highway)) {
                            highwayTypes.set(highway, highwayTypes.get(highway) + 1);
                        }
                        else {
                            highwayTypes.set(highway, 1);
                        }
                    }
                    const polyline = leaflet__WEBPACK_IMPORTED_MODULE_3__["polyline"](way.latlngs, option).addTo(this.map)
                        .bindPopup(popupContent, popupOptions);
                    polyline.on('mouseover', (e) => {
                        polyline.setStyle({
                            opacity: 0.5,
                        });
                    });
                    polyline.on('mouseout', (e) => {
                        polyline.setStyle({
                            opacity: 1,
                        });
                    });
                }
            }
            console.log('highwayTypes', highwayTypes);
        });
    }
    _clearPOI(type) {
        for (const node of this.overpassDisplayedNodes) {
            this.map.removeLayer(node.marker);
        }
        this.overpassDisplayedNodes = [];
    }
};
MapComponent.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injector"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ComponentFactoryResolver"] },
    { type: _overpass_service__WEBPACK_IMPORTED_MODULE_7__["OverpassService"] },
    { type: _media_service__WEBPACK_IMPORTED_MODULE_11__["MediaService"] },
    { type: _gpx_gpx_service__WEBPACK_IMPORTED_MODULE_6__["GpxService"] },
    { type: _routing_service__WEBPACK_IMPORTED_MODULE_12__["RoutingService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
], MapComponent.prototype, "state", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
], MapComponent.prototype, "activePOI", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('map', { static: true }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
], MapComponent.prototype, "mapElement", void 0);
MapComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-map',
        template: __webpack_require__(/*! raw-loader!./map.component.html */ "./node_modules/raw-loader/index.js!./src/app/map/map.component.html"),
        styles: [__webpack_require__(/*! ./map.component.scss */ "./src/app/map/map.component.scss"), __webpack_require__(/*! ./leaflet.popup.scss */ "./src/app/map/leaflet.popup.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injector"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ComponentFactoryResolver"],
        _overpass_service__WEBPACK_IMPORTED_MODULE_7__["OverpassService"],
        _media_service__WEBPACK_IMPORTED_MODULE_11__["MediaService"],
        _gpx_gpx_service__WEBPACK_IMPORTED_MODULE_6__["GpxService"],
        _routing_service__WEBPACK_IMPORTED_MODULE_12__["RoutingService"]])
], MapComponent);



/***/ }),

/***/ "./src/app/map/media.service.ts":
/*!**************************************!*\
  !*** ./src/app/map/media.service.ts ***!
  \**************************************/
/*! exports provided: MediaService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MediaService", function() { return MediaService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");





let MediaService = class MediaService {
    constructor(_httpClient) {
        this._httpClient = _httpClient;
        this._tileRequests = [];
        this._tilesMap = new Map();
    }
    requestDataFromMultipleSources(sources) {
        const responses = [];
        for (const source of sources) {
            if (this._tilesMap.has(source.url)) {
                responses.push(Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(this._tilesMap.get(source.url)));
            }
            else {
                responses.push(this.getImage(source.url)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])((res) => {
                    const tileBuffer = {
                        tile: source.tile,
                        arrayBuffer: res,
                    };
                    this._tilesMap.set(source.url, tileBuffer);
                    return tileBuffer;
                }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(err => Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(err))));
            }
        }
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["forkJoin"])(responses);
    }
    getImage(imageUrl) {
        return this._httpClient.get(imageUrl, {
            observe: 'response',
            responseType: 'blob',
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["mergeMap"])((res) => {
            const blob = new Blob([res.body], { type: res.headers.get('Content-Type') });
            const arrayBuffer = new Response(blob).arrayBuffer();
            return arrayBuffer;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(err => Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(err)));
    }
    // TODO TU
    getGamma(lat, lng, tileBounds, data) {
        const north = tileBounds[0][0];
        const south = tileBounds[1][0];
        const west = tileBounds[0][1];
        const east = tileBounds[1][1];
        const x = Math.floor(Math.abs(west - lng) * data.width / Math.abs(west - east));
        const y = Math.floor(Math.abs(north - lat) * data.height / Math.abs(north - south));
        const i = y * data.width + x;
        // const pixel = {
        //   r: data.data[i * 4],
        //   v: data.data[i * 4 + 1],
        //   b: data.data[i * 4 + 2],
        //   g: data.data[i * 4 + 3],
        // };
        return data.data[i * 4 + 3];
    }
};
MediaService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
];
MediaService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root',
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
], MediaService);



/***/ }),

/***/ "./src/app/map/osm.helper.ts":
/*!***********************************!*\
  !*** ./src/app/map/osm.helper.ts ***!
  \***********************************/
/*! exports provided: osmEleUrl, getDistance, getDistanceLatLngs, getTotalAscent, lngTotile, latTotile, coordsToTile, nextTile, TileToCoords, getTileUrl, getTileBounds, extractLatLngsFromGeoJson, extractElevationFromGeoJson, getLatLngBoundsFromGeoJson, getLatLngBounds, filterLngLatInTile */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "osmEleUrl", function() { return osmEleUrl; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDistance", function() { return getDistance; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDistanceLatLngs", function() { return getDistanceLatLngs; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTotalAscent", function() { return getTotalAscent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lngTotile", function() { return lngTotile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "latTotile", function() { return latTotile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "coordsToTile", function() { return coordsToTile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "nextTile", function() { return nextTile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TileToCoords", function() { return TileToCoords; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTileUrl", function() { return getTileUrl; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTileBounds", function() { return getTileBounds; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "extractLatLngsFromGeoJson", function() { return extractLatLngsFromGeoJson; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "extractElevationFromGeoJson", function() { return extractElevationFromGeoJson; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getLatLngBoundsFromGeoJson", function() { return getLatLngBoundsFromGeoJson; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getLatLngBounds", function() { return getLatLngBounds; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "filterLngLatInTile", function() { return filterLngLatInTile; });
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! leaflet */ "./node_modules/leaflet/dist/leaflet-src.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(leaflet__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _leaflet_helper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./leaflet.helper */ "./src/app/map/leaflet.helper.ts");


const osmEleUrl = (type, id) => `https://www.openstreetmap.org/${type}/${id}`;
function getDistance(way) {
    if (!way.latlngs) {
        return null;
    }
    return way.latlngs.reduce((acc, latlng, index) => {
        if (index === 0) {
            return 0;
        }
        return acc + Object(_leaflet_helper__WEBPACK_IMPORTED_MODULE_1__["customHaversine"])(way.latlngs[index - 1], latlng);
    }, 0);
}
function getDistanceLatLngs(latLngs) {
    if (!latLngs) {
        return null;
    }
    return latLngs.reduce((acc, latlng, index) => {
        if (index === 0) {
            return 0;
        }
        return acc + Object(_leaflet_helper__WEBPACK_IMPORTED_MODULE_1__["customHaversine"])(latLngs[index - 1], latlng);
    }, 0);
}
function getTotalAscent(elevations) {
    if (!elevations) {
        return null;
    }
    return elevations.reduce((acc, ele, index) => {
        if (index === 0) {
            return [0, 0];
        }
        const diff = ele - elevations[index - 1];
        acc[0] += diff > 0 ? diff : 0;
        acc[1] += diff < 0 ? diff : 0;
        return acc;
    }, [0, 0]);
}
function lngTotile(lng, zoom) {
    return Math.floor((lng + 180) / 360 * Math.pow(2, zoom));
}
function latTotile(lat, zoom) {
    return Math.floor((1 - Math.log(Math.tan(lat * Math.PI / 180) + 1 / Math.cos(lat * Math.PI / 180)) / Math.PI) / 2 * Math.pow(2, zoom));
}
function coordsToTile(lat, lng, zoom) {
    return {
        zoom,
        y: latTotile(lat, zoom),
        x: lngTotile(lng, zoom),
    };
}
function tileToLng(x, z) {
    return (x / Math.pow(2, z) * 360 - 180);
}
function tileToLat(y, z) {
    const n = Math.PI - 2 * Math.PI * y / Math.pow(2, z);
    return (180 / Math.PI * Math.atan(0.5 * (Math.exp(n) - Math.exp(-n))));
}
function nextTile(tile) {
    return Object.assign({}, tile, { x: tile.x + 1, y: tile.y + 1 });
}
function TileToCoords(tile) {
    return [
        tileToLat(tile.y, tile.zoom),
        tileToLng(tile.x, tile.zoom),
    ];
}
function getTileUrl(tileServer, tile) {
    return tileServer
        .replace('{z}', tile.zoom.toString())
        .replace('{x}', tile.x.toString())
        .replace('{y}', tile.y.toString());
}
function getTileBounds(tile) {
    const coordsUpperLeft = TileToCoords(tile);
    const coordsBottomRight = TileToCoords(nextTile(tile));
    return [
        [coordsUpperLeft[0], coordsUpperLeft[1]],
        [coordsBottomRight[0], coordsBottomRight[1]],
    ];
}
function extractLatLngsFromGeoJson(data) {
    return data.features['0'].geometry.coordinates.map(c => [c[1], c[0]]);
}
function extractElevationFromGeoJson(data) {
    return data.features['0'].geometry.coordinates.map(c => c[2]);
}
function getLatLngBoundsFromGeoJson(data, extend = 0) {
    return getLatLngBounds(extractLatLngsFromGeoJson(data), extend);
}
function getLatLngBounds(tuples, extend = 0) {
    const lats = tuples.map(t => t[0]);
    const lngs = tuples.map(t => t[1]);
    return new leaflet__WEBPACK_IMPORTED_MODULE_0__["LatLngBounds"]([Math.min(...lats) - extend, Math.min(...lngs) - extend], [Math.max(...lats) + extend, Math.max(...lngs) + extend]);
}
function filterLngLatInTile(tuples, bounds) {
    const north = bounds[0][0];
    const south = bounds[1][0];
    const west = bounds[0][1];
    const east = bounds[1][1];
    return tuples.filter(t => south < t[0] && t[0] < north && west < t[1] && t[1] < east);
}


/***/ }),

/***/ "./src/app/map/overpass.service.ts":
/*!*****************************************!*\
  !*** ./src/app/map/overpass.service.ts ***!
  \*****************************************/
/*! exports provided: Natural, pointOfInterestOnRoute, OverpassService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Natural", function() { return Natural; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pointOfInterestOnRoute", function() { return pointOfInterestOnRoute; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OverpassService", function() { return OverpassService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _helper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../helper */ "./src/app/helper.ts");
/* harmony import */ var _interfaces__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./interfaces */ "./src/app/map/interfaces.ts");






const toBbox = (bounds, n) => {
    if (n) {
        return `${Object(_helper__WEBPACK_IMPORTED_MODULE_4__["round"])(bounds.getSouth(), n)},${Object(_helper__WEBPACK_IMPORTED_MODULE_4__["round"])(bounds.getWest(), n)},${Object(_helper__WEBPACK_IMPORTED_MODULE_4__["round"])(bounds.getNorth(), n)},${Object(_helper__WEBPACK_IMPORTED_MODULE_4__["round"])(bounds.getEast(), n)}`;
    }
    return `${bounds.getSouth()},${bounds.getWest()},${bounds.getNorth()},${bounds.getEast()}`;
};
const precision = 3;
var Natural;
(function (Natural) {
    Natural["Peak"] = "peak";
    Natural["Saddle"] = "saddle";
})(Natural || (Natural = {}));
const pointOfInterestOnRoute = [
    '["natural"="saddle"]',
    '["natural"="peak"]',
    '["natural"="volcano"]',
    '["tourism"="viewpoint"]["map_type"!="toposcope"]',
    '["map_type"="toposcope"]',
    '["waterway"="waterfall"]',
    '["amenity"="drinking_water"]',
];
//   'saddle':'node["natural"="saddle"]',
//     'peak':'node["natural"="peak"]',
// # 'peak':'node["natural"="volcano"]',
//     'waterfall':'node["waterway"="waterfall"]',
// # 'waterfall':'node["natural"="waterfall"]',
//     'guidepost':'node["information"="guidepost"]',
//     'cave_entrance':'node["natural"="cave_entrance"]',
//     'viewpoint':'node["tourism"="viewpoint"]["map_type"!="toposcope"]',
//     'toposcope':'node["map_type"="toposcope"]',
//     'drinking_water':'node["amenity"="drinking_water"]',
//     'fountain':'node["amenity"="fountain"]',
//     'alpine_hut':'node["tourism"="alpine_hut"]',
//     'wilderness_hut':'node["tourism"="wilderness_hut"]',
//     'shelter':'node["amenity"="shelter"]',
//     'tree':'node["natural"="tree"]["name"]',
//     'aircraft_wreck':'node["historic"="aircraft_wreck"]["aircraft_wreck"]',
//     'barrier':'node["barrier"]["barrier"!="bollard"]',
//     'chapel':'node["building"="chapel"]',
//     'ford':'node["ford"="yes"]',
//     'ruins':'node["historic"="ruins"]',
//     'castle':'node["historic"="castle"]',
//     'toilets':'node["amenity"="toilets"]',
//     'attraction':'node["tourism"="attraction"]',
//     'spring':'node["natural"="spring"]',
//     'cairn':'node["man_made"="cairn"]',
//     'locality':'node["place"="locality"]',
//     'camp_site':'node["tourism"="camp_site"]',
//     'hostel':'node["tourism"="hostel"]',
//     'hotel':'node["tourism"="hotel"]'
let OverpassService = class OverpassService {
    constructor(_httpClient) {
        this._httpClient = _httpClient;
    }
    getWays(query, bounds) {
        return this._fetchOverpassApi(`[out:json][timeout:25];(way${query}(${toBbox(bounds, precision)}););out;>;out skel qt;`)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])((data) => {
            return this._handleWays(data);
        }));
    }
    getNodes(query, bounds) {
        return this._fetchOverpassApi(`[out:json][timeout:25];(${query.map(q => `node${q}(${toBbox(bounds, precision)});`).join('')});out;>;out skel qt;`)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])((data) => {
            return this._handleNodes(data);
        }));
    }
    getWayFromCoord(latlng, around = 1) {
        return this._fetchOverpassApi(`[out:json][timeout:25];(way(around:${around}, ${latlng.join(',')}););out;>;out skel qt;`)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])((data) => {
            return this._handleWays(data);
        }));
    }
    getPOI(bounds, overPassKey) {
        return this._fetchOverpassApi(`[out:json][timeout:25];(node${overPassKey}(${toBbox(bounds, precision)}););out;>;out skel qt;`)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])((data) => {
            return this._handleNodes(data);
        }));
    }
    getPeaks(bounds, type) {
        return this._fetchOverpassApi(`[out:json][timeout:25];(node["natural"="${type}"](${toBbox(bounds, precision)}););out;>;out skel qt;`)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])((data) => {
            return this._handleNodes(data);
        }));
    }
    getElementById(id, osmType) {
        return this._fetchOverpassApi(`[out:json][timeout:25];(${osmType}(${id}););out;>;out skel qt;`)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])((data) => {
            switch (osmType) {
                case _interfaces__WEBPACK_IMPORTED_MODULE_5__["OsmType"].Node: {
                    return this._handleNodes(data)[0];
                }
                case _interfaces__WEBPACK_IMPORTED_MODULE_5__["OsmType"].Way: {
                    return this._handleWays(data)[0];
                }
            }
        }));
    }
    _fetchOverpassApi(overpassQuery) {
        // .replace(/[\s]*/gm, '')
        console.log('overpassQuery', overpassQuery);
        const queryString = encodeURIComponent(overpassQuery);
        // console.log('queryString', overpassQuery);
        // const url = `http://overpass-api.de/api/interpreter?data=${queryString}`;
        const url = `https://overpass.openstreetmap.fr/api/interpreter?data=${queryString}`;
        // const url = `https://lz4.overpass-api.de/api/interpreter?data=${queryString}`;
        return this._httpClient.get(url);
    }
    _handleNodes(data) {
        return data.elements.map(e => (Object.assign({}, e, { latlng: [e.lat, e.lon] })));
    }
    _handleWays(data) {
        const nodes = data.elements.filter(e => e.type === 'node');
        const ways = data.elements.filter(e => e.type === 'way');
        return ways.map(way => {
            const output = way;
            output.latlngs = output.nodes.map(e => {
                const node = nodes.find(n => n.id === e);
                return [node.lat, node.lon];
            });
            return output;
        });
    }
};
OverpassService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
];
OverpassService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root',
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
], OverpassService);



/***/ }),

/***/ "./src/app/map/popup/popup.component.scss":
/*!************************************************!*\
  !*** ./src/app/map/popup/popup.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "div {\n  background: #182b33;\n  color: #ffffff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2tyaXMvRG9jdW1lbnRzL3JlcG9zL2tyaXMvc3VtbWl0cy9zcmMvYXBwL21hcC9wb3B1cC9wb3B1cC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbWFwL3BvcHVwL3BvcHVwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0UsbUJBSFM7RUFJVCxjQUFBO0FDREYiLCJmaWxlIjoic3JjL2FwcC9tYXAvcG9wdXAvcG9wdXAuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIkZGVlcEJsdWU6ICMxODJiMzM7XG5cbmRpdiB7XG4gIGJhY2tncm91bmQ6ICRkZWVwQmx1ZTtcbiAgY29sb3I6ICNmZmZmZmY7XG59XG4iLCJkaXYge1xuICBiYWNrZ3JvdW5kOiAjMTgyYjMzO1xuICBjb2xvcjogI2ZmZmZmZjtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/map/popup/popup.component.ts":
/*!**********************************************!*\
  !*** ./src/app/map/popup/popup.component.ts ***!
  \**********************************************/
/*! exports provided: PopupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PopupComponent", function() { return PopupComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let PopupComponent = class PopupComponent {
    constructor() {
        this.test = 'ok';
    }
    ngOnInit() {
        // this.test = 'loool';
        console.log('this.test', this.test);
    }
};
PopupComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-popup',
        template: __webpack_require__(/*! raw-loader!./popup.component.html */ "./node_modules/raw-loader/index.js!./src/app/map/popup/popup.component.html"),
        styles: [__webpack_require__(/*! ./popup.component.scss */ "./src/app/map/popup/popup.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], PopupComponent);



/***/ }),

/***/ "./src/app/map/routing.service.ts":
/*!****************************************!*\
  !*** ./src/app/map/routing.service.ts ***!
  \****************************************/
/*! exports provided: RoutingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoutingService", function() { return RoutingService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! leaflet */ "./node_modules/leaflet/dist/leaflet-src.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(leaflet__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _gpx_gpx_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../gpx/gpx.service */ "./src/app/gpx/gpx.service.ts");
/* harmony import */ var _helper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../helper */ "./src/app/helper.ts");
/* harmony import */ var _interfaces__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./interfaces */ "./src/app/map/interfaces.ts");
/* harmony import */ var _overpass_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./overpass.service */ "./src/app/map/overpass.service.ts");
/* harmony import */ var _osm_helper__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./osm.helper */ "./src/app/map/osm.helper.ts");
/* harmony import */ var _leaflet_media__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./leaflet.media */ "./src/app/map/leaflet.media.ts");
/* harmony import */ var _leaflet_helper__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./leaflet.helper */ "./src/app/map/leaflet.helper.ts");
/* harmony import */ var _b_router_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./b-router.service */ "./src/app/map/b-router.service.ts");
/* harmony import */ var _gpx_helper__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./gpx.helper */ "./src/app/map/gpx.helper.ts");













const circleIcon = (isFirst, clickType) => leaflet__WEBPACK_IMPORTED_MODULE_3__["divIcon"]({
    className: isFirst
        ? 'leaflet-div-icon-green'
        : clickType === _interfaces__WEBPACK_IMPORTED_MODULE_6__["ClickType"].Left
            ? 'leaflet-div-icon-red'
            : 'leaflet-div-icon-violet',
});
let RoutingService = class RoutingService {
    constructor(_gpxService, _overpassService, _bRouterService, _httpClient) {
        this._gpxService = _gpxService;
        this._overpassService = _overpassService;
        this._bRouterService = _bRouterService;
        this._httpClient = _httpClient;
        this._routingMarkers = [];
        this._redoRoutingMarkers = [];
        this._isReversed = false;
        this._waypointMarkers = [];
        // Optimisation to find associated polyline closest to mouse
        this._mouseOverPolylineId = null;
        this._mouseAdditionalMarker = null;
        this._distanceToVanish = 0.3;
    }
    initMap(lMap) {
        if (!this._map) {
            this._map = lMap;
        }
    }
    getInfo() {
        const distance = this._routingMarkers.reduce((acc, marker) => {
            return acc + Object(_osm_helper__WEBPACK_IMPORTED_MODULE_8__["getDistanceLatLngs"])(marker.route);
        }, 0);
        const elevation = this._routingMarkers.reduce((acc, marker) => {
            if (marker && marker.elevation) {
                const ascent = Object(_osm_helper__WEBPACK_IMPORTED_MODULE_8__["getTotalAscent"])(marker.elevation);
                acc[0] += ascent[0];
                acc[1] += ascent[1];
            }
            return acc;
        }, [0, 0]);
        return {
            distance,
            numberOfMarkers: this._routingMarkers.length,
            totalAscent: elevation[0],
            totalDescent: -elevation[1],
            data: {
                elevation: [],
            },
        };
    }
    canUndoRedo(type) {
        if (type === 'undo') {
            return this._routingMarkers.length > 0;
        }
        return this._redoRoutingMarkers.length > 0;
    }
    undo() {
        const lastMarker = this._routingMarkers.pop();
        if (lastMarker) {
            if (lastMarker.polyline) {
                this._map.removeLayer(lastMarker.polyline);
            }
            this._map.removeLayer(lastMarker.marker);
            this._redoRoutingMarkers.push(lastMarker);
        }
    }
    redo() {
        const lastMarker = this._redoRoutingMarkers.pop();
        if (lastMarker) {
            if (lastMarker.polyline) {
                this._map.addLayer(lastMarker.polyline);
            }
            this._map.addLayer(lastMarker.marker);
            this._routingMarkers.push(lastMarker);
        }
    }
    reverse() {
        // Move polyline to the marker ahead (so the marker index 0 has no polyline)
        for (let i = 0; i < this._routingMarkers.length; i++) {
            const marker = this._routingMarkers[i];
            if (i < this._routingMarkers.length - 1) {
                const nextMarker = this._routingMarkers[i + 1];
                marker.polyline = nextMarker.polyline;
                marker.route = nextMarker.route.reverse();
            }
            else {
                marker.polyline = null;
                marker.route = [];
                marker.marker.setIcon(circleIcon(true, marker.type));
            }
            if (i === 0) {
                marker.marker.setIcon(circleIcon(false, marker.type));
            }
        }
        this._routingMarkers.reverse();
        this._isReversed = !this._isReversed;
    }
    resetRouting() {
        for (const marker of this._routingMarkers) {
            if (marker.polyline) {
                this._map.removeLayer(marker.polyline);
            }
            this._map.removeLayer(marker.marker);
        }
        for (const marker of this._waypointMarkers) {
            this._map.removeLayer(marker.marker);
        }
        this._routingMarkers = [];
    }
    saveAsGpx() {
        const completeRoute = this._routingMarkers.reduce((route, marker) => {
            return route.concat(marker.route);
        }, []);
        Object(_gpx_helper__WEBPACK_IMPORTED_MODULE_12__["convertToGpx"])(completeRoute);
    }
    navigate(lMap, latLng, clickType, previous) {
        // Avoid redo
        this._redoRoutingMarkers = [];
        const isFirst = this._routingMarkers.length === 0;
        const newDestinationMarker = leaflet__WEBPACK_IMPORTED_MODULE_3__["marker"](latLng, {
            draggable: true,
            icon: circleIcon(isFirst, clickType),
        }).addTo(lMap);
        // Remove destination
        newDestinationMarker.on('click', (ev) => {
            this._renavigate(lMap, newDestinationMarker, ev, 'click');
        });
        newDestinationMarker.on('moveend', (ev) => {
            this._renavigate(lMap, newDestinationMarker, ev, 'moveend');
            this._removeHoverMarker(lMap);
        });
        newDestinationMarker.on('mouseover', () => {
            this._removeHoverMarker(lMap);
        });
        const routingMarker = {
            markerLeafletId: newDestinationMarker._leaflet_id,
            latLng: latLng,
            type: clickType,
            marker: newDestinationMarker,
        };
        if (this._routingMarkers.length === 0) {
            this._routingMarkers.push(routingMarker);
            return;
        }
        // TODO Useless for now ... WHy ???
        const previousLatLng = previous ? previous : this._routingMarkers[this._routingMarkers.length - 1].latLng;
        if (clickType === _interfaces__WEBPACK_IMPORTED_MODULE_6__["ClickType"].Right) {
            routingMarker.polyline = leaflet__WEBPACK_IMPORTED_MODULE_3__["polyline"]([previousLatLng, latLng], _leaflet_media__WEBPACK_IMPORTED_MODULE_9__["routingPolylineOptions"]).addTo(lMap);
            routingMarker.route = [previousLatLng, latLng];
            // TODO find a way to get elevation data
            routingMarker.elevation = [null, null];
        }
        else {
            this._bRouterService.fetchRoute(previousLatLng, latLng)
                .subscribe((geoJsonData) => {
                if (!geoJsonData) {
                    lMap.removeLayer(newDestinationMarker);
                    this._routingMarkers.pop();
                    return;
                }
                // Check if marker has not been deleted
                if (!this._routingMarkers.find(m => m.markerLeafletId === routingMarker.markerLeafletId)) {
                    console.log('ghost');
                    return;
                }
                routingMarker.route = [
                    previousLatLng,
                    ...Object(_osm_helper__WEBPACK_IMPORTED_MODULE_8__["extractLatLngsFromGeoJson"])(geoJsonData)
                ];
                routingMarker.polyline = leaflet__WEBPACK_IMPORTED_MODULE_3__["polyline"](routingMarker.route, _leaflet_media__WEBPACK_IMPORTED_MODULE_9__["routingPolylineOptions"])
                    .addTo(lMap)
                    .bindPopup(`<pre>${JSON.stringify(routingMarker.route, null, 2)}</pre>`);
                routingMarker.elevation = Object(_osm_helper__WEBPACK_IMPORTED_MODULE_8__["extractElevationFromGeoJson"])(geoJsonData);
                // routingMarker.polyline.on('click', (e) => {
                //   this._mouseOverPolylineId = (e.target as any)._leaflet_id;
                // });
                routingMarker.polyline.on('mouseover', (e) => {
                    this._mouseOverPolylineId = e.target._leaflet_id;
                });
                routingMarker.polyline.on('mouseout', () => {
                    this._mouseOverPolylineId = null;
                });
                // POIs
                this._addPointOfInterestsOnRoute(lMap, routingMarker.route, newDestinationMarker);
                // Try to guess way ids then recall overpass API to get other data
                const messages = geoJsonData.features['0'].properties.messages;
                const messagesLatLngs = this._bRouterService.parseCoordsFromBRouterMessages(messages);
                if (messagesLatLngs && messagesLatLngs.length) {
                    this._overpassService.getWayFromCoord(messagesLatLngs[0])
                        .subscribe((ways) => {
                        for (const way of ways) {
                            this._handleSacScale(lMap, way);
                        }
                    });
                }
            });
        }
        this._routingMarkers.push(routingMarker);
    }
    mouseOverPolyline(lMap, mouseLatLng) {
        if (this._mouseAdditionalMarker) {
            const distanceToPolyline = Object(_leaflet_helper__WEBPACK_IMPORTED_MODULE_10__["customHaversine"])(Object(_leaflet_helper__WEBPACK_IMPORTED_MODULE_10__["getLatLngTuple"])(this._mouseAdditionalMarker.marker), mouseLatLng);
            if (distanceToPolyline > this._distanceToVanish && !this._mouseAdditionalMarker.isMoving) {
                lMap.removeLayer(this._mouseAdditionalMarker.marker);
                this._mouseAdditionalMarker = null;
                return;
            }
            const markerOnMouse = this._routingMarkers
                .filter(m => m.polyline)
                .find(m => m.polyline._leaflet_id === this._mouseAdditionalMarker.polylineId);
            if (markerOnMouse.polyline) {
                const closetCoordinate = Object(_leaflet_helper__WEBPACK_IMPORTED_MODULE_10__["getClosestCoordinate"])(markerOnMouse.polyline, mouseLatLng);
                if (closetCoordinate) {
                    this._mouseAdditionalMarker.marker.setLatLng(closetCoordinate);
                }
            }
        }
        if (this._mouseOverPolylineId) {
            if (!this._mouseAdditionalMarker) {
                const marker = leaflet__WEBPACK_IMPORTED_MODULE_3__["marker"](mouseLatLng, {
                    draggable: true,
                    icon: leaflet__WEBPACK_IMPORTED_MODULE_3__["divIcon"]({
                        className: 'leaflet-div-icon-light-blue',
                    })
                })
                    .addTo(lMap);
                this._mouseAdditionalMarker = {
                    marker,
                    polylineId: this._mouseOverPolylineId,
                    isMoving: false,
                };
                marker.on('movestart', () => {
                    this._mouseAdditionalMarker.isMoving = true;
                });
                marker.on('moveend', (e) => {
                    lMap.removeLayer(this._mouseAdditionalMarker.marker);
                    this._mouseAdditionalMarker = null;
                    this._renavigate(lMap, marker, e, 'moveend');
                });
            }
        }
    }
    _renavigate(lMap, marker, event, type) {
        const markerToMoveOrRemove = this._routingMarkers.find(p => p.markerLeafletId === event.target._leaflet_id);
        const markerIndex = this._routingMarkers.indexOf(markerToMoveOrRemove);
        const { nextMarker, previousMarker } = this._getBoundsMarkers(markerIndex);
        // Delete marker
        if (type === 'click') {
            lMap.removeLayer(marker);
            this._removeAssociatedWaypointOnMap(lMap, marker);
            if (markerToMoveOrRemove.polyline) {
                lMap.removeLayer(markerToMoveOrRemove.polyline);
            }
            if (!previousMarker && nextMarker) {
                lMap.removeLayer(nextMarker.polyline);
                nextMarker.route = [];
                nextMarker.polyline = null;
                nextMarker.marker.setIcon(circleIcon(true, nextMarker.type));
            }
            else if (previousMarker && nextMarker) {
                lMap.removeLayer(nextMarker.polyline);
                this._bRouterService.fetchRoute(previousMarker.latLng, nextMarker.latLng)
                    .subscribe((geoJsonData) => {
                    if (!geoJsonData) {
                        nextMarker.polyline = leaflet__WEBPACK_IMPORTED_MODULE_3__["polyline"]([previousMarker.latLng, nextMarker.latLng], _leaflet_media__WEBPACK_IMPORTED_MODULE_9__["routingPolylineOptions"]).addTo(lMap);
                        return;
                    }
                    const route = this._getRoute(previousMarker, geoJsonData);
                    nextMarker.polyline = leaflet__WEBPACK_IMPORTED_MODULE_3__["polyline"](route, _leaflet_media__WEBPACK_IMPORTED_MODULE_9__["routingPolylineOptions"]).addTo(lMap);
                });
            }
            this._routingMarkers.splice(markerIndex, 1);
        }
        else if (type === 'moveend') {
            const newLatLng = [event.target._latlng.lat, event.target._latlng.lng];
            markerToMoveOrRemove.latLng = newLatLng;
            if (markerToMoveOrRemove.polyline) {
                lMap.removeLayer(markerToMoveOrRemove.polyline);
                this._removeAssociatedWaypointOnMap(lMap, markerToMoveOrRemove.markerLeafletId);
                if (markerToMoveOrRemove.type === _interfaces__WEBPACK_IMPORTED_MODULE_6__["ClickType"].Left) {
                    this._bRouterService.fetchRoute(previousMarker.latLng, newLatLng)
                        .subscribe((data) => {
                        if (!data) {
                            markerToMoveOrRemove.polyline = leaflet__WEBPACK_IMPORTED_MODULE_3__["polyline"]([previousMarker.latLng, newLatLng], _leaflet_media__WEBPACK_IMPORTED_MODULE_9__["routingPolylineOptions"]).addTo(lMap);
                            return;
                        }
                        const route = this._getRoute(previousMarker, data);
                        markerToMoveOrRemove.polyline = leaflet__WEBPACK_IMPORTED_MODULE_3__["polyline"](route, _leaflet_media__WEBPACK_IMPORTED_MODULE_9__["routingPolylineOptions"]).addTo(lMap);
                        this._addPointOfInterestsOnRoute(lMap, route, markerToMoveOrRemove.marker);
                    });
                }
                else {
                    markerToMoveOrRemove.polyline = leaflet__WEBPACK_IMPORTED_MODULE_3__["polyline"]([previousMarker.latLng, newLatLng], _leaflet_media__WEBPACK_IMPORTED_MODULE_9__["routingPolylineOptions"]).addTo(lMap);
                }
            }
            if (nextMarker) {
                lMap.removeLayer(nextMarker.polyline);
                this._removeAssociatedWaypointOnMap(lMap, nextMarker.markerLeafletId);
                if (nextMarker.type === _interfaces__WEBPACK_IMPORTED_MODULE_6__["ClickType"].Left) {
                    this._bRouterService.fetchRoute(newLatLng, nextMarker.latLng)
                        .subscribe((data) => {
                        if (!data) {
                            nextMarker.polyline = leaflet__WEBPACK_IMPORTED_MODULE_3__["polyline"]([newLatLng, nextMarker.latLng], _leaflet_media__WEBPACK_IMPORTED_MODULE_9__["routingPolylineOptions"]).addTo(lMap);
                            return;
                        }
                        nextMarker.polyline = leaflet__WEBPACK_IMPORTED_MODULE_3__["polyline"](Object(_osm_helper__WEBPACK_IMPORTED_MODULE_8__["extractLatLngsFromGeoJson"])(data), _leaflet_media__WEBPACK_IMPORTED_MODULE_9__["routingPolylineOptions"]).addTo(lMap);
                    });
                }
                else {
                    nextMarker.polyline = leaflet__WEBPACK_IMPORTED_MODULE_3__["polyline"]([newLatLng, nextMarker.latLng], _leaflet_media__WEBPACK_IMPORTED_MODULE_9__["routingPolylineOptions"]).addTo(lMap);
                }
            }
        }
    }
    _addPointOfInterestsOnRoute(lMap, route, newDestinationMarker) {
        const boundsBox = Object(_osm_helper__WEBPACK_IMPORTED_MODULE_8__["getLatLngBounds"])(route, 0.001);
        this._overpassService.getNodes(_overpass_service__WEBPACK_IMPORTED_MODULE_7__["pointOfInterestOnRoute"], boundsBox)
            .subscribe((osmNodes) => {
            for (const osmNode of osmNodes) {
                this._addWaypointMarker(lMap, route, newDestinationMarker, osmNode);
            }
        });
    }
    _getBoundsMarkers(markerIndex) {
        const isFirstMarker = markerIndex === 0;
        const isLastMarker = markerIndex === this._routingMarkers.length - 1;
        let nextMarker;
        let previousMarker;
        if (!isLastMarker) {
            nextMarker = this._routingMarkers[markerIndex + 1];
        }
        if (!isFirstMarker) {
            previousMarker = this._routingMarkers[markerIndex - 1];
        }
        return { nextMarker, previousMarker };
    }
    _addWaypointMarker(lMap, route, nextDestinationMarker, node) {
        if (!node || !node.tags) {
            return;
        }
        // Avoid duplicate
        if (this._waypointMarkers.some(m => m.osmNodeId === node.id)) {
            return;
        }
        // Avoid ghosts
        if (!Object(_leaflet_helper__WEBPACK_IMPORTED_MODULE_10__["isMarkerOnMap"])(lMap, nextDestinationMarker)) {
            return;
        }
        // Find closest point to osm node
        let minDist = Infinity;
        for (const point of route) {
            const dist = Object(_leaflet_helper__WEBPACK_IMPORTED_MODULE_10__["customHaversine"])(point, node.latlng);
            minDist = Math.min(dist, minDist);
        }
        console.log(`min dist ${Object(_helper__WEBPACK_IMPORTED_MODULE_5__["round"])(minDist * 1000, 2)} m`);
        if (minDist * 1000 > 60) {
            return;
        }
        let markerType = 'tree';
        if ('amenity' in node.tags) {
            switch (node.tags.amenity) {
                case 'drinking_water': {
                    markerType = 'water';
                }
            }
        }
        else if ('natural' in node.tags) {
            markerType = node.tags.natural;
        }
        else if ('tourism' in node.tags) {
            markerType = node.tags.tourism;
        }
        const osmMarker = leaflet__WEBPACK_IMPORTED_MODULE_3__["marker"](node.latlng, {
            icon: Object(_leaflet_media__WEBPACK_IMPORTED_MODULE_9__["waypointMarker"])(markerType),
        })
            .addTo(lMap)
            .bindPopup(`<pre>${JSON.stringify(node.tags, null, 2)}</pre>`);
        // .bindPopup(`min dist ${round(minDist * 1000, 2)} m`);
        this._waypointMarkers.push({
            marker: osmMarker,
            osmNodeId: node.id,
            id: nextDestinationMarker._leaflet_id,
        });
    }
    _handleSacScale(lMap, way) {
        if (way.tags && way.tags.sac_scale) {
            const coord = way.latlngs[0];
            switch (way.tags.sac_scale) {
                case 'hiking':
                    // option.color = 'green';
                    break;
                case 'mountain_hiking':
                    // option.color = 'blue';
                    break;
                case 'demanding_mountain_hiking':
                    // option.color = 'blue';
                    // option.dashArray = '10 5';
                    const myIcon = leaflet__WEBPACK_IMPORTED_MODULE_3__["divIcon"]({
                        className: 'leaflet-div-icon-yellow',
                    });
                    const marker = leaflet__WEBPACK_IMPORTED_MODULE_3__["marker"](coord, {
                        draggable: true,
                        icon: myIcon,
                    }).addTo(lMap);
                    break;
                case 'alpine_hiking':
                    // option.color = 'red';
                    break;
                case 'demanding_alpine_hiking':
                    // option.color = 'black';
                    break;
                case 'difficult_alpine_hiking':
                    // option.color = 'black';
                    // option.dashArray = '10 5';
                    break;
            }
        }
    }
    _getRoute(previousMarker, geoJsonData) {
        if (previousMarker.type === _interfaces__WEBPACK_IMPORTED_MODULE_6__["ClickType"].Right) {
            return [
                previousMarker.latLng,
                ...Object(_osm_helper__WEBPACK_IMPORTED_MODULE_8__["extractLatLngsFromGeoJson"])(geoJsonData),
            ];
        }
        else {
            return Object(_osm_helper__WEBPACK_IMPORTED_MODULE_8__["extractLatLngsFromGeoJson"])(geoJsonData);
        }
    }
    _removeAssociatedWaypointOnMap(lMap, oldDestinationMarker) {
        const leafletId = typeof oldDestinationMarker === 'number'
            ? oldDestinationMarker
            : oldDestinationMarker._leaflet_id;
        for (const waypoint of this._waypointMarkers.filter(w => w.id === leafletId)) {
            lMap.removeLayer(waypoint.marker);
        }
        this._waypointMarkers = this._waypointMarkers.filter(w => w.id !== leafletId);
    }
    /**
     * Avoid messy maps
     */
    _removeHoverMarker(lMap) {
        if (this._mouseAdditionalMarker && this._mouseAdditionalMarker.marker) {
            lMap.removeLayer(this._mouseAdditionalMarker.marker);
        }
        this._mouseAdditionalMarker = null;
    }
};
RoutingService.ctorParameters = () => [
    { type: _gpx_gpx_service__WEBPACK_IMPORTED_MODULE_4__["GpxService"] },
    { type: _overpass_service__WEBPACK_IMPORTED_MODULE_7__["OverpassService"] },
    { type: _b_router_service__WEBPACK_IMPORTED_MODULE_11__["BRouterService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
RoutingService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_gpx_gpx_service__WEBPACK_IMPORTED_MODULE_4__["GpxService"],
        _overpass_service__WEBPACK_IMPORTED_MODULE_7__["OverpassService"],
        _b_router_service__WEBPACK_IMPORTED_MODULE_11__["BRouterService"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], RoutingService);



/***/ }),

/***/ "./src/app/map/tiles.endpoints.ts":
/*!****************************************!*\
  !*** ./src/app/map/tiles.endpoints.ts ***!
  \****************************************/
/*! exports provided: tileServers, mapboxTiles, suuntoHeatmapDomain, suuntoHeatmap, overlay */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "tileServers", function() { return tileServers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mapboxTiles", function() { return mapboxTiles; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "suuntoHeatmapDomain", function() { return suuntoHeatmapDomain; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "suuntoHeatmap", function() { return suuntoHeatmap; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "overlay", function() { return overlay; });
// tslint:disable:max-line-length
const tileServers = {
    openStreetMap: {
        url: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png'
    },
    openTopoMap: {
        url: 'http://{s}.tile.opentopomap.org/{z}/{x}/{y}.png',
        options: {
            maxZoom: 17,
            detectRetina: true,
            attribution: '',
        },
    },
    strava: {
        url: 'https://b.tiles.mapbox.com/v4/strava.blprdx6r/{z}/{x}/{y}.png?access_token=pk.eyJ1Ijoic3RyYXZhIiwiYSI6IlpoeXU2U0UifQ.c7yhlZevNRFCqHYm6G6Cyg'
    },
};
const mapboxUrl = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}@2x.png?access_token=pk.eyJ1Ijoia3Jpc3RyZW1vbmRvdXMiLCJhIjoiT2R0Y084YyJ9.fePC_n9LzdWkjWug4mdTCw';
const mapboxAttr = '';
const mapboxTiles = {
    grayscale: {
        url: mapboxUrl,
        options: {
            id: 'mapbox.light',
            attribution: mapboxAttr
        },
    },
    strava: {
        url: mapboxUrl,
        options: {
            id: 'strava.1vr0ms4i',
            attribution: mapboxAttr
        },
    },
    streets: {
        url: mapboxUrl,
        options: {
            id: 'mapbox.streets',
            attribution: mapboxAttr
        },
    },
    satellite: {
        url: mapboxUrl,
        options: {
            id: 'mapbox.satellite',
            attribution: mapboxAttr
        },
    },
};
// http://heatmaprestapi-production.eu-west-1.elasticbeanstalk.com/heatmap/v1/AllTrails/{z}/{x}/{y}.png?appkey=KJ5ZC2d0mx1wE6FtkXrHGigZzdrL3OOl9IZKWftul9KhH25L2wKkAUyBDgzyKC6I
const suuntoHeatmapDomain = 'http://heatmaprestapi-production.eu-west-1.elasticbeanstalk.com';
const appKey = 'KJ5ZC2d0mx1wE6FtkXrHGigZzdrL3OOl9IZKWftul9KhH25L2wKkAUyBDgzyKC6I';
const suuntoHeatmap = (type) => `${suuntoHeatmapDomain}/heatmap/v1/${type}/{z}/{x}/{y}.png?appkey=${appKey}`;
const overlay = {
    waymarkedtrail: {
        url: 'https://tile.waymarkedtrails.org/hiking/{z}/{x}/{y}.png',
    },
    suunto: {
        options: {
            maxNativeZoom: 14,
        },
    }
};


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ "./src/material.module.ts":
/*!********************************!*\
  !*** ./src/material.module.ts ***!
  \********************************/
/*! exports provided: MaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialModule", function() { return MaterialModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");



let MaterialModule = class MaterialModule {
};
MaterialModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenavModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
            // MatFormFieldModule,
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
            // MatInputModule,
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatListModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatMenuModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatBadgeModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
        ],
        exports: [
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenavModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatListModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatMenuModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatBadgeModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
        ],
    })
], MaterialModule);



/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/kris/Documents/repos/kris/summits/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map